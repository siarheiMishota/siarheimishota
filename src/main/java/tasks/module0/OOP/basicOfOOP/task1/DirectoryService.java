package tasks.module0.OOP.basicOfOOP.task1;

public class DirectoryService {

    private Directory directory;

    public DirectoryService(Directory directory) {
        this.directory = directory;
    }

    public Directory getDirectory() {
        return directory;
    }

    public void addDirectory(Directory inputDirectory) {
        if (this.directory.getDirectories().contains(inputDirectory)) {

            throw new IllegalArgumentException("Directory already exists");

        }

        this.directory.getDirectories().add(inputDirectory);

    }

    public void addFile(File file) {

        if (directory.getFiles().contains(file)) {
            throw new IllegalArgumentException("File already exists");
        }

        directory.getFiles().add(file);
    }

    public boolean deleteFile(File file) {
        return directory.getFiles().remove(file);
    }


}
