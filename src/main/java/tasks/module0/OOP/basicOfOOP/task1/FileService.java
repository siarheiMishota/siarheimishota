package tasks.module0.OOP.basicOfOOP.task1;

public class FileService {

    private File file;

    public FileService(File file) {
        this.file = file;
    }

    public static void create(String name, String contents, Directory directory) {

        File newFile = new File(name, contents);

        directory.getFiles().add(newFile);

    }

    public boolean rename(String nameNew) {

        if (file != null && nameNew != null) {

            file.setName(nameNew);
            return true;

        }

        return false;
    }

    public boolean addContents(String addedContent) {

        if (file != null && addedContent != null) {
            file.setContent("" + file.getContent() + " " + addedContent);
            return true;
        }
        return false;

    }

    public String getContents() {

        return this.file.getContent();

    }


}
