package tasks.module0.OOP.basicOfOOP.task1;

public class DirectoryDAO {

    public Directory getDirectories() {

        int numberOfDirectory = 1, numberOfFile = 1;

        Directory main = new Directory("main");

        DirectoryService mainDirectoryService = new DirectoryService(main);

        mainDirectoryService.addDirectory(new Directory("directory" + numberOfDirectory++));
        mainDirectoryService.addDirectory(new Directory("directory" + numberOfDirectory++));
        mainDirectoryService.addDirectory(new Directory("directory" + numberOfDirectory++));
        mainDirectoryService.addDirectory(new Directory("directory" + numberOfDirectory++));

        mainDirectoryService.addFile(new File("file" + numberOfFile, "context " + numberOfFile++));
        mainDirectoryService.addFile(new File("file" + numberOfFile, "context " + numberOfFile++));
        mainDirectoryService.addFile(new File("file" + numberOfFile, "context " + numberOfFile++));
        mainDirectoryService.addFile(new File("file" + numberOfFile, "context " + numberOfFile++));

        DirectoryService directoryService = new DirectoryService(mainDirectoryService.getDirectory().getDirectories().get(0));

        directoryService.addDirectory(new Directory("directory" + numberOfDirectory++));
        directoryService.addDirectory(new Directory("directory" + numberOfDirectory++));
        directoryService.addDirectory(new Directory("directory" + numberOfDirectory++));
        directoryService.addDirectory(new Directory("directory" + numberOfDirectory++));
        directoryService.addDirectory(new Directory("directory" + numberOfDirectory++));
        directoryService.addDirectory(new Directory("directory" + numberOfDirectory++));
        directoryService.addDirectory(new Directory("directory" + numberOfDirectory++));
        directoryService.addDirectory(new Directory("directory" + numberOfDirectory++));
        directoryService.addDirectory(new Directory("directory" + numberOfDirectory++));

        directoryService.addFile(new File("file" + numberOfFile, "context " + numberOfFile++));
        directoryService.addFile(new File("file" + numberOfFile, "context " + numberOfFile++));
        directoryService.addFile(new File("file" + numberOfFile, "context " + numberOfFile++));
        directoryService.addFile(new File("file" + numberOfFile, "context " + numberOfFile++));
        directoryService.addFile(new File("file" + numberOfFile, "context " + numberOfFile++));
        directoryService.addFile(new File("file" + numberOfFile, "context " + numberOfFile++));
        directoryService.addFile(new File("file" + numberOfFile, "context " + numberOfFile++));
        directoryService.addFile(new File("file" + numberOfFile, "context " + numberOfFile++));
        directoryService.addFile(new File("file" + numberOfFile, "context " + numberOfFile++));
        directoryService.addFile(new File("file" + numberOfFile, "context " + numberOfFile++));
        directoryService.addFile(new File("file" + numberOfFile, "context " + numberOfFile++));

        return main;
    }

}
