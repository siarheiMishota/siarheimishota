package tasks.module0.OOP.basicOfOOP.task1;

import static tasks.module0.OOP.basicOfOOP.task1.ChoiceService.choiceDirectory;
import static tasks.module0.OOP.basicOfOOP.task1.ChoiceService.choiceFile;
import static tasks.module0.Service.getIntValue;
import static tasks.module0.Service.getStringLine;

public class Action {

    int choice;
    File file;
    Directory selectedInnerDirectory, directory = new DirectoryDAO().getDirectories();
    FileService fileService;
    DirectoryService selectedDirectoryService;

    String infoMenu = "\nInput the number:\n" +
            "1-create the file on directory\n" +
            "2-rename the file\n" +
            "3-print of the content of the file\n" +
            "4-add content of the file\n" +
            "5-delete the file\n" +
            "other- exit";

    public void execute() {

        while (true) {

            choice = getIntValue(infoMenu);
            System.out.println("You choice- " + choice + " \n-----------------------------------------------------------------------");
            switch (choice) {
                case 1:

                    selectedInnerDirectory = choiceDirectory(directory);
                    String name = getStringLine("Input name of the file");
                    String content = getStringLine("Input content of the file");

                    FileService.create(name, content, selectedInnerDirectory);


                    break;

                case 2:

                    file = choiceFile(directory, "Choice the file");

                    if (file == null) {
                        break;
                    }

                    fileService = new FileService(file);

                    String newName = getStringLine("Input new name of the file");

                    fileService.rename(newName);

                    break;

                case 3:

                    file = choiceFile(directory, "Choice the file");

                    if (file == null) {
                        break;
                    }

                    fileService = new FileService(file);

                    System.out.println(fileService.getContents());

                    break;

                case 4:

                    file = choiceFile(directory, "Choice the file");

                    if (file == null) {
                        break;
                    }

                    fileService = new FileService(file);
                    String contentForAdding = getStringLine("Input added content of the file");

                    boolean isAdded = fileService.addContents(contentForAdding);

                    System.out.println("Added? " + isAdded);

                    break;

                case 5:

                    selectedInnerDirectory = choiceDirectory(directory);
                    file = choiceFile(selectedInnerDirectory, "Choice the file");
                    selectedDirectoryService = new DirectoryService(selectedInnerDirectory);

                    if (file == null) {
                        break;
                    }

                    boolean isDeleted = selectedDirectoryService.deleteFile(file);

                    System.out.println("File was deleted? " + isDeleted);
                    break;

                default:

                    System.exit(0);
                    break;


            }
        }
    }

}
