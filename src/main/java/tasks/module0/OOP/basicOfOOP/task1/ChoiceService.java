package tasks.module0.OOP.basicOfOOP.task1;

import static tasks.module0.Service.getPositiveAndZeroIntValueTillMaxNumber;

public class ChoiceService {

    public static Directory choiceDirectory(Directory directory) {
        int choice;
        Directory middleDirectory = directory;

        System.out.println("Choice the directory");

        while (true) {

            int position = 1;

            for (Directory innerDirectory : middleDirectory.getDirectories()) {
                System.out.println(position++ + "- " + innerDirectory.getName());
            }

            System.out.println(position++ + "- return this directory");


            choice = getPositiveAndZeroIntValueTillMaxNumber("Input the number", position);
            position--;

            if (isSelectedDirectory(choice, position)) {
                middleDirectory = middleDirectory.getDirectories().get(choice - 1);
            } else {
                return middleDirectory;
            }


        }


    }

    private static boolean isSelectedDirectory(int choice, int position) {
        return choice < position;
    }

    public static File choiceFile(Directory directory, String message) {
        int choice;
        Directory choiceDirectory = directory;

        System.out.println(message);

        while (true) {

            int position = 1;

            for (Directory innerDirectory : choiceDirectory.getDirectories()) {
                System.out.println(position++ + "- " + innerDirectory.getName());
            }

            if (isDirectoryHasFiles(choiceDirectory)) {
                for (File innerFile : choiceDirectory.getFiles()) {
                    System.out.println(position++ + "- " + innerFile.getName());
                }
            }else {
                System.out.println("Directory hasn't got files");
                return null;
            }

            choice = getPositiveAndZeroIntValueTillMaxNumber("Input the number", position);

            if (isIndexOfDirectory(choice-1, choiceDirectory)) {
                choiceDirectory = choiceDirectory.getDirectories().get(choice - 1);
            } else {

                return choiceDirectory.getFiles().get(choice - choiceDirectory.getDirectories().size() - 1);
            }

        }


    }

    private static boolean isDirectoryHasFiles(Directory choiceDirectory) {
        return !choiceDirectory.getFiles().isEmpty();
    }

    private static boolean isIndexOfDirectory(int choice, Directory choiceDirectory) {
        return choice < choiceDirectory.getDirectories().size();
    }
}
