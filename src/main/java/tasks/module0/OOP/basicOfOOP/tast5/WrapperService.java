package tasks.module0.OOP.basicOfOOP.tast5;

import java.util.List;

public interface WrapperService {

    FlowerWrapper getById(int id);

    List<FlowerWrapper> getAll();

    FlowerWrapper getByName(String name);

    void addFlowerWrapper(FlowerWrapper addingFlowerWrapper);


}
