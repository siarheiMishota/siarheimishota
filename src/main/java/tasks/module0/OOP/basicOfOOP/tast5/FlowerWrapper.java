package tasks.module0.OOP.basicOfOOP.tast5;

public class FlowerWrapper {

    private String name;
    private String color;

    public FlowerWrapper(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return String.format("Wrapper: %s %s",name,color);
    }
}
