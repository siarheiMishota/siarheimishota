package tasks.module0.OOP.basicOfOOP.tast5;

public class Main {

    public static void main(String[] args) {


        FlowerService flowerService = new FlowerServiceImpl(new FlowerDAO().getFlowers());
        WrapperService wrapperService = new WrapperServiceImpl(new FlowerWrapperDAO().getFlowerWrappers());
        FlowerCompositionsService flowerCompositionsService = new FlowerCompositionsService();

        Action action = new Action(flowerService, wrapperService, flowerCompositionsService);

        action.execute();

    }
}
