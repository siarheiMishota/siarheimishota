package tasks.module0.OOP.basicOfOOP.tast5;

import java.util.ArrayList;
import java.util.List;

public class FlowerCompositionsService {

    private List<FlowerComposition> flowerCompositions = new ArrayList<>();

    public FlowerComposition createComposition(List<Flower> flowers, List<FlowerWrapper> wrappers) {

        if (flowers == null || wrappers == null) {
            throw new NullPointerException("Flowers or wrappers is null");
        }

        FlowerComposition creatingFlowerComposition = new FlowerComposition(flowers, wrappers);

        return creatingFlowerComposition;

    }

    public void addComposition(FlowerComposition addingFlowerComposition) {

        if (addingFlowerComposition == null) {
            throw new NullPointerException("Adding flower composition is null");
        }

        flowerCompositions.add(addingFlowerComposition);

    }

    public List<FlowerComposition> getFlowerCompositions() {
        return flowerCompositions;
    }
}
