package tasks.module0.OOP.basicOfOOP.tast5;

import java.util.List;

public interface FlowerService {

    Flower getByID(int id);
    List<Flower> getAll();
    Flower getByName(String name);
    void add(Flower addingFlower);

}
