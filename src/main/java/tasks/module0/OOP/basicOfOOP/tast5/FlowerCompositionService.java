package tasks.module0.OOP.basicOfOOP.tast5;


import java.util.ArrayList;
import java.util.List;

public class FlowerCompositionService {

    private List<Flower> flowers = new ArrayList<>();
    private List<FlowerWrapper> wrappers = new ArrayList<>();

    public void addFlower(Flower flower) {
        if (flower != null) {
            flowers.add(flower);
        }
    }

    public void addWrapper(FlowerWrapper wrapper) {

        if (wrapper != null) {
            wrappers.add(wrapper);
        }

    }

    public FlowerComposition createComposition() {

        if (flowers.isEmpty() || wrappers.isEmpty()) {
            throw new IllegalArgumentException("Flowers or wrappers isn't");
        }

        FlowerComposition flowerComposition = new FlowerComposition(flowers, wrappers);
        return flowerComposition;
    }


}
