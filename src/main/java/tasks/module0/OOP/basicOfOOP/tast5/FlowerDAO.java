package tasks.module0.OOP.basicOfOOP.tast5;

import java.util.ArrayList;
import java.util.List;

public class FlowerDAO {

    private List<Flower> flowers = new ArrayList<>();

    public FlowerDAO() {
        createFlowers();
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    private void createFlowers() {

        Flower flower = new Flower("Rose", "White", 10);
        flowers.add(flower);

        flower = new Flower("Rose", "Red", 10);
        flowers.add(flower);

        flower = new Flower("Rose", "Yellow", 10);
        flowers.add(flower);

        flower = new Flower("Rose", "Pink", 10);
        flowers.add(flower);

        flower=new Flower("Rose","Blue",10);
        flowers.add(flower);

        flower=new Flower("Rose","Light blue",10);
        flowers.add(flower);

        flower=new Flower("Tulips","Purple",10);
        flowers.add(flower);

        flower=new Flower("Tulips","White",10);
        flowers.add(flower);

        flower=new Flower("Tulips","Yellow",10);
        flowers.add(flower);

        flower=new Flower("Tulips","Red",10);
        flowers.add(flower);

        flower=new Flower("Tulips","Blue",10);
        flowers.add(flower);

        flower=new Flower("Lilies","Blue",10);
        flowers.add(flower);

        flower=new Flower("Lilies","White",10);
        flowers.add(flower);

        flower=new Flower("Lilies","Pink",10);
        flowers.add(flower);




    }
}
