package tasks.module0.OOP.basicOfOOP.tast5;

import tasks.module0.Service;

public class Action {

    private FlowerService flowerService;
    private WrapperService wrapperService;
    private FlowerCompositionsService flowerCompositionsService;

    public Action(FlowerService flowerService, WrapperService wrapperService, FlowerCompositionsService flowerCompositionsService) {
        this.flowerService = flowerService;
        this.wrapperService = wrapperService;
        this.flowerCompositionsService = flowerCompositionsService;
    }

    public void execute() {

        int choice;
        String infoMenu = "\nInput the number:\n" +
                "   1-add a composition\n" +
                "   2-get all compositions\n" +
                "   other- exit";


        while (true) {

            choice = Service.getPositiveIntValue(infoMenu);

            switch (choice) {
                case 1:
                    FlowerComposition composition = composition();
                    flowerCompositionsService.addComposition(composition);

                    break;
                case 2:
                    System.out.println(flowerCompositionsService.getFlowerCompositions());
                    break;
                default:
                    System.exit(0);
            }


        }

    }

    private FlowerComposition composition() {
        String infoComposition = "\nInput the number:\n" +
                "   1- add a flower\n" +
                "   2- add a wrapper\n" +
                "   3- create a composition\n" +
                "   other- back without creating composition";

        FlowerCompositionService flowerCompositionService = new FlowerCompositionService();

        int choice;

        while (true) {

            choice = Service.getPositiveIntValue(infoComposition);

            switch (choice) {
                case 1:

                    Flower flower = choiceFlower();
                    flowerCompositionService.addFlower(flower);
                    break;
                case 2:
                    FlowerWrapper flowerWrapper = choiceFlowerWrapper();
                    flowerCompositionService.addWrapper(flowerWrapper);
                    break;
                case 3:

                    return flowerCompositionService.createComposition();

                default:
                    return null;
            }
        }

    }



    private Flower choiceFlower() {

        for (int i = 0; i < flowerService.getAll().size(); i++) {

            System.out.format("%3d- %s\n", (i + 1), flowerService.getByID(i));

        }

        int choiceFlower;

        do {
            choiceFlower = Service.getPositiveIntValue("Input positive number till " + flowerService.getAll().size());
        } while (choiceFlower > flowerService.getAll().size());

        Flower selectedFlower = flowerService.getByID(choiceFlower - 1);

        return selectedFlower;

    }

    private FlowerWrapper choiceFlowerWrapper() {

        for (int i = 0; i < wrapperService.getAll().size(); i++) {

            System.out.format("%3d- %s\n", (i + 1), wrapperService.getById(i));

        }

        int choiceFlowerWrapper;

        do {
            choiceFlowerWrapper = Service.getPositiveIntValue("Input positive number till " + wrapperService.getAll().size());
        } while (choiceFlowerWrapper > wrapperService.getAll().size());

        FlowerWrapper selectedWrapper = wrapperService.getById(choiceFlowerWrapper - 1);

        return selectedWrapper;

    }


}
