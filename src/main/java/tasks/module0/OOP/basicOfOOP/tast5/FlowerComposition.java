package tasks.module0.OOP.basicOfOOP.tast5;

import java.util.List;

public class FlowerComposition {

    private List<Flower> flowers;
    private List<FlowerWrapper> flowerWrappers;

    public FlowerComposition(List<Flower> flowers, List<FlowerWrapper> flowerWrappers) {
        this.flowers = flowers;
        this.flowerWrappers = flowerWrappers;
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    public List<FlowerWrapper> getFlowerWrappers() {
        return flowerWrappers;
    }

    @Override
    public String toString() {
        return "\n" + flowers + "; " + flowerWrappers;
    }
}
