package tasks.module0.OOP.basicOfOOP.tast5;

import java.util.List;

public class WrapperServiceImpl implements WrapperService {

    private List<FlowerWrapper> flowerWrappers;

    public WrapperServiceImpl(List<FlowerWrapper> flowerWrappers) {
        this.flowerWrappers = flowerWrappers;
    }

    @Override
    public FlowerWrapper getById(int id) {
        return flowerWrappers.get(id);
    }

    @Override
    public void addFlowerWrapper(FlowerWrapper addingFlowerWrapper) {
        flowerWrappers.add(addingFlowerWrapper);
    }

    @Override
    public List<FlowerWrapper> getAll() {
        return flowerWrappers;
    }

    @Override
    public FlowerWrapper getByName(String name) {

        for (FlowerWrapper flowerWrapper : this.flowerWrappers) {

            if (flowerWrapper.getName().equalsIgnoreCase(name)) {
                return flowerWrapper;
            }

        }
        return null;
    }


}
