package tasks.module0.OOP.basicOfOOP.tast5;

import java.util.ArrayList;
import java.util.List;

public class FlowerWrapperDAO {

    private List<FlowerWrapper> flowerWrappers=new ArrayList<>();

    public FlowerWrapperDAO() {
        createFlowerWrappers();
    }

    public List<FlowerWrapper> getFlowerWrappers() {
        return flowerWrappers;
    }

    private void createFlowerWrappers() {

        FlowerWrapper flowerWrapper=new FlowerWrapper("Paper","Red");
        flowerWrappers.add(flowerWrapper);

        flowerWrapper=new FlowerWrapper("Paper","Black");
        flowerWrappers.add(flowerWrapper);

        flowerWrapper=new FlowerWrapper("Paper","White");
        flowerWrappers.add(flowerWrapper);

        flowerWrapper=new FlowerWrapper("Paper","Blue");
        flowerWrappers.add(flowerWrapper);

        flowerWrapper=new FlowerWrapper("Paper","Pink");
        flowerWrappers.add(flowerWrapper);

        flowerWrapper=new FlowerWrapper("Paper","Green");
        flowerWrappers.add(flowerWrapper);

        flowerWrapper=new FlowerWrapper("Paper","Purple");
        flowerWrappers.add(flowerWrapper);

        flowerWrapper=new FlowerWrapper("Film","Red");
        flowerWrappers.add(flowerWrapper);

        flowerWrapper=new FlowerWrapper("Film","Orange");
        flowerWrappers.add(flowerWrapper);

        flowerWrapper=new FlowerWrapper("Film","Grey");
        flowerWrappers.add(flowerWrapper);

        flowerWrapper=new FlowerWrapper("Film","Transparent");
        flowerWrappers.add(flowerWrapper);







    }
}
