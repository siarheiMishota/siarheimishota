package tasks.module0.OOP.basicOfOOP.tast5;

public class Flower {

    private String name;
    private String color;
    private int size;

    public Flower(String name, String color, int size) {
        this.name = name;
        this.color = color;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return String.format("Flower: %s %s, size=%3d",name,color,size);
    }
}
