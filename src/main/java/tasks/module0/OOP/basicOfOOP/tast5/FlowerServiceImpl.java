package tasks.module0.OOP.basicOfOOP.tast5;

import java.util.List;

public class FlowerServiceImpl implements FlowerService {

    private List<Flower> flowers;

    public FlowerServiceImpl(List<Flower> flowers) {
        this.flowers = flowers;
    }

    @Override
    public Flower getByID(int id) {
        return flowers.get(id);
    }

    @Override
    public void add(Flower addingFlower) {
        flowers.add(addingFlower);
    }

    @Override

    public List<Flower> getAll() {
        return flowers;
    }

    @Override
    public Flower getByName(String name) {

        for (Flower flower : flowers) {

            if (flower.getName().equalsIgnoreCase(name)) {
                return flower;
            }

        }

        return null;
    }
}
