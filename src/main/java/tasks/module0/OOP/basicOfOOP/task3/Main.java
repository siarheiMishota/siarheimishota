package tasks.module0.OOP.basicOfOOP.task3;

import tasks.module0.Service;

public class Main {

    public static void main(String[] args) {
        int year = Service.getPositiveIntValue("Input year for calendar");
        int firstDayOfWeekOfYear = Service.getPositiveIntValue("Input the day of week when 1 january");

        Calendar calendar = new Calendar(year, firstDayOfWeekOfYear);

        String infoChoice = "Choice:\n" +
                "1- next day\n" +
                "other- exit";

        int choice;

        while (true) {

            choice = Service.getPositiveIntValue(infoChoice);

            switch (choice) {
                case 1:
                    System.out.println(calendar.nextDay());
                    break;
                default:
                    System.exit(0);
            }
        }

    }
}
