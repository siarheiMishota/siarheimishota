package tasks.module0.OOP.basicOfOOP.task3;

public enum DayOfWeek {

    MONDAY(1),
    TUESDAY(2),
    WEDNESDAY(3),
    THURSDAY(4),
    FRIDAY(5),
    SATURDAY(6),
    SUNDAY(7);

    int number;

    DayOfWeek(int i) {
        number = i;
    }

    public static DayOfWeek nextDay(DayOfWeek dayOfWeek) {

        return getDay((dayOfWeek.number() + 1) % 7);


    }

    private int number() {
        return number;
    }

    public static DayOfWeek getDay(int number) {

        switch (number) {
            case 1:
                return MONDAY;
            case 2:
                return TUESDAY;
            case 3:
                return WEDNESDAY;
            case 4:
                return THURSDAY;
            case 5:
                return FRIDAY;
            case 6:
                return SATURDAY;
            default:
                return SUNDAY;
        }
    }
}
