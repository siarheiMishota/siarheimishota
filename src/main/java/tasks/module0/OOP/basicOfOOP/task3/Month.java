package tasks.module0.OOP.basicOfOOP.task3;

public enum Month {

    JAN("January", 1) {
        public int numberOfDays(int year) {
            return 31;
        }
    },
    FEB("February", 2) {
        public int numberOfDays(int year) {

            return isLeapYears(year) ? 29 : 28;
        }

        private boolean isLeapYears(int year) {
            return year % 4 == 0;
        }
    },
    MARCH("March", 3) {
        @Override
        int numberOfDays(int year) {
            return 31;
        }
    },
    APR("April", 4) {
        @Override
        int numberOfDays(int year) {
            return 30;
        }
    },
    MAY("May", 5) {
        @Override
        int numberOfDays(int year) {
            return 31;
        }
    },
    JUNE("June", 6) {
        @Override
        int numberOfDays(int year) {
            return 30;
        }
    },
    JULY("July", 7) {
        @Override
        int numberOfDays(int year) {
            return 31;
        }
    },

    AUG("August", 8) {
        @Override
        int numberOfDays(int year) {
            return 31;
        }
    },

    SEPT("September", 9) {
        @Override
        int numberOfDays(int year) {
            return 30;
        }
    },

    OCT("October", 10) {
        @Override
        int numberOfDays(int year) {
            return 31;
        }
    },

    NOV("November", 30) {
        @Override
        int numberOfDays(int year) {
            return 30;
        }
    },

    DEC("December", 12) {
        @Override
        int numberOfDays(int year) {
            return 31;
        }
    };

    private String fullName;
    private int numberOfMonth;

    Month(String fullName, int numberOfMonth) {
        this.fullName = fullName;
        this.numberOfMonth = numberOfMonth;
    }

    abstract int numberOfDays(int year);

    }
