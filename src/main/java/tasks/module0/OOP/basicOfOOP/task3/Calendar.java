package tasks.module0.OOP.basicOfOOP.task3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Calendar {

    private int year;
    private List<Day> days = new ArrayList<>();
    private Iterator<Day> iteratorDays;


    public Calendar(int year, int firstDayOfWeekOfYear) {
        this.year = year;
        init(year, firstDayOfWeekOfYear);
        this.iteratorDays=days.iterator();
    }

    private void init(int year, int firstDayOfWeekOfYear) {

        days = new ArrayList<>();
        DayOfWeek lastDay = DayOfWeek.getDay(firstDayOfWeekOfYear);
        int dayOfYear = 1;

        for (Month month : Month.values()) {

            for (int dayOfMonth = 1; dayOfMonth <= month.numberOfDays(year); dayOfMonth++) {

                boolean isWeekend = isWeekend(lastDay);
                Day day = new Day(dayOfMonth, month, lastDay, dayOfYear++, isWeekend);
                lastDay = DayOfWeek.nextDay(lastDay);

                days.add(day);

            }
        }


    }

    private boolean isWeekend(DayOfWeek lastDay) {
        return lastDay.equals(DayOfWeek.SUNDAY) || lastDay.equals(DayOfWeek.SATURDAY);
    }

    public String nextDay(){

        if (iteratorDays.hasNext()){
            Day next = iteratorDays.next();
            return next.toString();
        }

        return "year is over";


    }




    private class Day {

        private int dayOfMonth;
        private Month month;
        private DayOfWeek dayOfWeek;
        private int dayOfYear;
        private boolean isWeekend;

        public Day(int dayOfMonth, Month month, DayOfWeek dayOfWeek, int dayOfYear, boolean isWeekend) {
            this.month = month;
            this.dayOfWeek = dayOfWeek;
            this.dayOfYear = dayOfYear;
            this.dayOfMonth = dayOfMonth;
            this.isWeekend = isWeekend;
        }

        public DayOfWeek getDayOfWeek() {
            return dayOfWeek;
        }

        public int getDayOfYear() {
            return dayOfYear;
        }

        public Month getMonth() {
            return month;
        }

        public int getDayOfMonth() {
            return dayOfMonth;
        }

        @Override
        public String toString() {
            return String.format("%s, %d %s %d, %s",dayOfWeek,dayOfMonth,month,year,isWeekend?"weekend":"working day");
        }
    }
}
