package tasks.module0.OOP.basicOfOOP.task2;

import java.util.ArrayList;
import java.util.List;

import static tasks.module0.Service.*;

public class Main {

    public static void main(String[] args) {

        List<Payment> payments = new ArrayList<>();
        int choice;

        String infoMenu = "\nInput the number:\n" +
                "1-add the payment\n" +
                "2-add the product to the payment\n" +
                "3-print the payment\n" +
                "other- exit";


        while (true) {

            choice = getIntValue(infoMenu);
            switch (choice) {
                case 1:

                    Payment payment = new Payment(getStringLine("Input the name of payment"));

                    payments.add(payment);

                    break;

                case 2:

                    String productName = getStringLine("Input name of the product");
                    int cost = getPositiveIntValue("Input the cost of the product more 0");

                    Payment choicePayment = choicePayment(payments, "Choice payment");

                    choicePayment.addProduct(productName, cost);

                    break;

                case 3:

                    Payment choicePaymentCase3 = choicePayment(payments, "Choice payment");
                    System.out.println(choicePaymentCase3);
                    break;

                default:

                    System.exit(0);
                    break;


            }
        }


    }


    private static Payment choicePayment(List<Payment> payments, String message) {

        System.out.println(message);

        int i = 0;
        for (Payment payment : payments) {
            System.out.println((++i) + "- " + payment.getName());
        }

        int choice;
        do {
            choice = getPositiveIntValue("Choice the payment");
        } while (isIncorrectIndex(payments, choice));

        return payments.get(choice);


    }

    private static boolean isIncorrectIndex(List<Payment> payments, int choice) {
        return choice >= payments.size();
    }


}
