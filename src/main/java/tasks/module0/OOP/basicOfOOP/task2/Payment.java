package tasks.module0.OOP.basicOfOOP.task2;

import java.util.ArrayList;
import java.util.List;

public class Payment {

    static int generateID = 0;

    private int id = generateID++;
    private String name;
    private List<Product> products = new ArrayList<>();
    private int cost;


    private class Product {

        private String name;

        private int cost;


        public Product(String name, int cost) {

            this.name = name;
            this.cost = cost;

        }

        public String getName() {

            return this.name;

        }

        public int getCost() {

            return this.cost;

        }

    }

    public Payment(String name) {

        this.name = name;

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void addProduct(String productName, int productCost) {

        products.add(new Product(productName, productCost));
        cost += productCost;

    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();

        if (products.isEmpty()) {
            stringBuilder.append("В корзине ничего нет");
        } else {
            stringBuilder.append("++++++++++++++++++++++++++++++\n");
            stringBuilder.append(String.format("Id=%15s,  Name=%15s\n", id, name));

            for (Product product : products) {

                stringBuilder.append(String.format("%16s", product.name));
                stringBuilder.append(String.format("%6d\n", product.cost));

            }

            stringBuilder.append("Общая стоимость: ");
            stringBuilder.append(String.format("%11d\n", cost));
            stringBuilder.append("++++++++++++++++++++++++++++++\n");


        }

        return stringBuilder.toString();
    }


}
