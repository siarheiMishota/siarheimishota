package tasks.module0.OOP.elementaryClassesAndObjects.task8;

public class CustomerValidator {

    private boolean incorrectFirstName = false;
    private boolean incorrectLastName = false;
    private boolean incorrectPatronymic = false;
    private boolean incorrectAddress = false;
    private boolean incorrectCreditCardNumber = false;
    private boolean incorrectBankAccountNumber = false;
    private boolean unCondition = true;

    public boolean isIncorrectFirstName() {
        return incorrectFirstName;
    }

    public void setIncorrectFirstName(boolean incorrectFirstName) {
        this.incorrectFirstName = incorrectFirstName;
    }

    public boolean isIncorrectLastName() {
        return incorrectLastName;
    }

    public void setIncorrectLastName(boolean incorrectLastName) {
        this.incorrectLastName = incorrectLastName;
    }

    public boolean isIncorrectPatronymic() {
        return incorrectPatronymic;
    }

    public void setIncorrectPatronymic(boolean incorrectPatronymic) {
        this.incorrectPatronymic = incorrectPatronymic;
    }

    public boolean isIncorrectAddress() {
        return incorrectAddress;
    }

    public void setIncorrectAddress(boolean incorrectAddress) {
        this.incorrectAddress = incorrectAddress;
    }

    public boolean isIncorrectCreditCardNumber() {
        return incorrectCreditCardNumber;
    }

    public void setIncorrectCreditCardNumber(boolean incorrectCreditCardNumber) {
        this.incorrectCreditCardNumber = incorrectCreditCardNumber;
    }

    public boolean isIncorrectBankAccountNumber() {
        return incorrectBankAccountNumber;
    }

    public void setIncorrectBankAccountNumber(boolean incorrectBankAccountNumber) {
        this.incorrectBankAccountNumber = incorrectBankAccountNumber;
    }

    public boolean isUnCondition() {
        unCondition= incorrectFirstName | incorrectLastName | incorrectPatronymic | incorrectAddress | incorrectCreditCardNumber | incorrectBankAccountNumber;
        return unCondition;
    }

}
