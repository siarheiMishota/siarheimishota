package tasks.module0.OOP.elementaryClassesAndObjects.task8;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CustomerService {

    private List<Customer> customers=new ArrayList<>();

    public CustomerService() {
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void add(Customer customer) {
        customers.add(customer);
    }

    public void add(String firstName, String lastName, String patronymic,
                    String address, long creditCardNumber, long bankAccountNumber) {

        Customer customer = new Customer(firstName, lastName, patronymic, address, creditCardNumber, bankAccountNumber);
        customers.add(customer);
    }

    public void sortInAlphabetical() {
        customers.sort(Comparator.comparing(Customer::getLastName)
                .thenComparing(Customer::getFirstName)
                .thenComparing(Customer::getPatronymic)
                .thenComparing(Customer::getBankAccountNumber)
        );
    }

    public List<Customer> filterAtRangeBetweenNumbers(long startIncluding, long endNotIncluding) {

        List<Customer> filterCustomers = new ArrayList<>();




        for (Customer customer : customers) {
            if (customer.getCreditCardNumber() >= startIncluding &&
                    customer.getCreditCardNumber() < endNotIncluding) {
                filterCustomers.add(customer);
            }
        }


        return filterCustomers;

    }

}
