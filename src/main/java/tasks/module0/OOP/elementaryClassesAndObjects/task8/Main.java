package tasks.module0.OOP.elementaryClassesAndObjects.task8;

import java.util.List;

import static tasks.module0.Service.getLongValue;
import static tasks.module0.Service.getStringValueWithoutNumbers;

public class Main {

    public static void main(String[] args) {

        int choice;

        CustomerService customerService = new CustomerService();

        String infoChoice = "\nInput number:\n" +
                "1- create and add new customer\n" +
                "2- get all customers\n" +
                "3- sort customers in alphabetical order\n" +
                "4- filter customers by number of credit card\n" +
                "other- exit";

        String infoBeginningRange = "Input beginning of the range to filter";
        String infoEndRange = "Input end of the range to filter";


        while (true) {

            choice = (int) getLongValue(infoChoice);

            switch (choice) {
                case 1:

                    String paramFirstName;
                    String paramLastName;
                    String paramPatronymic;
                    String paramAddress;
                    long paramCreditCardNumber;
                    long paramBankAccountNumber;

                    do {

                        paramFirstName = getStringValueWithoutNumbers("Input first name customer");
                        paramLastName = getStringValueWithoutNumbers("Input last name customer");
                        paramPatronymic = getStringValueWithoutNumbers("Input patronymic customer");
                        paramAddress = getStringValueWithoutNumbers("Input address customer");
                        paramCreditCardNumber = getLongValue("Input credit card number customer");
                        paramBankAccountNumber = getLongValue("Input bank account number customer");

                    } while (validator(paramFirstName, paramLastName, paramPatronymic
                            , paramAddress, paramCreditCardNumber, paramBankAccountNumber).isUnCondition());

                    customerService.add(paramFirstName, paramLastName, paramPatronymic
                            , paramAddress, paramCreditCardNumber, paramBankAccountNumber);

                    break;

                case 2:
                    System.out.println("All customers:");
                    System.out.println(customerService.getCustomers());
                    break;

                case 3:
                    customerService.sortInAlphabetical();
                    System.out.println("Sorted");
                    break;

                case 4:
                    long beginningRange = getLongValue(infoBeginningRange);
                    long endRange = getLongValue(infoEndRange);

                    while (isInCorrectRange(beginningRange, endRange)) {
                        System.out.println("ERROR: beginning > end\n " +
                                "input right numbers");
                        beginningRange = getLongValue(infoBeginningRange);
                        endRange = getLongValue(infoEndRange);
                    }

                    System.out.println("sort customers in alphabetical order");

                    List<Customer> filteredCustomers = customerService.filterAtRangeBetweenNumbers(beginningRange, endRange);
                    System.out.println(filteredCustomers);
                    break;

                default:

                    System.exit(0);
                    break;


            }
        }


    }

    private static boolean isInCorrectRange(long startIncluding, long endNotIncluding) {

        return startIncluding > endNotIncluding;

    }

    private static CustomerValidator validator(String firstName, String lastName, String patronymic,
                                               String address, long creditCardNumber, long bankAccountNumber) {

        CustomerValidator customerValidator = new CustomerValidator();

        if (firstName == null) {
            customerValidator.setIncorrectFirstName(true);
        }

        if (lastName == null) {
            customerValidator.setIncorrectLastName(true);
        }

        if (patronymic == null) {
            customerValidator.setIncorrectPatronymic(true);
        }

        if (address == null) {
            customerValidator.setIncorrectAddress(true);
        }

        if (creditCardNumber <= 0) {
            customerValidator.setIncorrectCreditCardNumber(true);
        }

        if (bankAccountNumber <= 0) {
            customerValidator.setIncorrectBankAccountNumber(true);
        }

        return customerValidator;
    }


}
