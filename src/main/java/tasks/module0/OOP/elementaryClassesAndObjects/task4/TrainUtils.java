package tasks.module0.OOP.elementaryClassesAndObjects.task4;

import java.util.Arrays;
import java.util.Comparator;

public class TrainUtils {

    public static void sortByTrainNumber(Train[] trains) {
        Arrays.sort(trains, Comparator.comparingInt(Train::getTrainNumber));
    }

    public static void sortByDestination(Train[] trains) {
        Arrays.sort(trains, Comparator.comparing(Train::getNameOfTheDestination).thenComparing(Train::getTimeDeparture));
    }

    public static Train searchByTrainNumber(Train[] trains, int trainNumber) {
        for (int i = 0; i < trains.length; i++) {
            if (trains[i].getTrainNumber() == trainNumber) {
                return trains[i];
            }
        }
        return null;
    }
}
