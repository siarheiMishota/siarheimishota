package tasks.module0.OOP.elementaryClassesAndObjects.task4;

import java.time.LocalTime;
import java.util.Arrays;

import static tasks.module0.Service.getIntValue;
import static tasks.module0.Service.getRandomInt;
import static tasks.module0.OOP.elementaryClassesAndObjects.task4.TrainUtils.searchByTrainNumber;

public class Main {

    public static void main(String[] args) {

        int searching, countTrains = 20;

        Train[] trains = new Train[countTrains];

        for (int i = 0; i < trains.length; i++) {

            trains[i] = new Train("" + (char)('A'+getRandomInt(24)), getRandomInt(100_000),
                    LocalTime.of(getRandomInt(24), getRandomInt(60)));
        }

        System.out.println("Source array trains:");
        System.out.println(Arrays.toString(trains) + "\n");
        System.out.println("_________________________________________________________________________________________");

        System.out.println("Sorted by destination array trains:");
        TrainUtils.sortByDestination(trains);
        System.out.println(Arrays.toString(trains));
        System.out.println("_________________________________________________________________________________________");

        System.out.println("Sorted by train number array trains:");
        TrainUtils.sortByTrainNumber(trains);
        System.out.println(Arrays.toString(trains));
        System.out.println("_________________________________________________________________________________________");

        searching = getIntValue("Input number of train");
        System.out.println("\n" + searchByTrainNumber(trains, searching));


    }

}
