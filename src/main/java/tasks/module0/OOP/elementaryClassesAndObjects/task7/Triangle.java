package tasks.module0.OOP.elementaryClassesAndObjects.task7;

public class Triangle {

    private Point topA;
    private Point topB;
    private Point topC;

    private Triangle(Point topA, Point topB, Point topC) {
        this.topA = topA;
        this.topB = topB;
        this.topC = topC;
    }

    public static Triangle create(Point topA, Point topB, Point topC) {
        return new Triangle(topA, topB, topC);
    }

    public static Triangle create() {
        return create(new Point(0, 0), new Point(0, 3), new Point(4, 0));
    }

    public Point getTopA() {
        return topA;
    }

    public Point getTopB() {
        return topB;
    }

    public Point getTopC() {
        return topC;
    }
}
