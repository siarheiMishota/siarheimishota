package tasks.module0.OOP.elementaryClassesAndObjects.task7;

import tasks.module0.Service;

import static tasks.module0.Service.getIntValue;

public class Main {

    public static void main(String[] args) {

        int inner;
        Point pointA, pointB, pointC;

        Triangle triangle;

        String infoActive = "Input number:\n" +
                "1-get perimeter\n" +
                "2-get area\n" +
                "3-get point of intersection of medians\n" +
                "4-print points of triangle\n" +
                "other number-exit";

        do {
            int parAX = Service.getRandomInt(150);
            int parAY = Service.getRandomInt(150);
            pointA = new Point(parAX, parAY);

            int parBX = Service.getRandomInt(150);
            int parBY = Service.getRandomInt(150);
            pointB = new Point(parBX, parBY);

            int parCX = Service.getRandomInt(150);
            int parCY = Service.getRandomInt(150);
            pointC = new Point(parCX, parCY);

        } while (isLine(pointA, pointB, pointC));

        triangle = Triangle.create(pointA, pointB, pointC);
        TriangleService triangleService = new TriangleService(triangle);


        while (true) {
            inner = getIntValue(infoActive);
            switch (inner) {
                case 1:
                    System.out.format("Perimeter=%.2f\n", triangleService.perimeter());
                    break;
                case 2:
                    System.out.format("Area=%.2f\n", triangleService.area());
                    break;
                case 3:
                    System.out.format("Point of intersection of medians=%.2f\n", triangleService.pointOfIntersectionOfMedians().toString());
                    break;
                case 4:
                    System.out.println("\t" + pointA + "\n\t" + pointB + "\n\t" + pointC);
                    break;
                default:
                    System.exit(0);
            }
        }


    }

    private static boolean isLine(Point topA, Point topB, Point topC) {
        return (topC.getY() - topA.getY()) * (topB.getX() - topA.getX()) == (topB.getY() - topA.getY()) * (topC.getX() - topA.getX());
    }
}
