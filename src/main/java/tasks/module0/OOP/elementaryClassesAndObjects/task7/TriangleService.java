package tasks.module0.OOP.elementaryClassesAndObjects.task7;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class TriangleService {

    private Triangle triangle;
    private double sideA, sideB, sideC;

    public TriangleService(Triangle triangle) {
        this.triangle = triangle;
        calculateSides();
    }

    private void calculateSides() {

        sideA = side(triangle.getTopA(), triangle.getTopB());
        sideB = side(triangle.getTopB(), triangle.getTopC());
        sideC = side(triangle.getTopC(), triangle.getTopA());

    }

    private static double side(Point point1, Point point2) {
        return sqrt(pow((point1.getX() - point2.getX()), 2) + pow((point1.getY() - point2.getY()), 2));
    }




    private double halfPerimeter() {
        return perimeter() / 2;
    }

    public double perimeter() {
        return sideA + sideB + sideC;
    }

    public double area() {
        return sqrt(halfPerimeter() * (halfPerimeter() - sideA) *
                (halfPerimeter() - sideB) *
                (halfPerimeter() - sideC));
    }

    public Point pointOfIntersectionOfMedians() {
        Point pointCrossMedians = new Point((triangle.getTopA().getX() + triangle.getTopB().getX() + triangle.getTopC().getX()) / 3,
                (triangle.getTopA().getY() + triangle.getTopB().getY() + triangle.getTopC().getY()) / 3);

        return pointCrossMedians;
    }


}
