package tasks.module0.OOP.elementaryClassesAndObjects.task3;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Student[] students = new Student[10];
        Random random = new Random();

        for (int i = 0; i < students.length; i++) {

            students[i] = new Student("Student" + i, "S.A.", 513 + i,
                    new int[]{10, random.nextInt(10), 10, random.nextInt(10), 10});

        }

        for (Student student : students) {
            printEvolutionMore10(student);
        }
    }

    public static void printEvolutionMore10(Student student) {
        for (int learn : student.getLearningAchievement()) {
            if (learn != 9 && learn != 10) {
                return;
            }
        }

        System.out.println(student.getSurname() + "- " + student.getGroupNumber());
    }


}
