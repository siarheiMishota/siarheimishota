package tasks.module0.OOP.elementaryClassesAndObjects.task5;

public class CounterService {

    private Counter counter;

    public CounterService(Counter counter) {
        this.counter = counter;
    }

    public CounterService() {
        this.counter=new Counter();
    }

    public void increment() {

        counter.setValue(counter.getValue() + 1);

        if (counter.getValue() > counter.getUpperLimit()) {
            counter.setValue(counter.getUpperLimit());
        }
    }

    public void decrement() {
        counter.setValue(counter.getValue() - 1);

        if (counter.getValue() < counter.getLowerLimit()) {
            counter.setValue(counter.getLowerLimit());
        }
    }

    public int getValue(){
        return counter.getValue();
    }
}
