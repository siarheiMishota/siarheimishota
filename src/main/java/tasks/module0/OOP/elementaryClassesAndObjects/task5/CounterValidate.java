package tasks.module0.OOP.elementaryClassesAndObjects.task5;

public class CounterValidate {

    private boolean incorrectValue;
    private boolean incorrectLowerLimit;
    private boolean incorrectUpperLimit;
    private boolean unCondition;

    public CounterValidate() {

        this.incorrectValue =false;
        this.incorrectLowerLimit =false;
        this.incorrectUpperLimit =false;

    }

    public boolean isIncorrectValue() {
        return incorrectValue;
    }

    public boolean isIncorrectLowerLimit() {
        return incorrectLowerLimit;
    }

    public boolean isIncorrectUpperLimit() {
        return incorrectUpperLimit;
    }

    public void setIncorrectValue(boolean incorrectValue) {
        this.incorrectValue = incorrectValue;
    }

    public void setIncorrectLowerLimit(boolean incorrectLowerLimit) {
        this.incorrectLowerLimit = incorrectLowerLimit;
    }

    public void setIncorrectUpperLimit(boolean incorrectUpperLimit) {
        this.incorrectUpperLimit = incorrectUpperLimit;
    }

    public boolean isUnCondition() {

        unCondition = incorrectValue || incorrectLowerLimit || incorrectUpperLimit;

        return unCondition;
    }
}
