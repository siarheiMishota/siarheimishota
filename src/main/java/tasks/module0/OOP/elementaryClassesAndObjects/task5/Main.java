package tasks.module0.OOP.elementaryClassesAndObjects.task5;

import tasks.module0.Service;

public class Main {

    public static void main(String[] args) {
        int startValue, lowerLimit, upperLimit;
        CounterValidate validate;

        do {

            startValue = Service.getRandomInt(100);
            lowerLimit = Service.getRandomInt(20);
            upperLimit = Service.getRandomInt(100);

            validate = isCorrectData(startValue, lowerLimit, upperLimit);

        } while (validate.isUnCondition());


        Counter counter = new Counter(startValue, lowerLimit, upperLimit);
        CounterService counterService = new CounterService(counter);

        System.out.format("Value=%d \nLower limit=%d \nUpper limit=%d\n\n", startValue, lowerLimit, upperLimit);

        action(counterService);


    }


    private static CounterValidate isCorrectData(int currentValue, int lowerLimit, int upperLimit) {

        CounterValidate validate = new CounterValidate();

        if (lowerLimit > currentValue) {

            validate.setIncorrectValue(true);
            validate.setIncorrectLowerLimit(true);
        }

        if (upperLimit < currentValue) {
            validate.setIncorrectValue(true);
            validate.setIncorrectUpperLimit(true);
        }

        return validate;
    }

    private static void action(CounterService counterService) {
        while (true) {

            int action = Service.getIntValue("1- increment\n2- decrement\n3-print value\n4- exit");

            switch (action) {
                case 1:
                    counterService.increment();
                    break;
                case 2:
                    counterService.decrement();
                    break;
                case 3:
                    System.out.println(counterService.getValue());
                    break;
                default:
                    System.exit(0);
            }

        }
    }

}
