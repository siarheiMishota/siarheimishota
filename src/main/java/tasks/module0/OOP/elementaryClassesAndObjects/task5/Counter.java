package tasks.module0.OOP.elementaryClassesAndObjects.task5;

public class Counter {

    private int value;
    private int lowerLimit;
    private int upperLimit;

    public Counter(int value, int lowerLimit, int upperLimit) {

        this.value = value;
        this.lowerLimit = lowerLimit;
        this.upperLimit = upperLimit;
    }

    public Counter() {
        this(0, 0, 20);
    }

    public int getValue() {
        return value;
    }

    public int getLowerLimit() {
        return lowerLimit;
    }

    public int getUpperLimit() {
        return upperLimit;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
