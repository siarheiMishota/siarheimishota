package tasks.module0.OOP.elementaryClassesAndObjects.task10;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Set;

public class Airline {

    private String destination;
    private int flightNumber;
    private String typeOfAirline;
    private LocalTime timeOfDeparture;
    private Set<DayOfWeek> daysOfWeek;

    public Airline(String destination, int flightNumber, String typeOfAirline, LocalTime timeOfDeparture, Set<DayOfWeek> daysOfWeek) {
        this.destination = destination;
        this.flightNumber = flightNumber;
        this.typeOfAirline = typeOfAirline;
        this.timeOfDeparture = timeOfDeparture;


        this.daysOfWeek = daysOfWeek;

    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getTypeOfAirline() {
        return typeOfAirline;
    }

    public void setTypeOfAirline(String typeOfAirline) {
        this.typeOfAirline = typeOfAirline;
    }

    public LocalTime getTimeOfDeparture() {
        return timeOfDeparture;
    }

    public void setTimeOfDeparture(LocalTime timeOfDeparture) {
        this.timeOfDeparture = timeOfDeparture;
    }

    public Set<DayOfWeek> getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(Set<DayOfWeek> daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    @Override
    public String toString() {
        return "destination='" + destination + '\'' +
                ", flightNumber=" + flightNumber +
                ", typeOfAirline='" + typeOfAirline + '\'' +
                ", timeOfDeparture=" + timeOfDeparture +
                ", daysOfWeek=" + daysOfWeek + "\n";
    }


}
