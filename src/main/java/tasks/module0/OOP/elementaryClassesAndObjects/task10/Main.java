package tasks.module0.OOP.elementaryClassesAndObjects.task10;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;

import static tasks.module0.Service.*;

public class Main {

    public static void main(String[] args) {

        int choice;

        AirlineService airlineService = new AirlineService();

        String infoChoice = "\nInput number:\n" +
                "1- create and add new airline\n" +
                "2- get all airlines\n" +
                "3- list of airlines by specified destination\n" +
                "4- list of airlines by specified day of week\n" +
                "5- list of airlines by specified day of week after specified time\n" +
                "other- exit";


        while (true) {

            choice = getPositiveIntValue(infoChoice);

            switch (choice) {
                case 1:
                    String paramDestination = getStringValue("Input destination of airline");
                    int paramFlightNumber = getPositiveIntValue("Input flight number of airline");
                    String paramType = getStringValue("Input type of airlines");
                    LocalTime paramTimeOfDeparture = getLocalTime("Input time of departure airlines");

                    int paramCountDaysOfWeek;

                    do {
                        paramCountDaysOfWeek = getPositiveIntValue("Input day for filter");

                    } while (paramCountDaysOfWeek <= 0 || paramCountDaysOfWeek >= 8);

                    Set<DayOfWeek> paramDaysOfWeeks = getDaysOfWeek("Input day of week airline");


                    airlineService.add(new Airline(paramDestination, paramFlightNumber, paramType,
                            paramTimeOfDeparture, paramDaysOfWeeks));

                    break;

                case 2:
                    System.out.println("All airlines:");
                    System.out.println(airlineService.getAirlines());

                    break;

                case 3:


                    List<Airline> listOfFlightsForDestination = airlineService.listOfFlightsForDestination(getStringValueWithoutNumbers("Input destination for filter"));
                    System.out.println(listOfFlightsForDestination);
                    break;

                case 4:

                    int dayOfWeek;

                    do {
                        dayOfWeek = getPositiveIntValue("Input day for filter");
                    } while (dayOfWeek <= 0 || dayOfWeek >= 8);

                    List<Airline> listOfFlightsForDayOfWeek = airlineService.listOfFlightsForDayOfWeek(DayOfWeek.of(dayOfWeek));
                    System.out.println(listOfFlightsForDayOfWeek);

                    break;
                case 5:

                    int dayOfWeekWithTime;

                    do {
                        dayOfWeekWithTime = getPositiveIntValue("Input day for filter");
                    } while (dayOfWeekWithTime <= 0 || dayOfWeekWithTime >= 8);

                    LocalTime timeForSpecify = getLocalTime("Input time for filter");


                    List<Airline> listOfFlightsForDayOfWeekAfterSpecifyTime = airlineService.listOfFlightsForDayOfWeekAfterSpecifyTime(DayOfWeek.of(dayOfWeekWithTime), timeForSpecify);
                    System.out.println(listOfFlightsForDayOfWeekAfterSpecifyTime);
                    break;

                default:

                    System.exit(0);
                    break;


            }
        }


    }

}
