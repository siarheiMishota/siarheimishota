package tasks.module0.OOP.elementaryClassesAndObjects.task10;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class AirlineService {

    private List<Airline> airlines;
    private int count = 0;

    public AirlineService() {
        airlines = new ArrayList<>();
    }

    public List<Airline> getAirlines() {
        return airlines;
    }

    public void add(Airline airline) {
        if (airline != null) {
            airlines.add(airline);
        }
    }


    public List<Airline> listOfFlightsForDestination(String searchDestination) {

        List<Airline> searchedAirlines = new ArrayList<>();

        for (Airline airline : airlines) {

            if (airline.getDestination().equalsIgnoreCase(searchDestination)) {
                searchedAirlines.add(airline);
            }

        }

        return searchedAirlines;

    }

    public List<Airline> listOfFlightsForDayOfWeek(DayOfWeek searchDayOfWeek) {


        List<Airline> searchedAirlines = new ArrayList<>();

        for (Airline airline : airlines) {

            if (airline.getDaysOfWeek().contains(searchDayOfWeek)) {
                searchedAirlines.add(airline);
            }

        }

        return searchedAirlines;

    }

    public List<Airline> listOfFlightsForDayOfWeekAfterSpecifyTime(DayOfWeek searchDayOfWeek, LocalTime specifyTime) {


        List<Airline> searchedAirlines = new ArrayList<>();

        for (Airline airline : airlines) {

            if (isMoreThenSpecifyTimeAndDayOfWeek(searchDayOfWeek, specifyTime, airline)) {
                searchedAirlines.add(airline);
            }

        }

        return searchedAirlines;
    }

    private boolean isMoreThenSpecifyTimeAndDayOfWeek(DayOfWeek searchDayOfWeek, LocalTime specifyTime, Airline airline) {
        return airline.getDaysOfWeek().contains(searchDayOfWeek) && airline.getTimeOfDeparture().compareTo(specifyTime) <= 0;
    }


}
