package tasks.module0.OOP.elementaryClassesAndObjects.task6;

import tasks.module0.Service;

import static tasks.module0.Service.getIntValue;

public class Main {

    public static void main(String[] args) {

        int hour, minute, second;

        hour = Service.getRandomInt(34);
        minute = Service.getRandomInt(80);
        second = Service.getRandomInt(80);

        ClockValidate validate = validate(hour, minute, second);

        if (validate.isIncorrectHour()) {
            hour = 0;
            System.out.println("Incorrect hour, hour will be 0");
        }

        if (validate.isIncorrectMinute()) {
            minute = 0;
            System.out.println("Incorrect minute, minute will be 0");

        }

        if (validate.isIncorrectSecond()) {
            second = 0;
            System.out.println("Incorrect second, second will be 0");

        }

        Clock clock = new Clock(hour, minute, second);
        ClockService clockService = new ClockService(clock);

        int inner;
        String infoActive = "Input number:\n" +
                "1-change hours\n" +
                "2-change minutes\n" +
                "3-change seconds\n" +
                "4-print time\n" +
                "4-exit";


        while ((inner = getIntValue(infoActive)) >= 1 && inner <= 4) {
            switch (inner) {
                case 1:
                    int intervalHours = getIntValue("Input interval hours >0");

                    while (intervalHours < 0) {
                        intervalHours = getIntValue("Input interval hours >0");
                    }

                    clockService.changeToHoursInterval(intervalHours);
                    System.out.println(clockService.getClock());
                    break;

                case 2:
                    int intervalMinutes = getIntValue("Input interval minutes>0");

                    while (intervalMinutes < 0) {
                        intervalMinutes = getIntValue("Input interval minutes>0");

                    }

                    clockService.changeToMinutesInterval(intervalMinutes);
                    System.out.println(clockService.getClock());

                    break;

                case 3:
                    int intervalSeconds = getIntValue("Input interval seconds>0");

                    while (intervalSeconds < 0) {
                        intervalSeconds = getIntValue("Input interval seconds>0");

                    }

                    clockService.changeToSecondsInterval(intervalSeconds);
                    System.out.println(clockService.getClock());

                    break;

                case 4:
                    System.out.println(clockService.getClock());
                    break;
                default:
                    System.exit(0);
            }
        }


    }

    private static ClockValidate validate(int hour, int minute, int second) {

        ClockValidate validate = new ClockValidate();

        if (hour > 23) {
            validate.setIncorrectHour(true);
        }

        if (minute > 59) {
            validate.setIncorrectMinute(true);
        }

        if (second > 59) {
            validate.setIncorrectSecond(true);
        }

        return validate;

    }
}
