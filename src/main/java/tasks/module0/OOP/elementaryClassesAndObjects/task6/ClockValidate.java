package tasks.module0.OOP.elementaryClassesAndObjects.task6;

public class ClockValidate {

    private boolean incorrectHour;
    private boolean incorrectMinute;
    private boolean incorrectSecond;
    private boolean unCondition;

    public ClockValidate() {
        this.incorrectHour = false;
        this.incorrectMinute = false;
        this.incorrectSecond = false;
        this.unCondition = false;
    }

    public boolean isIncorrectHour() {
        return incorrectHour;
    }

    public void setIncorrectHour(boolean incorrectHour) {
        this.incorrectHour = incorrectHour;
    }

    public boolean isIncorrectMinute() {
        return incorrectMinute;
    }

    public boolean isUnCondition() {
        unCondition = incorrectHour || incorrectMinute || incorrectSecond;

        return unCondition;
    }

    public void setIncorrectMinute(boolean incorrectMinute) {
        this.incorrectMinute = incorrectMinute;
    }

    public boolean isIncorrectSecond() {
        return incorrectSecond;
    }

    public void setIncorrectSecond(boolean incorrectSecond) {
        this.incorrectSecond = incorrectSecond;
    }
}
