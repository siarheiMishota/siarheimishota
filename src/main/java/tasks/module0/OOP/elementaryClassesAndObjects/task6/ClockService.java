package tasks.module0.OOP.elementaryClassesAndObjects.task6;

public class ClockService {

    private Clock clock;

    public ClockService(Clock clock) {
        this.clock = clock;
    }

    public void changeToHoursInterval(int intervalHour) {
        int newHour=(clock.getHour() + intervalHour) % 24;
        clock.setHour(newHour);
    }

    public void changeToMinutesInterval(int intervalMinutes) {
        int newMinute = clock.getMinute() + intervalMinutes;

        if (newMinute >= 60) {
            changeToHoursInterval(newMinute / 60);
        }
        clock.setMinute( newMinute % 60);
    }

    public void changeToSecondsInterval(int intervalSeconds) {
        int newSecond = clock.getSecond() + intervalSeconds;

        if (newSecond >= 60) {
            changeToMinutesInterval(newSecond / 60);
        }
        clock.setSecond( newSecond % 60);
    }

    public Clock getClock() {
        return clock;
    }
}
