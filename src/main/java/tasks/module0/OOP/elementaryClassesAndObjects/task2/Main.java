package tasks.module0.OOP.elementaryClassesAndObjects.task2;

public class Main {

    public static void main(String[] args) {

        Test2 test1 = new Test2();
        Test2 test2 = new Test2(30, 50);

        System.out.println("test1.numberA= " + test1.getNumberA());
        System.out.println("test1.numberB= " + test1.getNumberB());

        test1.setNumberA(10);
        test1.setNumberB(20);

        System.out.println("test1.numberA= " + test1.getNumberA());
        System.out.println("test1.numberB= " + test1.getNumberB());

        System.out.println("test2.numberA= " + test2.getNumberA());
        System.out.println("test2.numberB= " + test2.getNumberB());


    }

}
