package tasks.module0.OOP.elementaryClassesAndObjects.task2;

public class Test2 {

    private int numberA;
    private int numberB;

    public Test2(int numberA, int numberB) {
        this.numberA = numberA;
        this.numberB = numberB;
    }

    public Test2() {
        this(2, 3);
    }

    public int getNumberA() {
        return numberA;
    }

    public void setNumberA(int numberA) {
        this.numberA = numberA;
    }

    public int getNumberB() {
        return numberB;
    }

    public void setNumberB(int numberB) {
        this.numberB = numberB;
    }

}
