package tasks.module0.OOP.elementaryClassesAndObjects.task9;

import java.util.ArrayList;
import java.util.List;

public class BookService {

    private List<Book> books;
    private int count = 0;

    public BookService() {
        books = new ArrayList<>();
    }

    public List<Book> getBooks() {
        return books;
    }

    public void add(Book book) {

        if (book != null) {
            books.add(book);
        }
    }


    public List<Book> listOfBooksByAuthor(String searchAuthor) {

        List<Book> searchedBooks = new ArrayList<>();

        for (Book book : books) {
            for (String author : book.getAuthors()) {
                if (author.equalsIgnoreCase(searchAuthor)) {
                    searchedBooks.add(book);
                }
            }
        }

        return searchedBooks;
    }

    public List<Book> listOfBooksByPublishing(String searchPublishing) {


        List<Book> searchedBooks = new ArrayList<>();

        for (Book book : books) {

            if (book.getPublishing().equalsIgnoreCase(searchPublishing)) {
                searchedBooks.add(book);
            }

        }

        return searchedBooks;

    }

    public List<Book> listOfBooksReleasedAfterSpecifiedYear(int specifiedYear) {

        List<Book> searchedBooks = new ArrayList<>();

        for (Book book : books) {

            if (book.getPublicationYear() == specifiedYear) {
                searchedBooks.add(book);
            }

        }

        return searchedBooks;

    }

}
