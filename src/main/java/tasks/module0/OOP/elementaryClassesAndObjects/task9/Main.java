package tasks.module0.OOP.elementaryClassesAndObjects.task9;

import java.util.ArrayList;
import java.util.List;

import static tasks.module0.Service.getIntValue;
import static tasks.module0.Service.getStringValueWithoutNumbers;

public class Main {

    public static void main(String[] args) {

        int choice;

        BookService booksService = new BookService();

        String infoChoice = "\nInput number:\n" +
                "1- create and add new book\n" +
                "2- get all books\n" +
                "3- list of books by specified author\n" +
                "4- list of books published by specified publishing\n" +
                "5- list of books published after specified year\n" +
                "other- exit";

        while (true) {

            choice = getIntValue(infoChoice);

            switch (choice) {
                case 1:
                    String paramTitle = getStringValueWithoutNumbers("Input title of book");
                    String paramPublishing = getStringValueWithoutNumbers("Input publishing book");
                    int paramPublishingYear = getIntValue("Input publishing year book");
                    int paramNumberOfPages = getIntValue("Input number pages by book");
                    int paramPrice = getIntValue("Input price book");
                    String paramTypeOfBinding = getStringValueWithoutNumbers("Input type of binding book");
                    int paramCountAuthors = getIntValue("Input number of authors");

                    List<String> paramAuthors = new ArrayList<>();

                    for (int i = 0; i < paramCountAuthors; i++) {
                        paramAuthors.add(getStringValueWithoutNumbers("" + "Input author of book " + i));

                    }

                    booksService.add(new Book(paramTitle, paramAuthors, paramPublishing, paramPublishingYear,
                            paramNumberOfPages, paramPrice, paramTypeOfBinding));

                    break;

                case 2:
                    System.out.println("All books:");
                    System.out.println(booksService.getBooks());

                    break;

                case 3:
                    List<Book> sortedBookByAuthor = booksService.listOfBooksByAuthor(getStringValueWithoutNumbers("Specify the author"));
                    System.out.println(sortedBookByAuthor);
                    break;

                case 4:
                    List<Book> sortedBookByPublishing = booksService.listOfBooksByPublishing(getStringValueWithoutNumbers("Specify the author"));
                    System.out.println(sortedBookByPublishing);
                    break;
                case 5:
                    List<Book> sortedBookReleasedAfterSpecifiedYear = booksService.listOfBooksReleasedAfterSpecifiedYear(getIntValue("Specify the published year"));
                    System.out.println(sortedBookReleasedAfterSpecifiedYear);

                default:

                    System.exit(0);
                    break;


            }
        }


    }

}
