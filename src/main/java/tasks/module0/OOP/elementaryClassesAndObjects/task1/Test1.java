package tasks.module0.OOP.elementaryClassesAndObjects.task1;

public class Test1 {

    public int numberA;
    public int numberB;


    public void print() {
        System.out.println("a= " + numberA + "\nb= " + numberB);
    }

    public int sum() {
        return numberA + numberB;
    }

    public int largestNumber() {
        return Math.max(numberA, numberB);
    }

}
