package tasks.module0.OOP.elementaryClassesAndObjects.task1;

public class Main {

    public static void main(String[] args) {

        Test1 test1 = new Test1();
        test1.numberA = 10;
        test1.numberB = 20;

        test1.print();
        System.out.println("sum= " + test1.sum());
        System.out.println("max= " + test1.largestNumber());
    }

}
