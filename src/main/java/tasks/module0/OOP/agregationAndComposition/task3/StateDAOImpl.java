package tasks.module0.OOP.agregationAndComposition.task3;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StateDAOImpl implements  StateDAO{

    public  State getState(Path pathSource) {

        City areaCenter, regionCenter;
        State state=null;
        List<Area> areaList = new ArrayList<>();

        Pattern patternArea = Pattern.compile("область");
        Pattern patternRegion = Pattern.compile("район");

        try (BufferedReader bufferedReader = Files.newBufferedReader(pathSource)) {

            String readLine, area, region;

            readLine = bufferedReader.readLine();

            String stateName = readLine.substring(0, readLine.indexOf(" "));
            String stateCapital = readLine.substring(readLine.indexOf(" "));


            while ((readLine = bufferedReader.readLine()) != null) {

                Matcher matcherArea;
                Matcher matcherRegion;

                readLine = readLine.trim();

                if ((matcherArea = patternArea.matcher(readLine)).find()) {

                    area = readLine.substring(0, matcherArea.start()).trim();
                    areaCenter = new City(readLine.substring(matcherArea.end()).trim());
                    areaList.add(new Area(area, areaCenter));

                } else if ((matcherRegion = patternRegion.matcher(readLine)).find()) {

                    region = readLine.substring(0, matcherRegion.start());
                    regionCenter = new City(readLine.substring(matcherRegion.end()).trim());
                    areaList.get(areaList.size() - 1).addRegion(new Region(region, regionCenter));

                } else {

                    areaList.get(areaList.size() - 1).getRegions().get(areaList.get(areaList.size() - 1).getRegions().size() - 1).addCity(new City(readLine.trim()));

                }


            }
            City city = new City(stateCapital);
            state = new State(stateName, areaList, city, 25);

        } catch (IOException e) {
            System.out.println("ERROR opening of file");
        }

        return state;
    }
}
