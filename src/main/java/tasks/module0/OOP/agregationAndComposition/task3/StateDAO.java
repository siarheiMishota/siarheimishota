package tasks.module0.OOP.agregationAndComposition.task3;

import java.nio.file.Path;

public interface StateDAO {

    State getState(Path  path);

}
