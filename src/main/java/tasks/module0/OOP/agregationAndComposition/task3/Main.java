package tasks.module0.OOP.agregationAndComposition.task3;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) {

        Path pathSource = Paths.get("src/main/java/siarheiMishota/jwd/tasks/module0/OOP/agregationAndComposition/task3/source.txt");

        State state= new StateDAOImpl().getState(pathSource);

        System.out.println("Capital- " + state.getCapital());
        System.out.println("Count areas= " + state.getCountAreas());
        System.out.println("Area of state= " + state.getArea());
        System.out.println("Area centers: " + state.getAreaCenters());







    }

}
