package tasks.module0.OOP.agregationAndComposition.task4;

import tasks.module0.Service;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<CustomerService> customerServices = new ArrayList<>();
        CustomerService customerService;
        DAO dao = new DAO();
        while ((customerService = dao.getCustomerService()) != null) {

            customerServices.add(customerService);

        }

        for (CustomerService customerServiceInCycle : customerServices) {
            System.out.println("\n" + customerServiceInCycle.getCustomer().getLastName() + " " + customerServiceInCycle.getCustomer().getFirstName());

            customerServiceInCycle.sortById();

            System.out.println("General sum- " + customerServiceInCycle.sumGeneral());
            System.out.println("Positive sum- " + customerServiceInCycle.sumPositive());
            System.out.println("Negative sum- " + customerServiceInCycle.sumNegative());

        }

        CustomerService customerServiceSearching = getCustomerService(customerServices);

        int searchingAccountID = Service.getPositiveIntValue("Input id of account");
        Account searched = customerServiceSearching.searchAccountById(searchingAccountID);
        System.out.println(searched);


    }

    private static CustomerService getCustomerService(List<CustomerService> customerServices) {

        int counter = 1;

        for (CustomerService customerService : customerServices) {

            String firstName = customerService.getCustomer().getFirstName();
            String lastName = customerService.getCustomer().getLastName();

            System.out.format("%2d- %s %s", counter++, firstName, lastName);

        }

        counter--;
        int choice;

        do {
            choice = Service.getPositiveIntValue("Input ID of customer");
        } while (choice > counter);

        return customerServices.get(choice);
    }

}
