package tasks.module0.OOP.agregationAndComposition.task4;

import java.util.Comparator;
import java.util.List;

public class CustomerService {

    private Customer customer;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public CustomerService(Customer customer) {
        this.customer = customer;
    }

    public void addAccount(Account account) {

        customer.getAccounts().add(account);
    }

    public void addAccounts(List<Account> account) {

        customer.getAccounts().addAll(account);

    }

    public List<Account> getAccounts() {
        return customer.getAccounts();
    }

    public double sumGeneral() {
        double sum = 0;

        for (Account account : customer.getAccounts()) {
            if (!account.isLock()) {
                sum += account.getBalance();
            }
        }

        return sum;
    }

    public double sumPositive() {
        double sum = 0;

        for (Account account : customer.getAccounts()) {
            if (!account.isLock() && account.getBalance() > 0) {
                sum += account.getBalance();
            }
        }

        return sum;

    }

    public double sumNegative() {
        double sum = 0;

        for (Account account : customer.getAccounts()) {
            if (!account.isLock() && account.getBalance() < 0) {
                sum += account.getBalance();
            }
        }

        return sum;

    }

    public Account searchAccountById(long id) {
        for (Account account : customer.getAccounts()) {
            if (account.getId() == id) {
                return account;
            }
        }
        return null;
    }

    public void sortById() {
        customer.getAccounts().sort(Comparator.comparing(Account::getId));

    }

}
