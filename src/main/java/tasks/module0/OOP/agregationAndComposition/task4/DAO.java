package tasks.module0.OOP.agregationAndComposition.task4;

import java.util.*;

public class DAO {

    private Map<Integer, CustomerService> customerServiceMap = new HashMap<>();
    private Iterator<Map.Entry<Integer, CustomerService>> iteratorCustomerService;

    public DAO() {
        createCustomerServices();
        iteratorCustomerService = customerServiceMap.entrySet().iterator();
    }

    public CustomerService getCustomerService() {

        if (iteratorCustomerService.hasNext()) {
            return iteratorCustomerService.next().getValue();
        }

        return null;

    }

    private void createCustomerServices() {

        Customer customer;
        CustomerService customerService;
        List<Account> accounts;

        customer = new Customer("Ivan", "Petrov");
        customerService = new CustomerService(customer);
        accounts = new ArrayList<>();
        accounts.add(new Account(111, false));
        accounts.add(new Account(112, false));
        accounts.add(new Account(113, false));
        accounts.add(new Account(114, false));
        customerService.addAccounts(accounts);
        customerServiceMap.put(0, new CustomerService(customer));

        customer = new Customer("Sergei", "Petrov");
        customerService = new CustomerService(customer);
        accounts = new ArrayList<>();
        accounts.add(new Account(211, false));
        accounts.add(new Account(-212, false));
        accounts.add(new Account(213, true));
        accounts.add(new Account(-214, false));
        customerService.addAccounts(accounts);
        customerServiceMap.put(1, new CustomerService(customer));

        customer = new Customer("Senya", "Petrov");
        customerService = new CustomerService(customer);
        accounts = new ArrayList<>();
        accounts.add(new Account(311, false));
        accounts.add(new Account(312, false));
        accounts.add(new Account(-313, false));
        accounts.add(new Account(314, true));
        customerService.addAccounts(accounts);
        customerServiceMap.put(2, new CustomerService(customer));

        customer = new Customer("Aleksey", "Petrov");
        customerService = new CustomerService(customer);
        accounts = new ArrayList<>();
        accounts.add(new Account(411, false));
        accounts.add(new Account(-412, false));
        accounts.add(new Account(413, false));
        accounts.add(new Account(414, false));
        customerService.addAccounts(accounts);
        customerServiceMap.put(3, new CustomerService(customer));

        customer = new Customer("Ivan", "Sidorov");
        customerService = new CustomerService(customer);
        accounts = new ArrayList<>();
        accounts.add(new Account(-511, false));
        accounts.add(new Account(512, false));
        accounts.add(new Account(513, false));
        accounts.add(new Account(514, true));
        customerService.addAccounts(accounts);
        customerServiceMap.put(4, new CustomerService(customer));

        customer = new Customer("Fedya", "Petrov");
        customerService = new CustomerService(customer);
        accounts = new ArrayList<>();
        accounts.add(new Account(-611, true));
        accounts.add(new Account(612, false));
        accounts.add(new Account(613, false));
        accounts.add(new Account(614, false));
        customerService.addAccounts(accounts);
        customerServiceMap.put(5, new CustomerService(customer));

        customer = new Customer("Vanya", "Petrov");
        customerService = new CustomerService(customer);
        accounts = new ArrayList<>();
        accounts.add(new Account(711, false));
        accounts.add(new Account(712, false));
        accounts.add(new Account(713, false));
        accounts.add(new Account(714, false));
        customerService.addAccounts(accounts);
        customerServiceMap.put(6, new CustomerService(customer));

        customer = new Customer("Danya", "Petrov");
        customerService = new CustomerService(customer);
        accounts = new ArrayList<>();
        accounts.add(new Account(811, false));
        accounts.add(new Account(812, false));
        accounts.add(new Account(813, false));
        accounts.add(new Account(814, false));
        customerService.addAccounts(accounts);
        customerServiceMap.put(7, new CustomerService(customer));

        customer = new Customer("Andrey", "Petrov");
        customerService = new CustomerService(customer);
        accounts = new ArrayList<>();
        accounts.add(new Account(911, false));
        accounts.add(new Account(912, false));
        accounts.add(new Account(913, false));
        accounts.add(new Account(914, false));
        customerService.addAccounts(accounts);
        customerServiceMap.put(8, new CustomerService(customer));

        customer = new Customer("Igor", "Petrov");
        customerService = new CustomerService(customer);
        accounts = new ArrayList<>();
        accounts.add(new Account(1011, false));
        accounts.add(new Account(1012, false));
        accounts.add(new Account(1013, false));
        accounts.add(new Account(-1014, false));
        customerService.addAccounts(accounts);
        customerServiceMap.put(9, new CustomerService(customer));

        customer = new Customer("Stas", "Petrov");
        customerService = new CustomerService(customer);
        accounts = new ArrayList<>();
        accounts.add(new Account(1111, false));
        accounts.add(new Account(1112, false));
        accounts.add(new Account(1113, false));
        accounts.add(new Account(1114, false));
        customerService.addAccounts(accounts);
        customerServiceMap.put(10, new CustomerService(customer));


    }


}
