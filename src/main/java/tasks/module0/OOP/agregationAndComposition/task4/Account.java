package tasks.module0.OOP.agregationAndComposition.task4;

import java.util.Objects;

public class Account {

    private static long idGenerate=0;

    private long id=idGenerate++;
    private double balance;
    private boolean lock;

    public Account(double balance, boolean lock) {
        this.balance = balance;
        this.lock = lock;
    }



    public long getId() {
        return id;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public boolean isLock() {
        return lock;
    }

    public void setLock(boolean lock) {
        this.lock = lock;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", balance=" + balance +
                ", lock=" + lock +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id &&
                Double.compare(account.balance, balance) == 0 &&
                lock == account.lock;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, balance, lock);
    }
}
