package tasks.module0.OOP.agregationAndComposition.task1;

import java.util.ArrayList;
import java.util.List;

public class Text {

    private List<Sentence> sentences;
    private String title = "Book";

    public Text(List<Sentence> sentences) {
        this.sentences = sentences;
    }

    public Text(String title) {
        sentences = new ArrayList<>();
        this.title = title;
    }


    public List<Sentence> getSentences() {
        return sentences;
    }

    public void setSentences(List<Sentence> sentences) {
        this.sentences = sentences;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();

        for (Sentence sentence : sentences) {
            stringBuilder.append(sentence);
            stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(" "));
            stringBuilder.append(".");
        }

        return stringBuilder.toString();

    }





}
