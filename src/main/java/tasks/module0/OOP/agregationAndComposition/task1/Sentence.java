package tasks.module0.OOP.agregationAndComposition.task1;

import java.util.ArrayList;
import java.util.List;

public class Sentence {

    private List<Word> words = new ArrayList<>();

    public Sentence(List<Word> innerSentence) {
        this.words = innerSentence;
    }

    public Sentence() {

    }

    public List<Word> getWords() {
        return words;
    }


    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < words.size(); i++) {
            stringBuilder.append(words.get(i)).append(" ");
        }

        return stringBuilder.toString();
    }


}
