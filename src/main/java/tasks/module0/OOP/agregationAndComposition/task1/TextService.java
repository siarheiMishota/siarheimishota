package tasks.module0.OOP.agregationAndComposition.task1;

public class TextService {

    private Text text;

    public TextService(Text text) {
        this.text = text;
    }

    public Text getText() {
        return text;
    }

    public void setText(Text text) {
        this.text = text;
    }

    public void addSentence(Sentence sentence) {

        if (sentence != null) {
            text.getSentences().add(sentence);

        }

    }
}
