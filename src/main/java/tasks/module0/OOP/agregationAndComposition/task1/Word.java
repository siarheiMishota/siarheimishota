package tasks.module0.OOP.agregationAndComposition.task1;

public class Word {


    private String value;

    public Word(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }


    @Override
    public String toString() {
        return value;
    }


}
