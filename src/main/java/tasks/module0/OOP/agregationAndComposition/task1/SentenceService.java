package tasks.module0.OOP.agregationAndComposition.task1;

import java.util.List;

public class SentenceService {

    private Sentence sentence;

    public SentenceService(Sentence sentence) {
        this.sentence = sentence;
    }

    public void addWord(Word word) {

        if (word != null) {
            sentence.getWords().add(word);
        }
    }

    public void addWords(List<Word> words) {

        if (words != null) {

            sentence.getWords().addAll(words);

        }

    }
}
