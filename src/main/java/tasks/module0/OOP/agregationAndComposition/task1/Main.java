package tasks.module0.OOP.agregationAndComposition.task1;

import static tasks.module0.Service.getIntValue;
import static tasks.module0.Service.getStringValueWithoutNumbers;

public class Main {

    public static void main(String[] args) {

        int choice;
        String infoTitle = "Input title the text";
        String infoMenu = "\nInput the number:\n" +
                "1-add sentence\n" +
                "2-print text\n" +
                "3-print title\n" +
                "other- exit";
        String infoCountWordsInSentence = "Input the number of words";
        String infoWord = "Input the word";

        String title = getStringValueWithoutNumbers(infoTitle);
        Text text = new Text(title);
        TextService textService = new TextService(text);

        while (true) {

            choice = getIntValue(infoMenu);

            switch (choice) {
                case 1:
                    int count = getIntValue(infoCountWordsInSentence);

                    Sentence sentence = new Sentence();
                    SentenceService sentenceService = new SentenceService(sentence);

                    for (int i = 0; i < count; i++) {

                        Word word = new Word(getStringValueWithoutNumbers(infoWord));
                        sentenceService.addWord(word);
                    }

                    textService.addSentence(sentence);


                    break;

                case 2:
                    System.out.println(text);

                    break;

                case 3:

                    System.out.println(text.getTitle());
                    break;

                default:

                    System.exit(0);
                    break;


            }
        }


    }

}
