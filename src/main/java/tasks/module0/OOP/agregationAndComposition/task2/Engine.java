package tasks.module0.OOP.agregationAndComposition.task2;

public class Engine {

    private String name;
    private double volume;
    private int power;
    private double fuelConsumption;

    public Engine(String name, double volume, int power, double fuelConsumption) {
        this.name = name;
        this.volume = volume;
        this.power = power;
        this.fuelConsumption = fuelConsumption;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public double getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(double fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    @Override
    public String toString() {
        return "name: " + name +
                ", volume: " + volume +
                ", power: " + power +
                ", fuelConsumption: " + fuelConsumption;
    }

}
