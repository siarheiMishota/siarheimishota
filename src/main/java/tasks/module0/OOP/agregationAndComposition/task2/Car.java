package tasks.module0.OOP.agregationAndComposition.task2;

import java.util.List;

public class Car {

    private String name;
    private String brand;
    private double volumeOfTank;
    private double litersInTank;
    private Engine engine;
    private List<Wheel> wheels;

    public Car(String name, String brand, double volumeOfTank, double litersInTank, Engine engine, List<Wheel> wheels) {
        this.name = name;
        this.brand = brand;
        this.volumeOfTank = volumeOfTank;
        this.litersInTank = litersInTank;
        this.engine = engine;
        this.wheels = wheels;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getVolumeOfTank() {
        return volumeOfTank;
    }

    public void setVolumeOfTank(double volumeOfTank) {
        this.volumeOfTank = volumeOfTank;
    }

    public double getLitersInTank() {
        return litersInTank;
    }

    public void setLitersInTank(double litersInTank) {
        this.litersInTank = litersInTank;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public List<Wheel> getWheels() {
        return wheels;
    }

    public void setWheels(List<Wheel> wheels) {
        this.wheels = wheels;
    }

    public void drive() {

        litersInTank-=engine.getFuelConsumption();


    }

    public void refuel(double liters) {

        litersInTank+=liters;
    }

    public void replaceWheel(int replacingWheel, Wheel newWheel) {


        switch (replacingWheel) {
            case 1:

                wheels.set(0, newWheel);
                break;
            case 2:

                wheels.set(1, newWheel);
                break;
            case 3:
                wheels.set(2, newWheel);
                break;
            default:

                wheels.set(3, newWheel);
                break;
        }

    }

    @Override
    public String toString() {
        return "Car\n" +
                "name: " + name +
                ", brand: " + brand +
                ", volumeOfTank: " + volumeOfTank +
                ", litersInTank: " + litersInTank +
                ", \nengine: " + engine +
                ", \nwheels: " + wheels;
    }


}
