package tasks.module0.OOP.agregationAndComposition.task2;

public class Wheel {

    private String name;
    private String brand;
    private double diameter;


    public Wheel(String name, String brand, double diameter) {
        this.name = name;
        this.brand = brand;
        this.diameter = diameter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getDiameter() {
        return diameter;
    }

    public void setDiameter(double diameter) {
        this.diameter = diameter;
    }

    @Override
    public String toString() {
        return "brand: " + brand +
                ", name: " + name +
                ", diameter: " + diameter;
    }


}
