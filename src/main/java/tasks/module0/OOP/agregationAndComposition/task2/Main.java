package tasks.module0.OOP.agregationAndComposition.task2;

import java.util.ArrayList;
import java.util.List;

import static tasks.module0.Service.*;

public class Main {

    public static void main(String[] args) {

        String infoMenu = "\nInput the number:\n" +
                "1-drive\n" +
                "2-refuel\n" +
                "3-replace wheel\n" +
                "4-get brand the car\n" +
                "5-get the car\n" +
                "6-fuel in the tank\n" +
                "other- exit";


//        String parCarName = getStringValueWithoutNumbers("Input name of car");
//        String parCarBrand = getStringValueWithoutNumbers("Input brand of car");
//        double parCarVolumeOfTanks = getDoubleValue("Input volume of tanks of car");
//        double parCarLitersInTanks = getDoubleValue("Input liters in tanks of car less then volume of tanks");
//
//        String parEngineName = getStringValueWithoutNumbers("Input name of engine");
//        double parEngineVolume = getDoubleValue("Input volume of engine");
//        int parEnginePower = getIntValue("Input power of engine");
//        double parEngineFuelConsumption = getDoubleValue("Input number fuel consumption of engine");
//
//        int paramNumberOfWheels = getIntValue("Input number of wheels");
//        String paramWheelsName = getStringValueWithoutNumbers("Input name of wheels");
//        String paramWheelsBrand = getStringValueWithoutNumbers("Input brand  of wheels");
//        double paramWheelsDiameter = getDoubleValue("Input diameter of wheels");

        String parCarName = "a6";
        String parCarBrand = "audi";
        int parCarVolumeOfTanks = 60;
        double parCarLitersInTanks = 25;

        String parEngineName = "BFT";
        double parEngineVolume = 3.0;
        int parEnginePower = 250;
        double parEngineFuelConsumption = 10;

        int paramNumberOfWheels = 4;
        String paramWheelsName = "Summer";
        String paramWheelsBrand = "Michlen";
        double paramWheelsDiameter = 15;

        List<Wheel> wheels = new ArrayList<>();

        for (int i = 0; i < paramNumberOfWheels; i++) {
            Wheel wheel = new Wheel(paramWheelsName, paramWheelsBrand, paramWheelsDiameter);

            wheels.add(wheel);

        }

        Engine engine = new Engine(parEngineName, parEngineVolume, parEnginePower, parEngineFuelConsumption);
        Car car = new Car(parCarName, parCarBrand, parCarVolumeOfTanks, parCarLitersInTanks, engine, wheels);

        int choice;

        while (true) {

            choice = getIntValue(infoMenu);

            switch (choice) {
                case 1:

                    car.drive();

                    if (car.getLitersInTank() <= 0) {
                        System.out.println("Tank is empty, to refuel");
                        car.setLitersInTank(0);
                    } else {
                        System.out.println("You drove an hour, congratulation");
                    }


                    break;

                case 2:
                    int refuel = getIntValue("Input number liters of fuel for refuel");

                    car.refuel(refuel);

                    if (car.getLitersInTank() > car.getVolumeOfTank()) {
                        double refueledInFact = refuel - (car.getLitersInTank() - car.getVolumeOfTank());
                        car.setLitersInTank(car.getVolumeOfTank());
                        System.out.println("You refuel " + refueledInFact + " and tank is full");
                    } else {
                        System.out.println("You refuel, congratulation");

                    }

                    break;

                case 3:

                    String infoReplacing = "\nInput the number:\n" +
                            "1-front left wheel\n" +
                            "2-front right wheel\n" +
                            "3-rear left wheel\n" +
                            "other- rear right";

                    int replacingWheel = getIntValue(infoReplacing);

                    String paramWheelName = getStringValueWithoutNumbers("Input name of wheel");
                    String paramWheelBrand = getStringValueWithoutNumbers("Input brand of wheel");
                    double paramWheelDiameter = getDoubleValue("Input diameter of wheel");

                    Wheel newWheel = new Wheel(paramWheelName, paramWheelBrand, paramWheelDiameter);
                    car.replaceWheel(replacingWheel, newWheel);

                    System.out.println("Replacing");

                    break;
                case 4:

                    System.out.println("Brand: " + car.getBrand());
                    break;
                case 5:
                    System.out.println("Car:\n" + car);
                    break;
                case 6:
                    System.out.println("Litters in tank= " + car.getLitersInTank());
                    break;

                default:

                    System.exit(0);
                    break;


            }
        }

    }


}
