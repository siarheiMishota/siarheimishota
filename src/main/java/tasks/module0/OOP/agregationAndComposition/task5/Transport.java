package tasks.module0.OOP.agregationAndComposition.task5;

import java.util.Objects;

public class Transport {

    private String name;
    private double coefficient;

    public Transport(String name, double coefficient) {
        this.name = name;
        this.coefficient = coefficient;
    }

    public String getName() {
        return name;
    }

    public double getCoefficient() {
        return coefficient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transport)) return false;
        Transport transport = (Transport) o;
        return Double.compare(transport.coefficient, coefficient) == 0 &&
                Objects.equals(name, transport.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, coefficient);
    }

    @Override
    public String toString() {
        return "Transport" + name;
    }
}
