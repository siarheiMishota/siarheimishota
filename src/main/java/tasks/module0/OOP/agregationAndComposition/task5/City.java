package tasks.module0.OOP.agregationAndComposition.task5;

import java.util.Objects;

public class City {

    private String name;
    private Country country;
    private double coefficient;

    public City(String name, Country country, double coefficient) {
        this.name = name;
        this.country = country;
        this.coefficient = coefficient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public double getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(double coefficient) {
        this.coefficient = coefficient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof City)) return false;
        City city = (City) o;
        return Double.compare(city.coefficient, coefficient) == 0 &&
                Objects.equals(name, city.name) &&
                Objects.equals(country, city.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, country, coefficient);
    }

    @Override
    public String toString() {
        return "City" + name + ", country=" + country;
    }
}
