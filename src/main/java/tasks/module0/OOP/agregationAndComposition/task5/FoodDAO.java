package tasks.module0.OOP.agregationAndComposition.task5;

import java.util.HashMap;
import java.util.Map;

public class FoodDAO {

    private Map<Integer, Food> foods = new HashMap<>();

    public FoodDAO() {

        create();

    }

    public Map<Integer, Food> getFoods() {
        return foods;
    }

    public Food getByKey(int key) {

        return foods.get(key);

    }

    private void create() {

        int counter=0;

        Food food = new Food("without meals", 0.1);
        foods.put(counter++, food);

        food = new Food("breakfast", 0.5);
        foods.put(counter++, food);

        food = new Food("breakfast and dinner", 1.0);
        foods.put(counter++, food);

        food = new Food("breakfast and lunch and dinner", 1.3);
        foods.put(counter++, food);

        food = new Food("all inclusive", 2.5);
        foods.put(counter++, food);

    }



}
