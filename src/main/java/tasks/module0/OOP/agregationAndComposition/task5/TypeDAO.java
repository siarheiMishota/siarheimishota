package tasks.module0.OOP.agregationAndComposition.task5;

import java.util.HashMap;
import java.util.Map;

public class TypeDAO {

    private Map<Integer, Type> typeMap = new HashMap<>();

    public TypeDAO() {
        create();
    }

    public Map<Integer, Type> getTypeMap() {
        return typeMap;
    }

    public Type getByKey(int key){

        return typeMap.get(key);

    }

    private void create(){

        int counter=0;
        Type type=new Type("REST",1.5);
        typeMap.put(counter++,type);

        type=new Type("EXCURSIONS",1.8);
        typeMap.put(counter++,type);

        type=new Type("TREATMENT",4);
        typeMap.put(counter++,type);

        type=new Type("SHOPPING",1.1);
        typeMap.put(counter++,type);

        type=new Type("CRUISE",2.5);
        typeMap.put(counter++,type);

    }

}
