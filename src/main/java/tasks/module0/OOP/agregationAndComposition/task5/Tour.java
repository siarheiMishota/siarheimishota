package tasks.module0.OOP.agregationAndComposition.task5;

import java.util.Objects;

public class Tour {

    private static int generateID = 0;

    private int id = generateID++;
    private String name;
    private City city;
    private Type type;
    private Transport transports;
    private Food foods;
    private int countDay;
    private double totalCost;

    public Tour(String name, City city, Type type, Transport transports, Food foods, int countDay, int startCost) {
        this.name = name;
        this.city = city;
        this.type = type;
        this.transports = transports;
        this.foods = foods;
        this.countDay = countDay;
        calculateCost(startCost);
    }

    private void calculateCost(int startCost) {

        totalCost = 1.0 * startCost * city.getCoefficient() * city.getCountry().getCoefficient()
                * type.getCoefficient() * transports.getCoefficient() * foods.getCoefficient()
                * countDay;


    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Transport getTransports() {
        return transports;
    }

    public void setTransports(Transport transports) {
        this.transports = transports;
    }

    public Food getFoods() {
        return foods;
    }

    public void setFoods(Food foods) {
        this.foods = foods;
    }

    public int getCountDay() {
        return countDay;
    }

    public void setCountDay(int countDay) {
        this.countDay = countDay;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(double totalCost) {
        this.totalCost = totalCost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tour)) return false;
        Tour tour = (Tour) o;
        return id == tour.id &&
                countDay == tour.countDay &&
                Double.compare(tour.totalCost, totalCost) == 0 &&
                Objects.equals(name, tour.name) &&
                Objects.equals(city, tour.city) &&
                Objects.equals(type, tour.type) &&
                Objects.equals(transports, tour.transports) &&
                Objects.equals(foods, tour.foods);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, city, type, transports, foods, countDay, totalCost);
    }

    @Override
    public String toString() {
        return "Tour{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", city=" + city +
                ", type=" + type +
                ", transports=" + transports +
                ", foods=" + foods +
                ", countDay=" + countDay +
                ", totalCost=" + String.format("%.2f",totalCost) +
                '}';
    }
}
