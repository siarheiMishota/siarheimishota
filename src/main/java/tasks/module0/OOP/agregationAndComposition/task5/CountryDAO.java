package tasks.module0.OOP.agregationAndComposition.task5;

import java.util.HashMap;
import java.util.Map;

public class CountryDAO {

    private Map<Integer, Country> countries = new HashMap<>();

    public CountryDAO() {
        create();
    }

    public Map<Integer, Country> getCountries() {
        return countries;
    }

    public Country getByKey(int key) {

        return countries.get(key);

    }

    private void create() {

        int counter=0;

        Country country = new Country("Belarus", 0.9);
        countries.put(counter++, country);

        country = new Country("Russia", 0.4);
        countries.put(counter++, country);

        country = new Country("Poland", 1.1);
        countries.put(counter++, country);

        country = new Country("Germany", 2.5);
        countries.put(counter++, country);

        country = new Country("Ukraine", 0.5);
        countries.put(counter++, country);

        country = new Country("Austria", 1.5);
        countries.put(counter++, country);

        country = new Country("Bulgaria", 1.8);
        countries.put(counter++, country);

    }
}
