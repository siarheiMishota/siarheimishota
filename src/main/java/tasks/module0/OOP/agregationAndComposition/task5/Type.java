package tasks.module0.OOP.agregationAndComposition.task5;

import java.util.Objects;

public class Type {

    private String name;
    private double coefficient;

    public Type(String name, double coefficient) {
        this.name = name;
        this.coefficient = coefficient;
    }

    public String getName() {
        return name;
    }

    public double getCoefficient() {
        return coefficient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Type)) return false;
        Type type = (Type) o;
        return Double.compare(type.coefficient, coefficient) == 0 &&
                Objects.equals(name, type.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, coefficient);
    }

    @Override
    public String toString() {
        return "type='" + name;
    }
}
