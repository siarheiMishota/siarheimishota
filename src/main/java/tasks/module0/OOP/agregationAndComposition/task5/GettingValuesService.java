package tasks.module0.OOP.agregationAndComposition.task5;

import tasks.module0.Service;

import java.util.Map;

public class GettingValuesService {

    public static Type getType() {

        TypeDAO typeDAO = new TypeDAO();

        for (Map.Entry<Integer, Type> integerTypeEntry : typeDAO.getTypeMap().entrySet()) {

            System.out.format("%3d - %s\n", integerTypeEntry.getKey(), integerTypeEntry.getValue().getName());

        }

        int keyType = Service.getPositiveAndZeroIntValueTillMaxNumber("selected type", typeDAO.getTypeMap().size());
        Type type = typeDAO.getByKey(keyType);
        return type;
    }

    public static Transport getTransport() {

        TransportDAO transportDAO = new TransportDAO();

        for (Map.Entry<Integer, Transport> integerTransportEntry : transportDAO.getTransports().entrySet()) {

            System.out.format("%3d - %s\n", integerTransportEntry.getKey(), integerTransportEntry.getValue().getName());

        }

        int keyTransport = Service.getPositiveAndZeroIntValueTillMaxNumber("selected transport", transportDAO.getTransports().size());
        Transport transport = transportDAO.getByKey(keyTransport);

        return transport;
    }

    public static Food getFood() {

        FoodDAO foodDAO = new FoodDAO();

        for (Map.Entry<Integer, Food> integerFoodEntry : foodDAO.getFoods().entrySet()) {

            System.out.format("%3d - %s\n", integerFoodEntry.getKey(), integerFoodEntry.getValue().getName());

        }

        int keyFood = Service.getPositiveAndZeroIntValueTillMaxNumber("selected food", foodDAO.getFoods().size());
        Food food = foodDAO.getByKey(keyFood);

        return food;
    }

    public static City getCity(Map<Integer, Country> countryMap) {

        CityDAO cityDAO = new CityDAO(countryMap);

        for (Map.Entry<Integer, City> integerCitiesEntry : cityDAO.getCities().entrySet()) {

            System.out.format("%3d - %s \n", integerCitiesEntry.getKey(), integerCitiesEntry.getValue().getName());

        }

        int keyCity = Service.getPositiveAndZeroIntValueTillMaxNumber("selected city", cityDAO.getCities().size());
        City city = cityDAO.getByKey(keyCity);
        return city;
    }
}
