package tasks.module0.OOP.agregationAndComposition.task5;

import java.util.Objects;

public class Food {

    private String name;
    private double coefficient;

    public Food(String name, double coefficient) {
        this.name = name;
        this.coefficient = coefficient;
    }

    public String getName() {
        return name;
    }

    public double getCoefficient() {
        return coefficient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Food)) return false;
        Food food = (Food) o;
        return Double.compare(food.coefficient, coefficient) == 0 &&
                Objects.equals(name, food.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, coefficient);
    }

    @Override
    public String toString() {
        return "Food" + name;
    }
}
