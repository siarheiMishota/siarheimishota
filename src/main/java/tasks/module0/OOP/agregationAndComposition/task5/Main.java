package tasks.module0.OOP.agregationAndComposition.task5;

import tasks.module0.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static tasks.module0.OOP.agregationAndComposition.task5.GettingValuesService.*;
import static tasks.module0.Service.getPositiveIntValue;

public class Main {

    public static void main(String[] args) {

        int numberOfTour = 10;
        int startCost = 200;
        int choice;

        List<Tour> tours = new ArrayList<>();

        String infoMenu = "\nInput the number:\n" +

                "1-add tour\n" +
                "2-sort the tours\n" +
                "3-print all the tours\n" +
                "other- exit";
        while (true) {
            choice = Service.getPositiveIntValue(infoMenu);

            switch (choice) {
                case 1:
                    Tour tour = createTour(numberOfTour, startCost);
                    tours.add(tour);
                    break;
                case 2:

                    String infoChoiceSorting = "\nInput the number:\n" +
                            "1-sort by ID\n" +
                            "other-sort by name\n";

                    int choiceSorting = getPositiveIntValue(infoChoiceSorting);

                    if (choiceSorting == 1) {
                        tours.sort(Comparator.comparing(Tour::getId));
                    } else {
                        tours.sort(Comparator.comparing(Tour::getName));

                    }

                    System.out.println("Sorted");
                    break;

                case 3:

                    for (Tour tourPrint : tours) {

                        System.out.println(tourPrint);
                    }

                    break;

                case 4:

                    System.exit(0);


            }
        }


    }

    private static Tour createTour(int numberOfTour, int startCost) {
        CountryDAO countryDAO = new CountryDAO();
        City city = getCity(countryDAO.getCountries());
        Type type = getType();
        Transport transport = getTransport();
        Food food = getFood();
        int countDay = Service.getPositiveIntValue("Input count of days on holiday");

        Tour tour = new Tour("" + numberOfTour++, city, type, transport, food, countDay, startCost);
        return tour;
    }


}
