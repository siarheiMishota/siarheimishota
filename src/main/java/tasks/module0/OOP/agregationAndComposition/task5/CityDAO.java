package tasks.module0.OOP.agregationAndComposition.task5;

import java.util.HashMap;
import java.util.Map;

public class CityDAO {

    private Map<Integer, City> cities = new HashMap<>();
    private Map<Integer,Country> countryMap;

    public CityDAO(Map<Integer,Country> countryMap) {
        this.countryMap=countryMap;
        create();
    }

    public City getByKey(int key){
        return cities.get(key);
    }

    public Map<Integer, City> getCities() {
        return cities;
    }

    private void create(){
        int counter=0;
        City  city=new City("Minsk",countryMap.get(1),1.7);
        cities.put(counter++,city);

        city=new City("Gomel",countryMap.get(1),1.2);
        cities.put(counter++,city);

        city=new City("Moscow",countryMap.get(2),1.1);
        cities.put(counter++,city);

        city=new City("Sochi",countryMap.get(2),1.4);
        cities.put(counter++,city);

        city=new City("Warsaw",countryMap.get(3),1.5);
        cities.put(counter++,city);

        city=new City("Belostok",countryMap.get(3),1.2);
        cities.put(counter++,city);

        city=new City("Berlin",countryMap.get(4),3.5);
        cities.put(counter++,city);

        city=new City("Nuremberg",countryMap.get(4),2.0);
        cities.put(counter++,city);

        city=new City("Kiev",countryMap.get(5),1.7);
        cities.put(counter++,city);

        city=new City("Harkov",countryMap.get(5),1.1);
        cities.put(counter++,city);

        city=new City("Vienna",countryMap.get(6),3.0);
        cities.put(counter++,city);

        city=new City("Graz",countryMap.get(6),2.1);
        cities.put(counter++,city);

        city=new City("Sofia",countryMap.get(7),2.5);
        cities.put(counter++,city);

        city=new City("Sozopol",countryMap.get(7),2.5);
        cities.put(counter++,city);






    }

}
