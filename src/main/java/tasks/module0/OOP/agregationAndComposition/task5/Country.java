package tasks.module0.OOP.agregationAndComposition.task5;

import java.util.Objects;

public class Country {

    private String name;
    private double coefficient;

    public Country(String name, double coefficient) {
        this.name = name;
        this.coefficient = coefficient;
    }

    public String getName() {
        return name;
    }

    public double getCoefficient() {
        return coefficient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Country)) return false;
        Country country = (Country) o;
        return Double.compare(country.coefficient, coefficient) == 0 &&
                Objects.equals(name, country.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, coefficient);
    }

    @Override
    public String toString() {
        return "Country" + name;
    }
}
