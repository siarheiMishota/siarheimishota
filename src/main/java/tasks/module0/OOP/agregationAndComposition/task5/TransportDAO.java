package tasks.module0.OOP.agregationAndComposition.task5;

import java.util.HashMap;
import java.util.Map;

public class TransportDAO {

    private Map<Integer, Transport> transports = new HashMap<>();

    public TransportDAO() {

        create();

    }

    public Map<Integer, Transport> getTransports() {
        return transports;
    }

    public Transport getByKey(int key) {

        return transports.get(key);

    }

    private void create() {

        int counter=0;

        Transport transport = new Transport("Plane", 3.0);
        transports.put(counter++, transport);

        transport = new Transport("Train", 2.0);
        transports.put(counter++, transport);

        transport = new Transport("Ship", 1.5);
        transports.put(counter++, transport);

        transport = new Transport("Bus", 1.0);
        transports.put(counter++, transport);


    }

}
