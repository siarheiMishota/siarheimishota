package tasks.module0.basic.linear;

public class Task23 {

    public static void execute() {

        double internalRadius = 10, externalRadius = 20;

        double area = Math.PI * (externalRadius * externalRadius - internalRadius * internalRadius);

        System.out.format("internal radius= %.3f, external radius= %.3f, area between ring= %.3f", internalRadius, externalRadius, area);


    }

}
