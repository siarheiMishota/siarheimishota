package tasks.module0.basic.linear;

public class Task24 {

    public static void execute() {

        double baseA = 10, baseB = 20, angle = 45;

        double area = (baseB * baseB - baseA * baseA) / 4 * Math.tan(Math.toRadians(angle));

        System.out.format("base a= %.3f,base b= %.3f,angle= %.3f,  area trapeze= %.3f", baseA, baseB, angle, area);


    }

}
