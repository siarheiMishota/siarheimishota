package tasks.module0.basic.linear;

import static java.lang.Math.*;

public class Task10 {

    public static void execute() {

        double functionValue;
        double x = 10, y = 30;

        functionValue = (sin(toRadians(x)) + cos(toRadians(y))) / (cos(toRadians(x)) - sin(toRadians(y))) * tan(toRadians(x * y));

        System.out.format("function= %.3f", functionValue);

    }

}
