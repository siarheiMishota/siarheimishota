package tasks.module0.basic.linear;

public class Task27 {

    public static void execute() {

        int a = 2;
        int aIn8, aIn10;

        a = a * a;
        a = a * a;
        aIn8 = a * a;

        a = 2;
        a = a * a;
        aIn10 = a * a;
        aIn10 = aIn10 * aIn10;
        aIn10 = a * aIn10;

        System.out.format("a= %d, a^8= %d, a^10= %d", a, aIn8, aIn10);

    }

}
