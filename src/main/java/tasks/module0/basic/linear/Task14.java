package tasks.module0.basic.linear;

public class Task14 {

    public static void execute() {

        double radius = 10;
        double area, circumference;


        circumference = 2 * Math.PI * radius;
        area = Math.PI * Math.pow(radius, 2);

        System.out.format("circumference= %.3f \n", circumference);
        System.out.format("area= %.3f \n", area);

    }

}
