package tasks.module0.basic.linear;

public class Task31 {

    public static void execute() {

        int speed = 20, speedRiver = 5, timeInLake = 3, timeInRiver = 4;

        int distance = speed * timeInLake + (speed + speedRiver) * timeInRiver;

        System.out.format("speed= %d, speed river= %d, time in lake= %d; time in river= %d; distance= %d", speed, speedRiver, timeInLake, timeInRiver, distance);


    }

}
