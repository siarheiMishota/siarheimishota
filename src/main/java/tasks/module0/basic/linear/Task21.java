package tasks.module0.basic.linear;

public class Task21 {

    public static void execute() {

        double inputNumber = 123.456;

        double intPart = (int) inputNumber;
        double fractionalPart = (int) ((inputNumber - intPart) * 1000);

        double resultingNumber = fractionalPart + intPart / 1000;

        System.out.format("Replaced integer and fractional parts places, was- %.3f  became= %.3f \n", inputNumber, resultingNumber);


    }

}
