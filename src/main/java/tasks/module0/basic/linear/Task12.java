package tasks.module0.basic.linear;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Task12 {

    public static void execute() {

        double distance;
        double x1 = 0, y1 = 0, x2 = 10, y2 = 10;


        distance = sqrt(pow((x1 - x2), 2) + pow((y1 - y2), 2));

        System.out.format("distance= %.3f", distance);

    }


}
