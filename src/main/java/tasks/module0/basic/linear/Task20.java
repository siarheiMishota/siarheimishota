package tasks.module0.basic.linear;

public class Task20 {

    public static void execute() {

        double circumference = 20;

        double area = circumference * circumference / 4 / Math.PI;

        System.out.format("If circumference= %.3f, area= %.3f \n", circumference, area);


    }
}
