package tasks.module0.basic.linear;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Task11 {


    public static void execute() {

        double area, perimeter;
        double legA = 3, legB = 4, legC;


        area = legA * legB / 2;

        legC = sqrt(pow(legA, 2) + pow(legB, 2));

        perimeter = legA + legB + legC;

        System.out.format("area= %.3f \n", area);
        System.out.format("perimeter= %.3f", perimeter);

    }


}
