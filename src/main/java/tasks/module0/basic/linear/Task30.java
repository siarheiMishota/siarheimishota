package tasks.module0.basic.linear;

public class Task30 {

    public static void execute() {

        double r1 = 10, r2 = 20, r3 = 30, rTotal, rObserveTotal;

        rObserveTotal = 1 / r1 + 1 / r2 + 1 / r3;
        rTotal = 1 / rObserveTotal;

        System.out.format("r1= %.1f, r2= %.1f, r3= %.1f; total= %.3f", r1, r2, r3, rTotal);


    }

}
