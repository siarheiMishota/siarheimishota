package tasks.module0.basic.linear;

public class Task19 {

    public static void execute() {

        int side = 32;
        double area, height, radiusInscribed, radiusCircumscribed;

        area = Math.sqrt(3) / 4 * side * side;
        height = Math.sqrt(3) / 2 * side;
        radiusInscribed = side / (2 * Math.sqrt(3));
        radiusCircumscribed = side / Math.sqrt(3);


        System.out.format("area = %.3f \n", area);
        System.out.format("height= %.3f \n", height);
        System.out.format("radius inscribed= %.3f \n", radiusInscribed);
        System.out.format("radius circumscribed= %.3f \n", radiusCircumscribed);

    }

}
