package tasks.module0.basic.linear;

public class Task25 {

    public static void execute() {

        int a = 1, b = 8, c = 4;

        double discriminant = b * b - 4 * a * c;

        double x1 = (-b - Math.sqrt(discriminant)) / 2 / a;
        double x2 = (-b + Math.sqrt(discriminant)) / 2 / a;

        System.out.format("a= %d, b= %d, c= %d. d= %.3f;  x1= %.3f, x2= %.3f", a, b, c, discriminant, x1, x2);

    }
}
