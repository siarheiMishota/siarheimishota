package tasks.module0.basic.linear;

public class Task16 {

    public static void execute() {

        int number = 9854, multiplyingNumber = 1;


        int[] digits = splitOnDigits(number);

        for (int digit : digits) {

            multiplyingNumber *= digit;

        }


        System.out.format("multiplying number= %6d \n", multiplyingNumber);

    }

    public static int[] splitOnDigits(int number) {

        int count = countDigitsInNumber(number);
        int[] digits = new int[count];
        int indexInDigits = count - 1;

        for (int j = 0; j < count; j++) {
            {
                digits[indexInDigits] = number % 10;
                number /= 10;
                indexInDigits--;
            }

        }
        return digits;

    }

    public static int countDigitsInNumber(int number) {

        int count = 0;

        while (number > 0) {
            number /= 10;
            count++;
        }
        return count;
    }

    public static boolean isNumberHasFourDigits(int number) {

        return countDigitsInNumber(number) == 4;

    }

}
