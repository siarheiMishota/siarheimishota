package tasks.module0.basic.linear;

public class Task15 {

    public static void execute() {

        System.out.println("degrees of PI");

        for (int i = 1; i < 5; i++) {

            System.out.format("%3d degree= %.3f \n", i, Math.pow(Math.PI, i));

        }

    }

}
