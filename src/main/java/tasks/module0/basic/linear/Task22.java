package tasks.module0.basic.linear;

public class Task22 {

    public static void execute() {

        int sourceSecond, h, min, sec;

        sourceSecond = 36000;

        h = sourceSecond / 3600;
        min = (sourceSecond - h * 3600) / 60;
        sec = sourceSecond - h * 3600 - min * 60;

        System.out.format("source second= %d - %sчч %sмин %s сек", sourceSecond, h, min, sec);
    }

}
