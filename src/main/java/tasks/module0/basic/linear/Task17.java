package tasks.module0.basic.linear;

public class Task17 {

    public static void execute() {

        int number1 = 22, number2 = 23;
        double cubeArithmeticAverage, cubeModuleGeometryAverage;


        cubeArithmeticAverage = (Math.pow(number1, 3) + Math.pow(number2, 3)) / 2;
        cubeModuleGeometryAverage = (Math.pow(Math.abs(number1), 3) * Math.pow(Math.abs(number2), 3)) / 2;

        System.out.format("middle arithmetic cubes= %.3f \n", cubeArithmeticAverage);
        System.out.format("middle geometry cubes= %.3f \n", cubeModuleGeometryAverage);


    }

}
