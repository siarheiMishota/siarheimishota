package tasks.module0.basic.linear;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Task8 {

    public static void execute() {

        double functionValue;
        double a = 5, b = 6, c = 7;

        functionValue = (b + sqrt(pow(b, 2) + 4 * a * c)) / (2 * a) - pow(a, 3) * c + pow(b, -2);

        System.out.format("function= %.3f", functionValue);

    }

}
