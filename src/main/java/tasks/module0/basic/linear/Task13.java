package tasks.module0.basic.linear;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Task13 {

    public static void execute() {

        double x1 = 0, y1 = 0, x2 = 5, y2 = 5, x3 = 1, y3 = 10;
        double sideA, sideB, sideC;
        double halfPerimeter, perimeter, area;

        sideA = sqrt(pow((x1 - x2), 2) + pow((y1 - y2), 2));
        sideB = sqrt(pow((x2 - x3), 2) + pow((y2 - y3), 2));
        sideC = sqrt(pow((x3 - x1), 2) + pow((y3 - y1), 2));

        perimeter = sideA + sideB + sideC;
        halfPerimeter = perimeter / 2;
        area = sqrt(halfPerimeter * (halfPerimeter - sideA) * (halfPerimeter - sideB) * (halfPerimeter - sideC));

        System.out.format("perimeter= %.3f \n", perimeter);
        System.out.format("area= %.3f \n", area);

    }

}
