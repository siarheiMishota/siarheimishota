package tasks.module0.basic.linear;

public class Task26 {

    public static void execute() {

        int baseA = 5, baseB = 8, angle = 33;

        double area = baseA * baseB * Math.sin(Math.toRadians(angle)) / 2;

        System.out.format("a= %d, b= %d, angle= %d. area= %.3f", baseA, baseB, angle, area);


    }

}
