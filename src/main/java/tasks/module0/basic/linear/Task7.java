package tasks.module0.basic.linear;

public class Task7 {

    public static void execute() {

        double heightToWidth = 2;
        double width = 15, height, area;

        height = heightToWidth * width;

        area = width * height;

        System.out.format("area= %.3f", area);


    }

}
