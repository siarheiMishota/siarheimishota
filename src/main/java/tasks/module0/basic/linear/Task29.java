package tasks.module0.basic.linear;

public class Task29 {

    public static void execute() {

        double sideA = 5, sideB = 4, sideC = 6;

        double angleInDegreesAB = Math.toDegrees((sideA * sideA + sideB * sideB - sideC * sideC) / (2 * sideA * sideB));
        double angleInDegreesBC = Math.toDegrees((sideB * sideB + sideC * sideC - sideA * sideA) / 2 / sideB / sideC);
        double angleInDegreesAC = 180 - angleInDegreesAB - angleInDegreesBC;

        System.out.println("angle a= " + (angleInDegreesAB));
        System.out.println("angle b= " + (angleInDegreesBC));
        System.out.println("angle c= " + (angleInDegreesAC));
    }

}
