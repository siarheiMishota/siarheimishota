package tasks.module0.basic.linear;

public class Task9 {

    public static void execute() {

        double function;
        double a = 2, b = 3, c = 4, d = 8;

        function = a / c * b / d - (a * b - c) / c / d;

        System.out.format("function= %.3f", function);

    }

}
