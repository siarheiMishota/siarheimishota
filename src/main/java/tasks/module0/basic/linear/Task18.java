package tasks.module0.basic.linear;

public class Task18 {

    public static void execute() {

        int numberFaceOfCube = 6;

        int sideCube = 25;
        double faceArea, cubeArea, cubeVolume;

        faceArea = Math.pow(sideCube, 2);
        cubeArea = faceArea * numberFaceOfCube;
        cubeVolume = Math.pow(sideCube, 3);


        System.out.format("area face of the cube= %.3f \n", faceArea);
        System.out.format("cube area= %.3f \n", cubeArea);
        System.out.format("cube volume= %.3f \n", cubeVolume);

    }


}
