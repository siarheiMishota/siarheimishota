package tasks.module0.basic.branching;

public class Task30 {

    public static void execute() {

        int a = -10, b = 20, c = -24;

        System.out.format("a=%d, b=%d,c=%d \n", a, b, c);

        if (a > b && b > c) {
            a = 2 * a;
            b = 2 * b;
            c = 2 * c;
        } else {
            a = Math.abs(a);
            b = Math.abs(b);
            c = Math.abs(c);
        }

        System.out.format("new a=%d, new b=%d, new c=%d", a, b, c);

    }

}
