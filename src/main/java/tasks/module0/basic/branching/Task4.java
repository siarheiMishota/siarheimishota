package tasks.module0.basic.branching;

import tasks.module0.Service;

public class Task4 {

    public static void execute() {

        int numberA, numberB;

        numberA = Service.getIntValue("Input number a");
        numberB = Service.getIntValue("Input number b");

        boolean isEqual = numberA == numberB;

        System.out.format("a=%d, b=%d, they equal=%b", numberA, numberB, isEqual);
    }

}
