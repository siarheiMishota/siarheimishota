package tasks.module0.basic.branching;

public class Task22 {

    public static void execute() {

        int x=10,y=15;

        System.out.format("x=%d, y=%d \n",x,y);

        if (y>x){
            int intermediate=x;
            x=y;
            y=intermediate;
        }

        System.out.format("new x=%d, new y=%d",x,y);


    }
}
