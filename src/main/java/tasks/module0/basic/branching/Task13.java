package tasks.module0.basic.branching;

import tasks.module0.Service;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Task13 {

    public static void execute() {

        int pointAX, pointAY, pointBX, pointBY, pointOrigin = 0;
        double distanceFromAToOrigin, distanceFromBToOrigin;

        pointAX = Service.getIntValue("Input coordinate x for point a");
        pointAY = Service.getIntValue("Input coordinate y for point a");
        pointBX = Service.getIntValue("Input coordinate x for point b");
        pointBY = Service.getIntValue("Input coordinate y for point b");

        distanceFromAToOrigin = sqrt(pow((pointAX - pointOrigin), 2) + pow((pointAY - pointOrigin), 2));
        distanceFromBToOrigin = sqrt(pow((pointBX - pointOrigin), 2) + pow((pointBY - pointOrigin), 2));

        String nearestPoint = distanceFromAToOrigin < distanceFromBToOrigin ? "point a" : "point b";

        System.out.format("A(%d,%d), B(%d,%d); nearest- %s", pointAX, pointAY, pointBX, pointBY, nearestPoint);


    }

}
