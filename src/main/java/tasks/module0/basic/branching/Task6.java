package tasks.module0.basic.branching;

import tasks.module0.Service;

public class Task6 {

    public static void execute() {

        int numberA, numberB, largerNumber;

        numberA = Service.getIntValue("Input number a");
        numberB = Service.getIntValue("Input number b");

        largerNumber = numberA > numberB ? numberA : numberB;

        System.out.format("a=%d, b=%d, larger number=%d", numberA, numberB, largerNumber);
    }

}
