package tasks.module0.basic.branching;

public class Task25 {

    public static void execute() {

        int maxTemperatureInRoomCelsius = 60;
        int currentTemperature = 80;

        System.out.format("current temperature=%d", currentTemperature);

        if (currentTemperature >= maxTemperatureInRoomCelsius) {
            System.out.println("Пожароопасная ситуация");
        }

    }
}
