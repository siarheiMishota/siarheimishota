package tasks.module0.basic.branching;

import tasks.module0.Service;

public class Task8 {

    public static void execute() {

        int a, b, smallerQuadrantNumber;

        a = Service.getIntValue("Input number a");
        b = Service.getIntValue("Input number b");

        smallerQuadrantNumber = a * a < b * b ? a * a : b * b;

        System.out.format("a=%d, b=%d, smaller number=%d", a, b, smallerQuadrantNumber);
    }

}
