package tasks.module0.basic.branching;

import tasks.module0.Service;

public class Task11 {

    public static void execute() {

        int sideATriangleA, sideBTriangleA, sideCTriangleA, sideATriangleB, sideBTriangleB, sideCTriangleB;
        double areaA, areaB, largerArea;
        double halfPerimeterA, halfPerimeterB;

        sideATriangleA = Service.getIntValue("Input side a triangle a");
        sideBTriangleA = Service.getIntValue("Input side b triangle a");
        sideCTriangleA = Service.getIntValue("Input side c triangle a");

        sideATriangleB = Service.getIntValue("Input side a triangle b");
        sideBTriangleB = Service.getIntValue("Input side b triangle b");
        sideCTriangleB = Service.getIntValue("Input side c triangle b");

        halfPerimeterA = ((double) sideATriangleA + sideBTriangleA + sideCTriangleA) / 2;
        halfPerimeterB = ((double) sideATriangleB + sideBTriangleB + sideCTriangleB) / 2;

        areaA = Math.sqrt(halfPerimeterA * (halfPerimeterA - sideATriangleA) * (halfPerimeterA - sideBTriangleA) * (halfPerimeterA - sideCTriangleA));
        areaB = Math.sqrt(halfPerimeterB * (halfPerimeterB - sideATriangleB) * (halfPerimeterB - sideBTriangleB) * (halfPerimeterB - sideCTriangleB));

        largerArea = areaA > areaB ? areaA : areaB;


        System.out.format("Triangle A:  a=%d, b=%d, c=%d;\n" +
                "Triangle B:  a=%d, b=%d, c=%d; larger area= %.3f \n", sideATriangleA, sideBTriangleA, sideCTriangleA, sideATriangleB, sideBTriangleB, sideCTriangleB, largerArea);
    }

}
