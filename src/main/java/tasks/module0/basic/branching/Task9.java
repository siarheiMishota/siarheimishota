package tasks.module0.basic.branching;

import tasks.module0.Service;

public class Task9 {

    public static void execute() {

        int sideA, sideB, sideC;
        boolean isEquilateralTriangle = false;

        sideA = Service.getIntValue("Input side a");
        sideB = Service.getIntValue("Input side b");
        sideC = Service.getIntValue("Input side c");

        if (sideA == sideB && sideA == sideC) {
            isEquilateralTriangle = true;
        }

        System.out.format("a=%d, b=%d, c=%d; equilateral triangle=%b\n", sideA, sideB, sideC, isEquilateralTriangle);
    }

}
