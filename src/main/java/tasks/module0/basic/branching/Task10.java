package tasks.module0.basic.branching;

import tasks.module0.Service;

public class Task10 {

    public static void execute() {

        int radiusA, radiusB;
        double areaA, areaB;

        radiusA = Service.getIntValue("Input radius a");
        radiusB = Service.getIntValue("Input radius b");

        areaA = Math.PI * radiusA * radiusA;
        areaB = Math.PI * radiusB * radiusB;

        String smallerArea = areaA < areaB ? "circle a" : "circle b";


        System.out.format("radius a=%d, radius b=%d, area a=%.3f, area b=%.3f; smaller area= %s\n", radiusA, radiusB, areaA, areaB, smallerArea);
    }

}
