package tasks.module0.basic.branching;

import tasks.module0.Service;

public class Task12 {

    public static void execute() {

        int[] numbers = new int[3];

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = Service.getIntValue("Input number " + i);
        }

        for (int i = 0; i < numbers.length; i++) {

            if (numbers[i] >= 0) {

                numbers[i] = (int) Math.pow(numbers[i], 2);

            } else {
                numbers[i] = (int) Math.pow(numbers[i], 4);
            }

        }


        for (int i = 0; i < numbers.length; i++) {

            System.out.format("%5d \t", numbers[i]);

        }

    }

}
