package tasks.module0.basic.branching;

public class Task28 {

    public static void execute() {
        int numberA = 10, numberB = 24, numberC = 12, d = 13;

        if (numberA == d) {
            System.out.println("a=d=" + d);
        } else if (numberB == d) {
            System.out.println("b=d=" + d);
        } else if (numberC == d) {
            System.out.println("c=d=" + d);

        } else {
            int ended = max(d - numberA, max(d - numberB, d - numberC));
            System.out.format("numberA=%d, numberB=%d, numberC=%d; numbers aren't equal, max(d-a,d-b,d-c)=%d", numberA, numberB, numberC, ended);
        }


    }

    private static int max(int a, int b) {
        return a >= b ? a : b;
    }

}
