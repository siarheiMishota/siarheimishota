package tasks.module0.basic.branching;

import static java.lang.Math.pow;

public class Task39 {

    public static void execute() {

        int x = 99;
        double f;


        if (x >= 8) {
            f = -pow(x, 2) + x - 9;
        } else {
            f = 1 / (pow(x, 4) - 6);
        }

        System.out.println("f= " + f);
    }

}
