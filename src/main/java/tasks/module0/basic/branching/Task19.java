package tasks.module0.basic.branching;

public class Task19 {

    public static void execute() {

        int a = 10, b = -16, c = 23;
        int countPositiveNumbers = 0;

        if (a > 0) {
            countPositiveNumbers++;
        }

        if (b > 0) {
            countPositiveNumbers++;
        }

        if (c > 0) {
            countPositiveNumbers++;
        }

        System.out.format("a=%d, b=%d, c=%d; count positive numbers=%d", a, b, c, countPositiveNumbers);

    }

}
