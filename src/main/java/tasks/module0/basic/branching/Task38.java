package tasks.module0.basic.branching;

import static java.lang.Math.pow;

public class Task38 {

    public static void execute() {

        int x = 2;
        double f;


        if (x >= 3 && x <= 3) {
            f = pow(x, 2);
        } else {
            f = 4;
        }

        System.out.println("f= " + f);
    }

}
