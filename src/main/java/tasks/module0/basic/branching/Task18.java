package tasks.module0.basic.branching;

public class Task18 {

    public static void execute() {

        int a = 10, b = -16, c = 23;
        int countNegativeNumbers = 0;

        if (a < 0) {
            countNegativeNumbers++;
        }

        if (b < 0) {
            countNegativeNumbers++;
        }

        if (c < 0) {
            countNegativeNumbers++;
        }

        System.out.format("a=%d, b=%d, c=%d; count negative numbers=%d", a, b, c, countNegativeNumbers);

    }

}
