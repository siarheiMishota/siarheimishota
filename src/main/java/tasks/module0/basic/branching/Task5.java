package tasks.module0.basic.branching;

import tasks.module0.Service;

public class Task5 {

    public static void execute() {

        int numberA, numberB, smallerNumber;

        numberA = Service.getIntValue("Input number a");
        numberB = Service.getIntValue("Input number b");

        smallerNumber = numberA < numberB ? numberA : numberB;

        System.out.format("a=%d, b=%d, smaller number=%d", numberA, numberB, smallerNumber);
    }

}
