package tasks.module0.basic.branching;

import tasks.module0.Service;

public class Task21 {

    public static void execute() {

        String man = "м", woman = "д";

        String enteredUserString = Service.getStringValue("Кто ты: мальчик или девочка?\n" +
                "д или м");

        if (enteredUserString.equalsIgnoreCase(man)) {
            System.out.println("мне нравятся мальчики!");
        } else if (enteredUserString.equalsIgnoreCase(woman)) {
            System.out.println("мне нравятся девочки!");

        } else {
            System.out.println("неправильно введено");
        }

    }

}
