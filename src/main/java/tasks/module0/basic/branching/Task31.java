package tasks.module0.basic.branching;

public class Task31 {

    public static void execute() {

        int a = 15, b = 16, x = 5, y = 20, z = 10, areaInlet, areaXY, areaYZ, areaXZ;
        boolean isPass = false;


        areaInlet = a * b;
        areaXY = x * y;
        areaYZ = y * z;
        areaXZ = x * z;

        if (areaInlet >= areaXY && max(a, b) >= max(x, y)) {
            isPass = true;
        } else if (areaInlet >= areaYZ && max(a, b) >= max(y, z)) {
            isPass = true;
        } else if (areaInlet >= areaXZ && max(a, b) >= max(x, z)) {
            isPass = true;
        }

        System.out.println(isPass ? "passes" : "don't pass");

    }


    private static int max(int a, int b) {
        return a >= b ? a : b;
    }

}
