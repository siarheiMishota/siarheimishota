package tasks.module0.basic.branching;

public class Task20 {

    public static void execute() {

        int a = 243, b = 543, c = 3459, k = 7;

        if (a % k == 0) {
            System.out.println("k is divisor a");
        }

        if (b % k == 0) {
            System.out.println("k is divisor b");
        }

        if (b % k == 0) {
            System.out.println("k is divisor c");
        }

    }

}
