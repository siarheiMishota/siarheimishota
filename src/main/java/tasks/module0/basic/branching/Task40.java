package tasks.module0.basic.branching;

import static java.lang.Math.pow;

public class Task40 {

    public static void execute() {

        int x = 12;
        double f;


        if (x <= 13) {
            f = -pow(x, 3) + 9;
        } else {
            f = -3 / (x + 1);
        }

        System.out.println("f= " + f);
    }

}
