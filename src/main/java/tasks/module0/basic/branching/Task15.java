package tasks.module0.basic.branching;

public class Task15 {

    public static void execute() {

        double x = 5, y = 8;
        double halfSum = (x + y) / 2;

        System.out.println("x=" + x + ", y=" + y);

        y = x * y * 2;
        x = halfSum;

        System.out.println("new x=" + x + ", new y=" + y);

    }


}
