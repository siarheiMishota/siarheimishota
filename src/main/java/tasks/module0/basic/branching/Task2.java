package tasks.module0.basic.branching;

public class Task2 {

    public static void execute() {

        int numberOne = 10, numberTwo = 20;

        String outString = numberOne < numberTwo ? "yes" : "no";

        System.out.println(outString);


    }
}
