package tasks.module0.basic.branching;

import tasks.module0.Service;

public class Task7 {

    public static void execute() {
        int a, b, c, x, solver;

        a = Service.getIntValue("Input number a");
        b = Service.getIntValue("Input number b");
        c = Service.getIntValue("Input number c");
        x = Service.getIntValue("Input number x");

        solver = Math.abs(a * x * x + b * b + c);


        System.out.format("a=%d, b=%d, c=%d,x=%d solver=%d", a, b, c, x, solver);
    }

}
