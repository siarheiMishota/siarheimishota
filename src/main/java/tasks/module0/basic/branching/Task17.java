package tasks.module0.basic.branching;

public class Task17 {

    public static void execute() {

        int m = 5, n = 8;

        System.out.println("m=" + m + ", n=" + n);

        if (m != n) {
            m = n = m > n ? m : n;
        } else {
            m = n = 0;
        }

        System.out.println("new m=" + m + ", new n=" + n);


    }

}
