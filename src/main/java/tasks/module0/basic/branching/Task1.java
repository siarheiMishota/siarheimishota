package tasks.module0.basic.branching;

public class Task1 {

    public static void execute() {

        int numberOne = 10, numberTwo = 20;

        int outInt = numberOne < numberTwo ? 7 : 8;

        System.out.format("%d \n", outInt);


    }

}
