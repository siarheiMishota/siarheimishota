package tasks.module0.basic.branching;

import tasks.module0.Service;

public class Task3 {

    public static void execute() {

        int number, constNumber = 3;

        number = Service.getIntValue("Input number");


        String outString = number < constNumber ? "Yes" : number > constNumber ? "No" : "";

        System.out.println(outString);

    }
}
