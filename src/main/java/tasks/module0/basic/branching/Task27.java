package tasks.module0.basic.branching;

public class Task27 {

    public static void execute() {

        int a = 11, b = 15, c = 2, d = 7;

        System.out.println("max{min(a,b),min(c,d)}= " + max(min(a, b), min(c, d)));
    }

    private static int max(int a, int b) {
        return a >= b ? a : b;
    }

    private static int min(int a, int b) {
        return a <= b ? a : b;
    }

}
