package tasks.module0.basic.branching;

public class Task26 {

    public static void execute() {

        int numberA = 10, numberB = 24, numberC = 12;
        int min = numberA, max = numberA;

        if (numberB > max) {
            max = numberB;
        }

        if (numberC > max) {
            max = numberC;
        }

        if (numberB < min) {
            min = numberB;
        }

        if (numberC < min) {
            min = numberC;
        }

        int sum = min + max;

        System.out.format("numberA=%d, numberB=%d, numberC=%d; sum max and min=%d", numberA, numberB, numberC, sum);

    }
}
