package tasks.module0.basic.cycles;

public class Task15 {

    public static void main(String[] args) {

        int number = 2, degree = 10, sum = 0;

        for (int i = 0; i <= degree; i++) {

            sum += Math.pow(number, i);

        }

        System.out.format("number=%d, degree=%d, sum=%d\n", number, degree, sum);

    }

}
