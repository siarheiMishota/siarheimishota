package tasks.module0.basic.cycles;

import static tasks.module0.Service.getDoubleValue;
import static tasks.module0.Service.getIntValue;

public class Task8 {

    public static void execute() {

        int a, b, c;
        double h = 0, delta = 1e-10, result;

        a = getIntValue("Input real number a - start of the segment");
        b = getIntValue("Input real number b - end of the segment");
        c = getIntValue("Input real number c");

        while (h <= 0) {
            h = getDoubleValue("Input number h>0");
        }

        for (double i = a; b - i >= delta; i = i + h) {

            if (i == 15) {
                result = (i + c) * 2;
                System.out.println(String.format("%.2f%n", result));
            } else {
                result = (i - c) + 6;
                System.out.println(String.format("%.2f%n", result));
            }
        }


    }

}
