package tasks.module0.basic.cycles;

import static tasks.module0.Service.getDoubleValue;
import static tasks.module0.Service.getIntValue;

public class Task7 {

    public static void execute() {

        int a, b;
        double h = 0, delta = 1e-10;

        a = getIntValue("Input real number a - start of the segment");
        b = getIntValue("Input real number b - end of the segment");

        while (h <= 0) {
            h = getDoubleValue("Input number h>0");
        }

        for (double i = a; b - i >= delta; i = i + h) {

            if (i > 2) {
                System.out.println(String.format("%.2g%n", (i)));
            } else {
                System.out.println(String.format("%.2g%n", (-i)));
            }
        }


    }


}
