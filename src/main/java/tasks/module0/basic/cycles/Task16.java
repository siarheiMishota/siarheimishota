package tasks.module0.basic.cycles;

public class Task16 {

    public static void main(String[] args) {


        long startNumber = 1, endNumber = 10, sum = 0, generalProduct = 1;

        for (int i = 1; i <= endNumber; i++) {

            sum += i;
            generalProduct *= sum;

        }

        System.out.format("startNumber=%d, endNumber=%d, product=%d\n", startNumber, endNumber, generalProduct);

    }

}
