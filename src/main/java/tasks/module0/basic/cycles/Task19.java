package tasks.module0.basic.cycles;

import static java.lang.Math.abs;
import static java.lang.Math.pow;
import static tasks.module0.Service.getDoubleValue;
import static tasks.module0.Service.getIntValue;

public class Task19 {

    public static void execute() {

        int n;
        double e, a, sum = 0;


        n = getIntValue("Input real number n");
        e = getDoubleValue("Input number e");

        for (int i = 0; i <= n; i++) {
            a = 1 / pow(2, i) + 1 / pow(3, i);

            if (abs(a) >= e) {
                sum += a;
            }
        }

        System.out.println("sum= " + sum);

    }

}
