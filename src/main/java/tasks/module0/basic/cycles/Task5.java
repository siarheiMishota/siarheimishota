package tasks.module0.basic.cycles;

public class Task5 {

    public static void execute() {

        int beginningRange = 1, endRange = 100, sum = 0;

        while (beginningRange < endRange) {

            if (beginningRange % 2 == 1) {
                sum += beginningRange;
            }

            beginningRange++;

        }

        System.out.println("sum of odd numbers up to 100= " + sum);
    }


}
