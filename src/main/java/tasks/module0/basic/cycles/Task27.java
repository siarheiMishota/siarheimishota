package tasks.module0.basic.cycles;

import static tasks.module0.Service.getPositiveIntValue;

public class Task27 {

    public static void main(String[] args) {

        int m, n;

        m = getPositiveIntValue("Input positive number m");
        n = getPositiveIntValue("Input positive number n");

        if (m > n) {
            int k = m;
            m = n;
            n = k;
        }

        for (int i = m; i <= n; i++) {
            if (i == 0 || i == 1) {
                continue;
            }
            System.out.print("All the divisors of " + i + "- ");
            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    System.out.print(j + ", ");
                }
            }

            System.out.println();
        }

    }

}
