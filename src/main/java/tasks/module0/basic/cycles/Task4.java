package tasks.module0.basic.cycles;

public class Task4 {

    public static void execute() {

        int beginningRange = 2, endRange = 100;

        while (beginningRange <= endRange) {


            if (beginningRange % 2 == 0) {
                System.out.print(beginningRange + ", ");
            }

            beginningRange++;

        }


    }

}
