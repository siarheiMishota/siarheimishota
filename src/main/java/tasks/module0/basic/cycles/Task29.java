package tasks.module0.basic.cycles;

import static java.lang.Math.abs;
import static tasks.module0.Service.getIntValue;

public class Task29 {

    public static void main(String[] args) {
        int numberA, numberB, k, m1;


        numberA = getIntValue("Input real number number A");
        numberB = getIntValue("Input real number number B");

        System.out.print("Overal figure: ");

        m1 = abs(numberA);

        while (m1 > 0) {
            k = m1 % 10;
            int n1 = abs(numberB);
            while (n1 > 0) {
                int t = n1 % 10;
                if (k == t) {
                    System.out.print(k + ", ");
                }
                n1 /= 10;
            }
            m1 /= 10;
        }


    }

}
