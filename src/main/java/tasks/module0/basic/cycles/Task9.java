package tasks.module0.basic.cycles;

import static java.lang.Math.pow;

public class Task9 {

    public static void execute() {

        int sum = 0, a = 100;
        for (int i = 1; i <= a; i++) {
            sum += pow(i, 2);
        }

        System.out.println("sum= " + sum);
    }

}
