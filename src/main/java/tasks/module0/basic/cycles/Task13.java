package tasks.module0.basic.cycles;

public class Task13 {

    public static void execute() {

        double step = 0.5, result;
        int beginningRange = -5, endRange = 5;

        for (double i = beginningRange; i <= endRange; i += step) {

            result = 5 * i * i / 2;

            System.out.format("i=%.2f, f=%.2f;\t\t", i, result);

        }


    }

}
