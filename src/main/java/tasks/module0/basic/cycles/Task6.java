package tasks.module0.basic.cycles;

import static tasks.module0.Service.getPositiveIntValue;

public class Task6 {

    public static void execute() {

        int enteredNumber, sum = 0;

        enteredNumber = getPositiveIntValue("Input positive number");

        for (int i = 0; i <= enteredNumber; i++) {
            sum += i;
        }

        System.out.println("n= " + enteredNumber + "\n" +
                "sum= " + sum);

    }

}
