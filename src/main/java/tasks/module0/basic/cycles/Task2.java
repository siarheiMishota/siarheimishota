package tasks.module0.basic.cycles;

public class Task2 {

    public static void execute() {

        for (int i = 5; i > 0; i--) {

            System.out.print(i + ", ");

        }

    }

}
