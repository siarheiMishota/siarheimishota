package tasks.module0.basic.cycles;

import tasks.module0.Service;

public class Task14 {

    public static void execute() {

        int number;
        double sum = 0;

        number = Service.getPositiveIntValue("Input number n");

        for (int i = 1; i <= number; i++) {

            sum += 1.0 / i;

        }

        System.out.format("n=%d, sum=%f\n", number, sum);


    }

}
