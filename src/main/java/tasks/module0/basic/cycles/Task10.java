package tasks.module0.basic.cycles;

import static java.lang.Math.pow;

public class Task10 {

    public static void execute() {

        long product = 1, a = 200;
        for (int i = 1; i <= a; i++) {
            product *= pow(i, 2);
        }

        System.out.println("product of the 200 numbers= " + product);
    }

}
