package tasks.module0.arrays.calculator;

import tasks.module0.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {

        String line = deleteAllSpaces(Service.getStringLine("Input expression for calculation, the following operations are supported:\n" +
                "/ * - + ").trim());

        while (hasOperand(line)) {

            line = calculateOneOperationAndReturnLine(line);

        }

        System.out.println(line);

    }

    private static String deleteAllSpaces(String lineIn) {

        return lineIn.replaceAll(" ", "");

    }

    private static boolean hasOperand(String lineIn) {

        Pattern patterOperand = Pattern.compile("[\\/*\\-+]");

        return patterOperand.matcher(lineIn).find();

    }

    private static String calculateOneOperationAndReturnLine(String lineIn) {

        int forNextIndex = 1, indexOperation = -1, indexStartFirstOperand = -1, indexEndSecondOperand = -1;
        double firstOperand = 1, secondOperand = 1;
        String outValue = "";
        Matcher matcherOperation;

        Pattern patternProduct = Pattern.compile("[*]");
        Pattern patternDividing = Pattern.compile("[/]");
        Pattern patternDifference = Pattern.compile("[-]");
        Pattern patternPlus = Pattern.compile("[+]");

        lineIn = replaceCommasWithDots(lineIn);


        if ((matcherOperation = patternDividing.matcher(lineIn)).find()) {

            indexOperation = matcherOperation.start();

            indexStartFirstOperand = getStartIndexFirstOperandExpression(lineIn, indexOperation);
            indexEndSecondOperand = getEndIndexSecondOperandExpression(lineIn, indexOperation);

            firstOperand = getDoubleBetweenIndexes(lineIn, indexStartFirstOperand, indexOperation);
            secondOperand = getDoubleBetweenIndexes(lineIn, indexOperation + forNextIndex, indexEndSecondOperand + forNextIndex);

            double dividing = firstOperand / secondOperand;
            outValue = String.format("%.3f", dividing);


        } else if ((matcherOperation = patternProduct.matcher(lineIn)).find()) {

            indexOperation = matcherOperation.start();

            indexStartFirstOperand = getStartIndexFirstOperandExpression(lineIn, indexOperation);
            indexEndSecondOperand = getEndIndexSecondOperandExpression(lineIn, indexOperation);

            firstOperand = getDoubleBetweenIndexes(lineIn, indexStartFirstOperand, indexOperation);
            secondOperand = getDoubleBetweenIndexes(lineIn, indexOperation + forNextIndex, indexEndSecondOperand + forNextIndex);

            double product = firstOperand * secondOperand;
            outValue = String.format("%.3f", product);


        } else if ((matcherOperation = patternDifference.matcher(lineIn)).find()) {

            indexOperation = matcherOperation.start();

            indexStartFirstOperand = getStartIndexFirstOperandExpression(lineIn, indexOperation);
            indexEndSecondOperand = getEndIndexSecondOperandExpression(lineIn, indexOperation);

            firstOperand = getDoubleBetweenIndexes(lineIn, indexStartFirstOperand, indexOperation);
            secondOperand = getDoubleBetweenIndexes(lineIn, indexOperation + forNextIndex, indexEndSecondOperand + forNextIndex);

            double difference = firstOperand - secondOperand;
            outValue = String.format("%.3f", difference);

        } else if ((matcherOperation = patternPlus.matcher(lineIn)).find()) {

            indexOperation = matcherOperation.start();

            indexStartFirstOperand = getStartIndexFirstOperandExpression(lineIn, indexOperation);
            indexEndSecondOperand = getEndIndexSecondOperandExpression(lineIn, indexOperation);

            firstOperand = getDoubleBetweenIndexes(lineIn, indexStartFirstOperand, indexOperation);
            secondOperand = getDoubleBetweenIndexes(lineIn, indexOperation + forNextIndex, indexEndSecondOperand + forNextIndex);

            double product = firstOperand + secondOperand;
            outValue = String.format("%.3f", product);


        }

        if (checkIndexOperand(indexStartFirstOperand, indexEndSecondOperand)) {

            String partOfLineTillExpression = lineIn.substring(0, indexStartFirstOperand);
            String partOfLinePastExpression = lineIn.substring(indexEndSecondOperand + forNextIndex);

            lineIn = partOfLineTillExpression + outValue + partOfLinePastExpression;

        }


        return lineIn;
    }

    private static String replaceCommasWithDots(String lineIn) {
        return lineIn.replaceAll(",", ".");
    }

    private static int getStartIndexFirstOperandExpression(String lineExpression, int indexOperation) {

        int startIndexFirstOperand = -1;

        Pattern patterFirstNumber = Pattern.compile("(\\d+\\.?\\d*)$");

        Matcher matcherFirstNumber = patterFirstNumber.matcher(lineExpression.substring(0, indexOperation));

        if (matcherFirstNumber.find()) {

            startIndexFirstOperand = matcherFirstNumber.start();

        }

        return startIndexFirstOperand;
    }

    private static int getEndIndexSecondOperandExpression(String lineExpression, int indexOperation) {

        int endIndexSecondNumber = -1, forNextIndex = 1;

        Pattern patterLastNumber = Pattern.compile("\\d+\\.?\\d*");

        String substring = lineExpression.substring(indexOperation + forNextIndex);
        Matcher matcherSecondNumber = patterLastNumber.matcher(substring);

        if (matcherSecondNumber.find()) {

            endIndexSecondNumber = matcherSecondNumber.end();

        }

        return indexOperation + endIndexSecondNumber;
    }

    private static double getDoubleBetweenIndexes(String line, int indexStart, int indexEndExclude) {

        return Double.parseDouble(line.substring(indexStart, indexEndExclude));

    }

    private static boolean checkIndexOperand(int indexOfFirstOperand, int indexOfSecondOperand) {

        return indexOfFirstOperand != -1 && indexOfSecondOperand != -1;

    }


}
