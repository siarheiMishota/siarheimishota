package tasks.module0.arrays.oneDimensional;

import tasks.module0.Service;

public class Task14 {

    public static void execute() {

        int[] array = Service.getArrayInt("Input integer array");

        int max = maxElementWithEvenIndex(array);
        int min = minElementWithEvenIndex(array);
        int result = max + min;

        System.out.println("result= " + result);

    }

    private static int maxElementWithEvenIndex(int[] arrayIn) {

        int max = arrayIn[0];

        for (int i = 0; i < arrayIn.length; i += 2) {

            if (arrayIn[i] > max) {
                max = arrayIn[i];
            }

        }
        return max;

    }

    private static int minElementWithEvenIndex(int[] arrayIn) {

        if (arrayIn.length < 2) {
            return 0;
        }

        int min = arrayIn[1];

        for (int i = 1; i < arrayIn.length; i += 2) {

            if (arrayIn[i] < min) {
                min = arrayIn[i];
            }

        }
        return min;

    }


}
