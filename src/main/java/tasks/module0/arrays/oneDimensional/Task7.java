package tasks.module0.arrays.oneDimensional;

import java.util.Arrays;

import static tasks.module0.Service.getArrayDouble;
import static tasks.module0.Service.getIntValue;

public class Task7 {

    public static void execute() {

        int z, count = 0;
        double[] a, input;

        a = getArrayDouble("Input double array");

        input = new double[a.length];

        System.out.println("_________________________________________________________");
        z = getIntValue("Input z");

        for (int i = 0; i < a.length; i++) {
            input[i] = a[i];
            if (a[i] > z) {
                a[i] = z;
                count++;
            }
        }

        System.out.println("Array input- " + Arrays.toString(input));
        System.out.println("count numbers > z= " + count);
        System.out.println("Array output- " + Arrays.toString(a));


    }


}
