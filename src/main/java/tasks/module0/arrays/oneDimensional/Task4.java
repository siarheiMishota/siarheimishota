package tasks.module0.arrays.oneDimensional;

import tasks.module0.Service;

public class Task4 {

    public static void execute() {

        boolean isIncreasingSequence = true;

        int[] array = Service.getArrayInt("Input the array of integer");


        for (int i = 1; i < array.length; i++) {

            if (array[i] < array[i - 1]) {
                isIncreasingSequence = false;
            }

        }

        System.out.println("Sequence is " + (isIncreasingSequence ? "increasing" : "not increasing"));

    }
}
