package tasks.module0.arrays.oneDimensional;

import static tasks.module0.Service.getArrayInt;

public class Task10 {

    public static void execute() {

        int[] a;

        a=getArrayInt("Input  array");

        System.out.print("a[i]>i - ");
        for (int i = 0; i < a.length; i++) {
            if (a[i]>i){
                System.out.print(a[i]+", ");
            }
        }
    }

}
