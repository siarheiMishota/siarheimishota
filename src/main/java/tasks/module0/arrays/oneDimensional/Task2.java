package tasks.module0.arrays.oneDimensional;

import java.util.Arrays;

import static tasks.module0.Service.getArrayInt;

public class Task2 {

    public static void execute() {

        int[] array = getArrayInt("Input the array");
        int[] arrayOfZero = getArrayOfZeroElements(array);

        System.out.println("Array- " + Arrays.toString(array));
        System.out.println("Array of zero- " + Arrays.toString(arrayOfZero));


    }

    private static int[] getArrayOfZeroElements(int[] arrayIn) {

        int counterZero = 0;
        int[] arrayOfZero = new int[arrayIn.length];

        for (int element : arrayIn) {

            if (element == 0) {
                arrayOfZero[counterZero++] = 0;
            }

        }

        int[] outputArray = new int[counterZero];

        System.arraycopy(arrayOfZero, 0, outputArray, 0, outputArray.length);

        return outputArray;
    }

}
