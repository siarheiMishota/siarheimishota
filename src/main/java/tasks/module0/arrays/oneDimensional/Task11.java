package tasks.module0.arrays.oneDimensional;

import tasks.module0.Service;

public class Task11 {

    public static void execute() {

        int[] arrayIn = Service.getArrayInt("Input array of natural numbers");
        int numberM = -1;

        while (numberM < 0) {
            numberM = Service.getPositiveIntValue("Input number M");

        }

        for (int value : arrayIn) {

            int numberL = value % numberM;

            if (0 < numberL && numberL < numberM - 1) {

                System.out.print(value + ", ");
            }

        }


    }

}
