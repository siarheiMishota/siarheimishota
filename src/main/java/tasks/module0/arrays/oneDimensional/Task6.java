package tasks.module0.arrays.oneDimensional;

import tasks.module0.Service;

public class Task6 {

    public static void execute() {

        int[] array = Service.getArrayInt("Input the array of integer");

        int min = min(array);
        int max = max(array);
        int leastLengthOfAxis = max - min;

        System.out.println("Least length of axis= " + leastLengthOfAxis);


    }

    private static int min(int[] arrayIn) {

        if (arrayIn == null) {
            throw new IllegalArgumentException("Incorrect array");
        }

        int min = arrayIn[0];

        for (int valueInArray : arrayIn) {

            if (valueInArray < min) {
                min = valueInArray;
            }

        }

        return min;

    }

    private static int max(int[] arrayIn) {

        if (arrayIn == null) {
            throw new IllegalArgumentException("Incorrect array");
        }

        int max = arrayIn[0];

        for (int valueInArray : arrayIn) {

            if (valueInArray > max) {
                max = valueInArray;
            }

        }

        return max;

    }


}
