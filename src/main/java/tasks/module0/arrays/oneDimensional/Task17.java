package tasks.module0.arrays.oneDimensional;

import java.util.Arrays;

import static tasks.module0.Service.getArrayInt;

public class Task17 {

    public static void execute() {

        int[] a;

        a = getArrayInt("Input array");

        int[] newArray = remove(a, min(a));

        System.out.println("array after remove- " + Arrays.toString(newArray));
    }

    private static int[] remove(int[] massive, double removable) {
        int[] output;
        int count = numberOfMinimumElements(massive);

        if (count == 0) {
            return massive;
        }

        output = new int[massive.length - count];
        int iOutput = 0;

        for (int value : massive) {
            if (value != removable) {
                output[iOutput] = value;
                iOutput++;
            }
        }

        return output;
    }

    private static int numberOfMinimumElements(int[] massive) {
        int removable = min(massive);
        int count = 0;

        for (int value : massive) {
            if (value == removable) {
                count++;
            }
        }

        return count;
    }

    private static int min(int[] massive) {
        int min = massive[0];
        for (int value : massive) {
            if (value < min) {
                min = value;
            }
        }
        return min;
    }

}
