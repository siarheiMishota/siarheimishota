package tasks.module0.arrays.oneDimensional;

import java.util.Arrays;

import static tasks.module0.Service.getArrayDouble;

public class Task12 {

    public static void execute() {

        double sum = 0;
        double[] a;
        double[] simpleA;

        a = getArrayDouble("Input double array");

        simpleA = new double[a.length];


        int k = 0;

        for (int i = 0; i < a.length; i++) {

            if (isSimple(i)) {

                sum += a[i];
                simpleA[k] = a[i];
                k++;

            }

        }

        double[] out = new double[k];

        System.arraycopy(simpleA, 0, out, 0, k);
        System.out.println("Array input- " + Arrays.toString(a));
        System.out.println("Array output- " + Arrays.toString(out));
        System.out.println("sum= " + sum);
    }

    private static boolean isSimple(int number) {
        if (number < 2) {
            return false;
        }

        for (int j = 2; j < number; j++) {

            if (number % j == 0) {
                return false;
            }

        }
        return true;
    }


}
