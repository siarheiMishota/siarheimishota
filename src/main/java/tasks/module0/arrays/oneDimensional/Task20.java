package tasks.module0.arrays.oneDimensional;

import java.util.Arrays;

import static tasks.module0.Service.getArrayInt;

public class Task20 {

    public static void execute() {


        int[] a = getArrayInt("Input array:");

        System.out.println("new massive- " + Arrays.toString(removeOnZero(a)));


    }

    private static int[] removeOnZero(int[] arrayIn) {
        int counter = 1;
        for (int i = 0; i < arrayIn.length; i++) {
            if (i % 2 != 0 && i != arrayIn.length - 1) {
                arrayIn[counter] = arrayIn[i + 1];
                counter++;
            }
        }

        for (int i = counter; i < arrayIn.length; i++) {
            arrayIn[i] = 0;
        }

        return arrayIn;
    }

}
