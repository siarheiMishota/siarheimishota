package tasks.module0.arrays.oneDimensional;

import tasks.module0.Service;

public class Task15 {

    public static void execute() {

        double[] array = Service.getArrayDouble("Input double array");

        double startOfGap = -1;
        double endOfGap = -1;

        while (isDataInCorrect(startOfGap, endOfGap)) {

            startOfGap = Service.getDoubleValue("Input number of start of gap");
            endOfGap = Service.getDoubleValue("Input number of end of gap");

        }

        for (double value : array) {

            if (belongToGap(startOfGap, endOfGap, value)) {

                System.out.print(value + ", ");

            }


        }


    }

    private static boolean isDataInCorrect(double startOfGap, double endOfGap) {

        return endOfGap <= startOfGap;
    }

    private static boolean belongToGap(double startOfGap, double endOfGap, double value) {
        return startOfGap <= value && value <= endOfGap;
    }

}
