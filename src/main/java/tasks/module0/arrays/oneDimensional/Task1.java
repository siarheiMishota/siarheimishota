package tasks.module0.arrays.oneDimensional;

import java.util.Arrays;

import static tasks.module0.Service.getArrayInt;
import static tasks.module0.Service.getIntValue;

public class Task1 {

    public static void execute() {

        int k, sum = 0;
        int[] a;

        a = getArrayInt("Input the array");

        System.out.println("_________________________________________________________");

        k = getIntValue("Input k");


        for (int i = 0; i < a.length; i++) {
            if (a[i] % k == 0) {
                sum += a[i];
            }
        }

        System.out.println("Array- " + Arrays.toString(a));
        System.out.println("sum of multiple numbers of " + k + "= " + sum);


    }

}
