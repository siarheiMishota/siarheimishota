package tasks.module0.arrays.oneDimensional;

import static tasks.module0.Service.getArrayDouble;

public class Task16 {

    public static void execute() {

        double[] a;

        a = getArrayDouble("Input double array");

        double[] modifiedArray = createArrayWithAlgorithm(a);

        double max = max(modifiedArray);

        System.out.println(max);


    }


    private static double max(double[] arrayIn) {

        double max = arrayIn[0];

        for (double t : arrayIn) {
            if (t > max) {
                max = t;
            }
        }
        return max;
    }

    private static double[] createArrayWithAlgorithm(double[] arrayIn) {

        int arrayInSize = arrayIn.length;
        double[] mass = new double[arrayIn.length % 2 == 0 ? arrayIn.length / 2 : arrayIn.length / 2 + 1];

        for (int i = 0; i < (arrayInSize % 2 == 0 ? arrayInSize / 2 : arrayInSize / 2 + 1); i++) {
            if (i == arrayInSize - 1 - i) {
                mass[i] = arrayIn[i];
            } else {
                mass[i] = arrayIn[i] + arrayIn[arrayInSize - 1 - i];
            }
        }

        return mass;

    }


}
