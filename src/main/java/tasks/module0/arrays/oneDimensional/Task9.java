package tasks.module0.arrays.oneDimensional;

import java.util.Arrays;

import static tasks.module0.Service.getArrayDouble;

public class Task9 {

    public static void execute() {

        int maxIndex = 0, minIndex = 0;
        double max, min;
        double[] a, input;

        a = getArrayDouble("Input double array");
        input = new double[a.length];

        System.arraycopy(a, 0, input, 0, a.length);

        max = a[0];
        min = a[0];

        for (int i = 0; i < a.length; i++) {

            if (a[i] > max) {
                max = a[i];
                maxIndex = i;

            } else if (a[i] < min) {
                min = a[i];
                minIndex = i;
            }
        }

        a[maxIndex] = min;
        a[minIndex] = max;

        System.out.println("Array input- " + Arrays.toString(input));
        System.out.println("min= " + min);
        System.out.println("max= " + max);
        System.out.println("Array output- " + Arrays.toString(a));
    }

}
