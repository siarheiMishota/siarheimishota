package tasks.module0.arrays.oneDimensional;

import tasks.module0.Service;

public class Task13 {

    public static void execute() {

        int[] arrayIn = Service.getArrayInt("Input array of natural numbers");

        int startOfGap = -1, endOfGap = -1, counterElementsInIntervalAndMultipleModels = 0;
        int numberM = Service.getPositiveIntValue("Input natural number M");

        while (isDataInCorrect(startOfGap, endOfGap)) {
            startOfGap = Service.getPositiveIntValue("Input number L is start of gap");
            endOfGap = Service.getPositiveIntValue("Input number N is end of gap");

        }

        for (int value : arrayIn) {

            if (value % numberM == 0 && belongToGap(startOfGap, endOfGap, value)) {
                counterElementsInIntervalAndMultipleModels++;
            }

        }

        System.out.println("count= " + counterElementsInIntervalAndMultipleModels);

    }


    private static boolean isDataInCorrect(int startOfGap, int endOfGap) {

        return endOfGap <= startOfGap;
    }

    private static boolean belongToGap(int startOfGap, int endOfGap, int value) {
        return startOfGap <= value && value < endOfGap;
    }
}
