package tasks.module0.arrays.oneDimensional;

import static tasks.module0.Service.getArrayInt;

public class Task19 {

    public static void execute() {

        int[] a;

        a = getArrayInt("Input array");

        int[][] differentNumberAndCount=countDifferentNumbers(a);
        int[] arrayMostCommonNumber=maximumNumber(differentNumberAndCount);
        int minimumNumberOfFrequentlyRepeatedNumbers=min(arrayMostCommonNumber);
        System.out.println("min= " + minimumNumberOfFrequentlyRepeatedNumbers);


    }


    private static int[][] countDifferentNumbers(int[] input) {

        int count = 0;
        int[][] test = new int[input.length][2];
        int[][] output;


        for (int value : input) {

            if (haveNumberInArray(count, test, value)) {

                for (int i = 0; i < count; i++) {

                    if (test[i][0] == value) {
                        test[i][1]++;
                    }
                }

            } else {
                test[count][0] = value;
                test[count][1] = 1;
                count++;
            }

        }

        output = new int[count][2];

        for (int i = 0; i < count; i++) {
            System.arraycopy(test[i], 0, output[i], 0, test[0].length);
        }
        return output;
    }

    private static boolean haveNumberInArray(int witchToCount, int[][] test, int value) {

        for (int j = 0; j < witchToCount; j++) {

            if (test[j][0] == value) {
                return true;
            }
        }
        return false;
    }

    private static int[] maximumNumber(int[][] input) {
        int[] maxOutput;
        int maxValue = input[0][0];
        int countMaxValue = 0;

        for (int[] value : input) {
            if (value[1] > maxValue) {
                maxValue = value[1];
            }
        }

        for (int[] ints : input) {
            if (ints[1] == maxValue) {
                countMaxValue++;
            }
        }

        maxOutput = new int[countMaxValue];

        for (int i = 0, j = 0; i < input.length; i++) {
            if (input[i][1] == maxValue) {
                maxOutput[j] = input[i][0];
                j++;
            }
        }
        return maxOutput;
    }

    private static int min(int[] input) {
        int min = input[0];
        for (int value : input) {
            if (value < min) {
                min = value;
            }
        }

        return min;
    }

}
