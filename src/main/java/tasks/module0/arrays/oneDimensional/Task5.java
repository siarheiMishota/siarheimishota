package tasks.module0.arrays.oneDimensional;

import tasks.module0.Service;

import java.util.Arrays;

public class Task5 {

    public static void execute() {

        boolean hasEvenNumber = false;
        int indexForArrayEvenNumbers = 0;

        int[] array = Service.getArrayInt("Input the array of integer");
        int[] arrayEvenNumbers = new int[array.length];


        for (int number : array) {

            if (isEvenNumber(number)) {
                arrayEvenNumbers[indexForArrayEvenNumbers++] = number;
                hasEvenNumber = true;
            }

        }

        if (hasEvenNumber) {

            int[] arrayOutputEvenNumbers = new int[indexForArrayEvenNumbers];

            System.arraycopy(arrayEvenNumbers, 0, arrayOutputEvenNumbers, 0, indexForArrayEvenNumbers);

            System.out.println("array of even numbers- " + Arrays.toString(arrayOutputEvenNumbers));

        } else {

            System.out.println("Array hasn't even numbers");

        }


    }

    private static boolean isEvenNumber(int number) {

        return number % 2 == 0;

    }
}
