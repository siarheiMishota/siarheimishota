package tasks.module0.arrays.oneDimensional;

import tasks.module0.Service;

public class Task3 {

    public static void execute() {

        int[] array = Service.getArrayInt("Input the array of integer");

        for (int value : array) {

            if (value > 0) {

                System.out.println("The first number is positive");
                break;

            } else if (value < 0) {

                System.out.println("The first number is negative");
                break;

            }

        }

    }

}
