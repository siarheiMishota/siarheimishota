package tasks.module0.arrays.oneDimensional;

import java.util.Arrays;

import static tasks.module0.Service.getArrayDouble;

public class Task8 {

    public static void execute() {

        int countPositive = 0, countNegative = 0, countZero = 0;
        double[] a;

        a = getArrayDouble("Input double array");

        for (int i = 0; i < a.length; i++) {
            if (a[i] < 0) {
                countNegative++;
            } else if (a[i] > 0) {
                countPositive++;
            } else {
                countZero++;
            }
        }

        System.out.println("Array input- " + Arrays.toString(a));
        System.out.println("count negative numbers= " + countNegative);
        System.out.println("count numbers of zero= " + countZero);
        System.out.println("count positive numbers= " + countPositive);


    }

}
