package tasks.module0.arrays.sortArray;

import tasks.module0.Service;

import java.util.Arrays;
import java.util.Random;

public class Task1 implements ArrayTasks {

    @Override
    public void execute() {

        int[] firstArray, secondArray;
        int firstLength = 10, indexAfterWhichInsertTwoArray = 2;
        Random random = new Random();

        secondArray = Service.getRandomIntArray(10, 25);

        firstArray = new int[firstLength + secondArray.length];

        for (int i = 0; i < firstLength; i++) {
            firstArray[i] = random.nextInt(40);
        }

        System.out.println("first array till insert:");

        for (int i = 0; i < firstLength; i++) {

            System.out.format("%5d", firstArray[i]);

        }

        System.out.println("\nsecond array:");
        System.out.println(Arrays.toString(secondArray));

        mergeTwoArraysWithKInA(firstArray, secondArray, indexAfterWhichInsertTwoArray);
        System.out.println(Arrays.toString(firstArray));


    }


    private static void mergeTwoArraysWithKInA(int[] firstArray, int[] secondArray, int k) {

        for (int i = firstArray.length - 1; i >= k + secondArray.length; i--) {

            firstArray[i] = firstArray[i - secondArray.length];

        }

        for (int i = k + 1, p = 0; p < secondArray.length; p++, i++) {

            firstArray[i] = secondArray[p];

        }
    }


}
