package tasks.module0.arrays.sortArray;

import java.util.Arrays;

import static tasks.module0.Service.getRandomIncreasingArrayOfInt;

public class Task7 implements ArrayTasks {

    @Override
    public void execute() {

        int lengthArrays = 10;

        int[] firstArray = getRandomIncreasingArrayOfInt(lengthArrays, 150);
        int[] secondArray = getRandomIncreasingArrayOfInt(lengthArrays, 150);

        int[] indexesToInsertIntoFirstArray = indexInsert(firstArray, secondArray);

        System.out.println("First array:\n" + Arrays.toString(firstArray));
        System.out.println("Second array:\n" + Arrays.toString(secondArray));

        System.out.println("Места в массиве А, для вставки массива В- " + Arrays.toString(indexesToInsertIntoFirstArray));
    }

    private static int[] indexInsert(int[] firstArray, int[] secondArray) {

        int[] massIndexes = new int[secondArray.length];
        int number = 0;

        int indexSecondArray = 0;

        for (int i = 0; i < firstArray.length; i++) {

            while (indexSecondArray != secondArray.length && firstArray[i] > secondArray[indexSecondArray]) {

                massIndexes[number] = i + indexSecondArray;
                indexSecondArray++;
                number++;


            }

            if (i == firstArray.length - 1) {

                i++;

                while (true) {
                    if (indexSecondArray == secondArray.length) break;

                    massIndexes[number] = i + indexSecondArray;
                    number++;
                    indexSecondArray++;

                }


            }

        }


        return massIndexes;

    }

}
