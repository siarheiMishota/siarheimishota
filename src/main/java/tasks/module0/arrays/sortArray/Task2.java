package tasks.module0.arrays.sortArray;

import java.util.Arrays;
import java.util.Random;

import static tasks.module0.Service.getRandomIncreasingArrayOfInt;

public class Task2 implements ArrayTasks {

    @Override
    public void execute() {

        Random random = new Random();
        int[] firstArray, secondArray;
        int firstLength = 10, limitForRandom = 1000;

        secondArray = getRandomIncreasingArrayOfInt(10, limitForRandom);

        firstArray = new int[firstLength + secondArray.length];


        do {
            firstArray[0] = random.nextInt(limitForRandom);
        } while (firstArray[0] > limitForRandom / 2);

        for (int i = 1; i < firstLength; i++) {

            do {

                firstArray[i] = random.nextInt(limitForRandom);

            } while (firstArray[i] < firstArray[i - 1]);

        }


        mergeTwoArraysAscendingOrder(firstArray, secondArray);
        System.out.println("First array after insert:\n" + Arrays.toString(firstArray));

    }


    private static void mergeTwoArraysAscendingOrder(int[] firstArray, int[] secondArray) {

        for (int i = 0, secondArrayIndex = 0; i < firstArray.length; i++) {

            if (secondArrayIndex != secondArray.length &&
                    firstArray[i] >= secondArray[secondArrayIndex]) {

                for (int j = firstArray.length - 1; j > i; j--) {

                    firstArray[j] = firstArray[j - 1];

                }
                firstArray[i] = secondArray[secondArrayIndex];
                secondArrayIndex++;

            }

        }

    }

}
