package tasks.module0.arrays.sortArray;

public class Main {

    public static void main(String[] args) {
        ArrayTasks[] arrayTasks = new ArrayTasks[6];

        int counterTasks = 0;

        arrayTasks[counterTasks++] = new Task1();
        arrayTasks[counterTasks++] = new Task2();
        arrayTasks[counterTasks++] = new Task3();
        arrayTasks[counterTasks++] = new Task4();
        arrayTasks[counterTasks++] = new Task5();
        arrayTasks[counterTasks++] = new Task6();

        counterTasks = 1;

        for (ArrayTasks arrayTask : arrayTasks) {
            System.out.println("\n_________________________________________________________________________________________________");
            System.out.println("Task" + counterTasks++);

            arrayTask.execute();

            System.out.println("\n-------------------------------------------------------------------------------------------------");

        }

    }

}
