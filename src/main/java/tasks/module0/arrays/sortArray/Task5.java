package tasks.module0.arrays.sortArray;

import tasks.module0.Service;

import java.util.Arrays;

public class Task5 implements ArrayTasks {

    @Override
    public void execute() {

        int[] array = Service.getRandomIntArray(10, 100);

        System.out.println("Source array:\n" + Arrays.toString(array));

        insertionSort(array);
        System.out.println("\nSorted array:\n" + Arrays.toString(array));

    }

    private static void insertionSort(int[] array) {

        int[] end = new int[array.length];

        end[0] = array[0];

        int index;

        for (int i = 1; i < array.length; i++) {

            index = binarySearch(end, array[i], i);

            if (index != i) {

                for (int j = end.length - 1; j > index; j--) {

                    end[j] = end[j - 1];

                }
            }
            end[index] = array[i];

        }

        for (int i = 0; i < array.length; i++) {
            array[i] = end[i];
        }


    }

    private static int binarySearch(int[] array, int val, int lastIndex) {
        int first = 0;

        while (true) {

            int middle = first + (lastIndex - first) / 2;

            if (array[middle] == val) {
                return middle;
            }

            if (first >= lastIndex) {
                return first;
            }

            if (array[middle] > val) {
                lastIndex = middle;
            } else {
                first = middle + 1;
            }

        }

    }

}
