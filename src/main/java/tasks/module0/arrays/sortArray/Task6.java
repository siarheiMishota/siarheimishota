package tasks.module0.arrays.sortArray;

import tasks.module0.Service;

import java.util.Arrays;

public class Task6 implements ArrayTasks {

    @Override
    public void execute() {

        int[] array = Service.getRandomIntArray(10, 150);

        sortByShella(array);
        System.out.println(Arrays.toString(array));

    }


    private static void sortByShella(int array[]) {
        while (true) {

            boolean isIncreasingNot = true;

            for (int i = 1; i < array.length; i++) {
                if (array[i - 1] > array[i]) {
                    int moveValue = array[i];
                    array[i] = array[i - 1];
                    array[i - 1] = moveValue;
                    int indexPrevious = i - 1;

                    while (indexPrevious > 0) {
                        if (array[indexPrevious - 1] > array[indexPrevious]) {
                            int p = array[indexPrevious];
                            array[indexPrevious] = array[indexPrevious - 1];
                            array[indexPrevious - 1] = p;
                        }
                        indexPrevious--;
                    }
                    isIncreasingNot = false;
                }


            }
            if (isIncreasingNot) {
                break;
            }
        }
    }

}
