package tasks.module0.arrays.sortArray;

import tasks.module0.Service;

import java.util.Arrays;

public class Task4 implements ArrayTasks {

    @Override
    public void execute() {

        int[] array = Service.getRandomIncreasingArrayOfInt(6, 100);

        sortBySelection(array);

        System.out.println(Arrays.toString(array));

    }


    private static void sortBySelection(int a[]) {
        int count = 0;

        while (true) {
            boolean isIncreasingNot = true;

            for (int i = 1; i < a.length; i++) {
                if (a[i - 1] > a[i]) {
                    int r = a[i];
                    a[i] = a[i - 1];
                    a[i - 1] = r;
                    isIncreasingNot = false;
                    count++;
                }


            }
            if (isIncreasingNot) {
                break;
            }

        }

    }

}
