package tasks.module0.arrays.sortArray;

import tasks.module0.Service;

import java.util.Arrays;

public class Task3 implements ArrayTasks {

    @Override
    public void execute() {

        int[] array = Service.getRandomIncreasingArrayOfInt(5, 100);


        System.out.println(Arrays.toString(sortBySelection(array)));

    }


    private static int[] sortBySelection(int array[]) {

        for (int i = 0; i < array.length; i++) {
            int maxIndex = maxStartingAtNIndex(array, i);
            if (array[i] < array[maxIndex]) {
                int r = array[i];
                array[i] = array[maxIndex];
                array[maxIndex] = r;
            }

        }
        return array;
    }

    private static int maxStartingAtNIndex(int[] array, int startIndex) {
        int maxValue = array[startIndex];
        int maxIndex = startIndex;

        if (array.length - 1 == startIndex) {
            return startIndex;
        }
        for (int i = startIndex + 1; i < array.length; i++) {
            if (array[i] > maxValue) {
                maxValue = array[i];
                maxIndex = i;
            }
        }
        return maxIndex;
    }

}
