package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

public class Task13 implements  ArrayTasks{

    @Override
    public void execute() {

        int orderMatrix = 6;
        int[][] array = new int[orderMatrix][orderMatrix];

        for (int i = 0; i < orderMatrix; i++) {
            for (int j = 0; j < orderMatrix; j++) {
                if (i % 2 == 0) {
                    array[i][j] = j + 1;
                } else {
                    array[i][j] = orderMatrix - j;
                }
            }
        }

        Service.printTwoDimensionalArrayOfInt(array);


    }
}
