package tasks.module0.arrays.arraysOfArrays;

import static java.lang.Math.*;

public class Task23 implements ArrayTasks {

    @Override
    public void execute() {
        int orderMatrix = 6;
        double[][] matrix = new double[orderMatrix][orderMatrix];

        for (int i = 0; i < orderMatrix; i++) {
            for (int j = 0; j < orderMatrix; j++) {
                matrix[i][j] = sin(toRadians((pow(i, 2) - pow(j, 2) - orderMatrix)));
            }
        }

        for (double[] doubles : matrix) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(String.format("%6.3f  ", doubles[j]));
            }
            System.out.println();
        }
    }

}
