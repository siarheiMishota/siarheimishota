package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

import static tasks.module0.Service.getRandomTwoDimensionIntArray;

public class Task3 implements ArrayTasks {

    @Override
    public void execute() {

        int[][] mass = getRandomTwoDimensionIntArray(6, 8, 150);

        System.out.println("Source array:\n");
        Service.printTwoDimensionalArrayOfInt(mass);

        printFirstAndLastColumn(mass);

    }

    private static void printFirstAndLastColumn(int[][] mass) {
        for (int[] ints : mass) {

            System.out.format("%5d %5d", ints[0], ints[ints.length - 1]);
        }
    }


}
