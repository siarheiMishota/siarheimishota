package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

public class Task25  implements ArrayTasks{

    @Override
    public void execute() {

        int orderMatrix = 6;
        int[] sourceMatrix = Service.getRandomIntArray(orderMatrix, 25);
        int[][] matrix = new int[orderMatrix][orderMatrix];
        int counter=1;

        for (int i = 0; i < orderMatrix; i++) {
            for (int j = 0; j < orderMatrix; j++) {

                matrix[i][j] =counter++;

            }
        }

        System.out.println("Array:");
        Service.printTwoDimensionalArrayOfInt(matrix);

    }

}
