package tasks.module0.arrays.arraysOfArrays;

public class Task19 implements ArrayTasks {

    @Override
    public void execute() {

        int orderMatrix = 6;
        int[][] matrix = new int[orderMatrix][orderMatrix];

        for (int i = 0; i < orderMatrix; i++) {
            for (int j = 0; j < orderMatrix; j++) {
                if (isCorrectCondition(i, j, orderMatrix)) {
                    matrix[i][j] = 0;
                } else {
                    matrix[i][j] = 1;
                }
            }
        }

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j] + "  ");
            }
            System.out.println();
        }
    }

    private static boolean isCorrectCondition(int i, int j, int orderMatrix) {

        if (i < orderMatrix / 2 && j < orderMatrix / 2 && i - j > 0) {
            return true;
        } else if (i < orderMatrix / 2 && j >= orderMatrix / 2 && i + j >= orderMatrix) {
            return true;
        } else if (i >= orderMatrix / 2 && j < orderMatrix / 2 && i + j < orderMatrix - 1) {
            return true;
        } else if (i >= orderMatrix / 2 && j >= orderMatrix / 2 && i - j < 0) {
            return true;
        }
        return false;

    }
}
