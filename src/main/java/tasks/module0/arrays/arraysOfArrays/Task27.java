package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

import static tasks.module0.Service.*;

public class Task27  implements ArrayTasks{

    @Override
    public void execute() {

        int column1, column2;
        int[][] mass = getRandomTwoDimensionIntArray(6, 8, 150);

        System.out.println("Source array:");
        Service.printTwoDimensionalArrayOfInt(mass);
        System.out.println();

        do {
            column1 = getPositiveIntValue("Input number column 1");

        } while (column1 >= mass[0].length);

        do {
            column2 = getPositiveIntValue("Input number column 2");

        } while (column2 >= mass[0].length);


        for (int i = 0; i < mass.length; i++) {
            int r = mass[i][column1 - 1];
            mass[i][column1 - 1] = mass[i][column2 - 1];
            mass[i][column2 - 1] = r;

        }

        printTwoDimensionalArrayOfInt(mass);

    }


}
