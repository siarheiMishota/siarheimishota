package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

import static tasks.module0.Service.getRandomTwoDimensionIntArray;

public class Task5 implements  ArrayTasks {

    @Override
    public void execute() {

        int[][] mass = getRandomTwoDimensionIntArray(6, 8, 150);

        System.out.println("Source array:\n");
        Service.printTwoDimensionalArrayOfInt(mass);
        System.out.println();

        printRowsWithEvenIndex(mass);
    }


    private static void printRowsWithEvenIndex(int[][] mass) {
        for (int i = 0; i < mass.length; i += 2) {
            for (int j = 0; j < mass[0].length; j++) {

                System.out.print(mass[i][j] + "\t");

            }
            System.out.println();
        }
    }

}
