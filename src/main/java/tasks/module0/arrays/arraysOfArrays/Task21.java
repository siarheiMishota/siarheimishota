package tasks.module0.arrays.arraysOfArrays;

public class Task21 implements ArrayTasks {

    @Override
    public void execute() {
        int orderMatrix = 6;
        int[][] matrix = new int[orderMatrix][orderMatrix];

        for (int i = 0; i < orderMatrix; i++) {
            for (int j = 0; j < orderMatrix; j++) {
                if (isCorrectCondition(i, j)) {
                    matrix[i][j] = orderMatrix - i + j;
                } else {
                    matrix[i][j] = 0;
                }
            }
        }

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j] + "  ");
            }
            System.out.println();
        }
    }

    private static boolean isCorrectCondition(int i, int j) {

        return i >= j;

    }

}
