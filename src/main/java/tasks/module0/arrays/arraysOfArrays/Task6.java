package tasks.module0.arrays.arraysOfArrays;

import static tasks.module0.Service.getRandomTwoDimensionIntArray;
import static tasks.module0.Service.printTwoDimensionalArrayOfInt;

public class Task6 implements ArrayTasks {

    @Override
    public void execute() {

        int rowCount = 7;
        int columnCount = 10;

        int[][] a = getRandomTwoDimensionIntArray(rowCount, columnCount, 150);

        int[][] massAfterFilter = new int[rowCount][columnCount];
        int counterColumnMassAfterFilter = 0;

        System.out.println("Source array");
        printTwoDimensionalArrayOfInt(a);
        System.out.println();

        for (int j = 1; j < columnCount; j += 2) {
            if (a[0][j] > a[rowCount - 1][j]) {
                for (int k = 0; k < rowCount; k++) {
                    massAfterFilter[k][counterColumnMassAfterFilter] = a[k][j];
                }
                counterColumnMassAfterFilter++;
            }
        }

        for (int i = 0; i < massAfterFilter.length; i++) {

            if (counterColumnMassAfterFilter >= 0) {

                for (int j = 0; j < counterColumnMassAfterFilter; j++) {
                    System.out.print(massAfterFilter[i][j] + " ");

                }

                System.out.println();

            }
        }

    }


}
