package tasks.module0.arrays.arraysOfArrays;

public class Main {

    public static void main(String[] args) {
        ArrayTasks[] arrayTasks = new ArrayTasks[33];

        int counterTasks = 0;

        arrayTasks[counterTasks++] = new Task1();
        arrayTasks[counterTasks++] = new Task2();
        arrayTasks[counterTasks++] = new Task3();
        arrayTasks[counterTasks++] = new Task4();
        arrayTasks[counterTasks++] = new Task5();
        arrayTasks[counterTasks++] = new Task6();
        arrayTasks[counterTasks++] = new Task8();
        arrayTasks[counterTasks++] = new Task9();
        arrayTasks[counterTasks++] = new Task10();
        arrayTasks[counterTasks++] = new Task11();
        arrayTasks[counterTasks++] = new Task12();
        arrayTasks[counterTasks++] = new Task13();
        arrayTasks[counterTasks++] = new Task14();
        arrayTasks[counterTasks++] = new Task15();
        arrayTasks[counterTasks++] = new Task16();
        arrayTasks[counterTasks++] = new Task17();
        arrayTasks[counterTasks++] = new Task18();
        arrayTasks[counterTasks++] = new Task19();
        arrayTasks[counterTasks++] = new Task20();
        arrayTasks[counterTasks++] = new Task21();
        arrayTasks[counterTasks++] = new Task22();
        arrayTasks[counterTasks++] = new Task23();
        arrayTasks[counterTasks++] = new Task24();
        arrayTasks[counterTasks++] = new Task25();
        arrayTasks[counterTasks++] = new Task27();
        arrayTasks[counterTasks++] = new Task28();
        arrayTasks[counterTasks++] = new Task29();
        arrayTasks[counterTasks++] = new Task30();
        arrayTasks[counterTasks++] = new Task31();
        arrayTasks[counterTasks++] = new Task32();
        arrayTasks[counterTasks++] = new Task33();
        arrayTasks[counterTasks++] = new Task34();
        arrayTasks[counterTasks++] = new Task35();

        counterTasks = 1;

        for (ArrayTasks arrayTask : arrayTasks) {
            System.out.println("\n_________________________________________________________________________________________________");
            System.out.println("Task" + counterTasks++);

            arrayTask.execute();

            System.out.println("\n-------------------------------------------------------------------------------------------------");

        }

    }

}
