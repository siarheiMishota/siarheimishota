package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

public class Task18 implements ArrayTasks {

    @Override
    public void execute() {

        int orderMatrix = 6;
        int[][] matrix = new int[orderMatrix][orderMatrix];

        for (int i = 0; i < orderMatrix; i++) {
            for (int j = 0; j < orderMatrix; j++) {
                if (isCorrectCondition(orderMatrix, i, j)) {
                    matrix[i][j] = 0;
                } else {
                    matrix[i][j] = i + 1;
                }
            }
        }

        Service.printTwoDimensionalArrayOfInt(matrix);
    }

    private static boolean isCorrectCondition(int orderMatrix, int i, int j) {
        return orderMatrix - i - j < 1;
    }

}
