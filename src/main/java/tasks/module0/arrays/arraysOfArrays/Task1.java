package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

public class Task1 implements ArrayTasks {

    @Override
    public void execute() {

        int[][] mass = new int[3][4];

        for (int i = 0; i < mass.length; i++) {
            for (int j = 0; j < mass[i].length; j++) {

                if (j == 0) {
                    mass[i][j] = 1;
                } else {
                    mass[i][j] = 0;

                }

            }
        }

        Service.printTwoDimensionalArrayOfInt(mass);

    }

}
