package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

public class Task9 implements  ArrayTasks{

    @Override
    public void execute() {

        int numberRowAndColumn = 6;
        int[][] mass = Service.getRandomTwoDimensionIntArray(numberRowAndColumn, numberRowAndColumn, 99);

        Service.printTwoDimensionalArrayOfInt(mass);
        System.out.println();

        for (int i = 0; i < mass.length; i++) {
            System.out.print(mass[i][i] + " ");
        }

    }

}
