package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

import java.util.Arrays;

import static tasks.module0.Service.*;

public class Task28  implements ArrayTasks{

    @Override
    public void execute() {

        int[][] mass = getRandomTwoDimensionIntArray(3, 8, 10);

        System.out.println("Source array:");
        Service.printTwoDimensionalArrayOfInt(mass);
        System.out.println();

        int[] sumInColumns=sumArray(mass);
        int indexOfMaxSumColumn=maxIndexOfSumInArray(sumInColumns);

        System.out.println("amount in each column:");
        System.out.println(Arrays.toString(sumInColumns));

        System.out.println();
        System.out.println("Number column with max sum="+(indexOfMaxSumColumn));


    }

    private static int[] sumArray(int[][] a){
        int[]sumArray = new int[a[0].length];
        int index=0;

        for (int j = 0; j < a[0].length; j++) {
            int sum = 0;
            for (int i = 0; i < a.length; i++) {
                sum += a[i][j];
            }
            sumArray[index++] = sum;
        }

        return sumArray;
    }

    private static int maxIndexOfSumInArray(int[] a){
        int max=a[0];
        int index=1;

        for (int i = 0; i < a.length; i++) {
            if (a[i]>max){
                max=a[i];
                index=i+1;
            }
        }

        return index;
    }

}
