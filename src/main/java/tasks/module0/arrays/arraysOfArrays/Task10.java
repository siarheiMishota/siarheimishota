package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

public class Task10 implements ArrayTasks {

    @Override
    public void execute() {

        int numberRow = 6, numberColumn = 10;
        int pRow;
        int kColumn;

        int[][] mass = Service.getRandomTwoDimensionIntArray(numberRow, numberColumn, 99);

        Service.printTwoDimensionalArrayOfInt(mass);
        System.out.println();


        do {
            pRow = Service.getPositiveIntValue("Input number of row");
        } while (pRow >= mass.length);

        do {
            kColumn = Service.getPositiveIntValue("Input number of column");
        } while (kColumn >= mass[0].length);

        System.out.println("k of row");
        printRow(mass, pRow);

        System.out.println("k of column");
        printColumn(mass, kColumn);

    }

    private static void printRow(int[][] ints, int row) {
        for (int j = 0; j < ints[0].length; j++) {
            System.out.print(ints[row][j] + " ");
        }
        System.out.println();
    }

    private static void printColumn(int[][] ints, int column) {
        for (int i = 0; i < ints.length; i++) {
            System.out.println(ints[i][column] + " ");
        }
    }

}
