package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

import java.util.Random;

public class Task34 implements ArrayTasks {

    @Override
    public void execute() {

        int numberOfRow = 6, numberOfColumn = 8;
        int[][] matrix = getRandomArrayWithZeroAndOne(numberOfRow, numberOfColumn);


        Service.printTwoDimensionalArrayOfInt(matrix);
    }


    private static int[][] getRandomArrayWithZeroAndOne(int m, int n) {
        Random rand = new Random();
        int[][] a = new int[m][n];

        for (int j = 0; j < n; j++) {
            int count = 0;
            for (int i = 0; i < m; i++) {

                if (count < j) {
                    a[i][j] = 1;
                    count++;
                } else {
                    a[i][j] = 0;
                }
            }

        }

        return a;
    }

}
