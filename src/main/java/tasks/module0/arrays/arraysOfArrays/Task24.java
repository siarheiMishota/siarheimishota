package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

public class Task24 implements ArrayTasks {

    @Override
    public void execute() {
        int orderMatrix = 6;
        int[] sourceMatrix = Service.getRandomIntArray(orderMatrix, 25);
        double[][] matrix = new double[orderMatrix][orderMatrix];

        for (int i = 0; i < orderMatrix; i++) {
            for (int j = 0; j < orderMatrix; j++) {

                matrix[i][j] = Math.pow(sourceMatrix[j], i + 1);

            }
        }

        System.out.println("Array:");
        Service.printTwoDimensionalArrayOfDouble(matrix);

    }

}
