package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

public class Task4 implements  ArrayTasks{

    @Override
    public void execute() {

        int[][] mass = Service.getRandomTwoDimensionIntArray(6, 10, 200);

        System.out.println("Source array:\n");
        Service.printTwoDimensionalArrayOfInt(mass);
        System.out.println();

        printFirstAndLastRows(mass);
    }

    private static void printFirstAndLastRows(int[][] mass) {
        for (int i = 0; i < mass[0].length; i++) {

            System.out.print(mass[0][i] + "\t");

        }

        System.out.println();

        for (int i = 0; i < mass[0].length; i++) {

            System.out.print(mass[mass.length - 1][i] + "\t");

        }
    }

}
