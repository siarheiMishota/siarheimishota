package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

import static tasks.module0.Service.getRandomTwoDimensionIntArray;

public class Task29  implements ArrayTasks{

    @Override
    public void execute() {

        int[][] mass = getRandomTwoDimensionIntArray(8, 8, 20);

        System.out.println("Source array:");
        Service.printTwoDimensionalArrayOfInt(mass);
        System.out.println();

        for (int i = 0; i < mass.length; i++) {
            if (mass[i][i] > 0) {
                System.out.print(mass[i][i] + " ");
            }
        }


    }

}
