package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

public class Task11 implements ArrayTasks{

    @Override
    public void execute() {

        int numberRow = 6, numberColumn = 10;
        int[][] mass = Service.getRandomTwoDimensionIntArray(numberRow, numberColumn, 99);

        Service.printTwoDimensionalArrayOfInt(mass);
        System.out.println();

        System.out.println("Changed version");
        for (int i = 0; i < mass.length; i++) {
            if (i % 2 == 0) {
                printRow(mass, i);
            } else {
                printRowWithEnd(mass, i);
            }
        }


    }

    private static void printRow(int[][] ints, int row) {
        for (int j = 0; j < ints[0].length; j++) {
            System.out.print(String.format("%3d", ints[row][j]) + " ");
        }
        System.out.println();
    }

    private static void printRowWithEnd(int[][] ints, int row) {

        for (int j = ints[0].length - 1; j >= 0; j--) {
            System.out.print(String.format("%3d", ints[row][j]) + " ");
        }
        System.out.println();

    }
}
