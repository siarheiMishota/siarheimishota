package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

import static tasks.module0.Service.getRandomTwoDimensionIntArray;
import static tasks.module0.Service.printTwoDimensionalArrayOfInt;

public class Task32  implements ArrayTasks{

    @Override
    public void execute() {

        int[][] mass = getRandomTwoDimensionIntArray(10, 10, 100);

        System.out.println("Source array:");
        Service.printTwoDimensionalArrayOfInt(mass);
        System.out.println();

        System.out.println("the array is sorted in ascending order:");
        sortAscending(mass);
        printTwoDimensionalArrayOfInt(mass);
        System.out.println();

        System.out.println("the array is sorted in descending order:");
        sortDescending(mass);
        printTwoDimensionalArrayOfInt(mass);

    }

    private static void sortAscending(int[][] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = a[0].length - 1; j > 0; j--) {
                for (int k = 0; k < j; k++) {
                    if (a[i][k] > a[i][k + 1]) {
                        int tmp = a[i][k];
                        a[i][k] = a[i][k + 1];
                        a[i][k + 1] = tmp;
                    }
                }
            }
        }
    }

    private static void sortDescending(int[][] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = a[0].length - 1; j > 0; j--) {
                for (int k = 0; k < j; k++) {
                    if (a[i][k] < a[i][k + 1]) {
                        int tmp = a[i][k];
                        a[i][k] = a[i][k + 1];
                        a[i][k + 1] = tmp;
                    }
                }
            }
        }
    }


}
