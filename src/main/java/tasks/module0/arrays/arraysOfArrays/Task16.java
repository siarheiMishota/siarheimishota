package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

public class Task16 implements  ArrayTasks {

    @Override
    public void execute() {

        int orderMatrix = 10;

        int[][] matrix = new int[orderMatrix][orderMatrix];

        for (int i = 0; i < matrix.length; i++) {

            for (int j = 0; j < matrix[0].length; j++) {

                if (isCorrectCondition(i, j, matrix.length)) {
                    int numberI = i + 1;

                    matrix[i][j] = numberI * (numberI + 1);

                } else {
                    matrix[i][j] = 0;
                }

            }

        }

        System.out.println("Array:");
        Service.printTwoDimensionalArrayOfInt(matrix);

    }

    private static boolean isCorrectCondition(int i, int j, int lengthMass) {

        return i == j;


    }


}
