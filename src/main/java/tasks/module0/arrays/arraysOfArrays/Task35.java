package tasks.module0.arrays.arraysOfArrays;

import static tasks.module0.Service.getRandomTwoDimensionIntArray;
import static tasks.module0.Service.printTwoDimensionalArrayOfInt;

public class Task35  implements ArrayTasks{

    @Override
    public void execute() {

        int[][] mass = getRandomTwoDimensionIntArray(10, 10, 100);

        System.out.println("Source array:");
        printTwoDimensionalArrayOfInt(mass);
        System.out.println();

        replaceAllOddElements(mass, max(mass));
        printTwoDimensionalArrayOfInt(mass);


    }


    private static int max(int[][] a) {
        int maxValue = a[0][0];
        for (int[] ints : a) {
            for (int j = 0; j < a[0].length; j++) {
                if (ints[j] > maxValue) {
                    maxValue = ints[j];
                }
            }
        }
        return maxValue;
    }

    private static int[][] replaceAllOddElements(int[][] a, int max) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                if (a[i][j] % 2 != 0) {
                    a[i][j] = max;
                }
            }
        }
        return a;
    }

}
