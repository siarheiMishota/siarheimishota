package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

import static tasks.module0.Service.getRandomTwoDimensionIntArray;
import static tasks.module0.Service.printTwoDimensionalArrayOfInt;

public class Task33  implements ArrayTasks{

    @Override
    public void execute() {

        int[][] mass = getRandomTwoDimensionIntArray(10, 10, 100);

        System.out.println("Source array:");
        Service.printTwoDimensionalArrayOfInt(mass);
        System.out.println();

        System.out.println("the array is sorted in ascending order: ");
        sortAscending(mass);
        printTwoDimensionalArrayOfInt(mass);
        System.out.println();

        System.out.println("the array is sorted in descending order:");
        sortDescending(mass);
        printTwoDimensionalArrayOfInt(mass);




    }

    private static void sortAscending(int[][] a) {
        for (int j = 0; j < a[0].length; j++) {
            for (int i = a.length - 1; i > 0; i--) {
                for (int k = 0; k < i; k++) {
                    if (a[k][j] > a[k + 1][j]) {
                        int tmp = a[k][j];
                        a[k][j] = a[k + 1][j];
                        a[k + 1][j] = tmp;
                    }
                }
            }
        }
    }

    private static void sortDescending(int[][] a) {
        for (int j = 0; j < a[0].length; j++) {
            for (int i = a.length - 1; i > 0; i--) {
                for (int k = 0; k < i; k++) {
                    if (a[k][j] < a[k + 1][j]) {
                        int tmp = a[k][j];
                        a[k][j] = a[k + 1][j];
                        a[k + 1][j] = tmp;
                    }
                }
            }
        }
    }

}
