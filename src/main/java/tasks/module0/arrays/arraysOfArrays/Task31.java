package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

import static tasks.module0.Service.getRandomTwoDimensionIntArray;

public class Task31  implements ArrayTasks{

    @Override
    public void execute() {

        int[][] mass = getRandomTwoDimensionIntArray(10, 10, 1000);

        System.out.println("Source array:");
        Service.printTwoDimensionalArrayOfInt(mass);
        System.out.println();

        int counterTwoDigitsNumbers = 0;

        for (int i = 0; i < mass.length; i++) {
            for (int j = 0; j < mass[0].length; j++) {
                if (numberOfDigitsInNumber(mass[i][j]) == 2) {
                    counterTwoDigitsNumbers++;
                }
            }

        }

        System.out.println("Number of two-digits numbers of mass= "+counterTwoDigitsNumbers);


    }

    private static int numberOfDigitsInNumber(int number) {

        int counter = 0;

        while (number > 0) {
            number /= 10;
            counter++;
        }
        return counter;


    }

}
