package tasks.module0.arrays.arraysOfArrays;

import static tasks.module0.Service.getRandomTwoDimensionIntArray;
import static tasks.module0.Service.printTwoDimensionalArrayOfInt;

public class Task8 implements  ArrayTasks{

    @Override
    public void execute() {

        int rowCount = 8;
        int columnCount = 10;
        int seven = 7;
        int sevenCounter = 0;

        int[][] mass = getRandomTwoDimensionIntArray(rowCount, columnCount, 150);

        System.out.println("Source array");
        printTwoDimensionalArrayOfInt(mass);
        System.out.println();

        for (int[] ints : mass) {

            for (int anInt : ints) {

                if (anInt == seven) {

                    sevenCounter++;

                }

            }

        }

        System.out.println("7 occurs in mass=" + sevenCounter);

    }

}
