package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

import static tasks.module0.Service.getRandomTwoDimensionIntArray;

public class Task2 implements ArrayTasks {

    @Override
    public void execute() {

        int[][] mass = getRandomTwoDimensionIntArray(6, 8, 150);

        Service.printTwoDimensionalArrayOfInt(mass);

    }
}
