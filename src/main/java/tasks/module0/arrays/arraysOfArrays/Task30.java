package tasks.module0.arrays.arraysOfArrays;

import tasks.module0.Service;

import static tasks.module0.Service.getRandomTwoDimensionIntArray;

public class Task30 implements ArrayTasks {

    @Override
    public void execute() {

        int[][] mass = getRandomTwoDimensionIntArray(10, 20, 15);

        System.out.println("Source array:");
        Service.printTwoDimensionalArrayOfInt(mass);
        System.out.println();

        System.out.print("rows with the number of fives more than 3- ");
        for (int i = 0; i < mass.length; i++) {
            int count = 0;
            for (int j = 0; j < mass[0].length; j++) {
                if (mass[i][j] == 5) {
                    count++;
                }
            }
            if (count >= 3) {
                System.out.print((i + 1) + ", ");
            }

        }


    }


}
