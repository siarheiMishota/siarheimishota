package tasks.module0;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

public class Service {

    public static boolean isPositive(int side) {
        return side > 0;
    }

    public static int getIntValue(String message) {
        System.out.println(message);

        Scanner scanner = new Scanner(System.in);
        while (true) {
            if (scanner.hasNextInt()) {
                return scanner.nextInt();
            } else if (scanner.hasNext()) {
                System.out.println("You have entered an invalid number, input real number");
                scanner.next();
            }
        }

    }

    public static long getLongValue(String message) {
        System.out.println(message);

        Scanner scanner = new Scanner(System.in);

        while (true) {
            if (scanner.hasNextLong()) {
                return scanner.nextLong();
            } else if (scanner.hasNext()) {
                System.out.println("You have entered an invalid number, input real number");
                scanner.next();
            }
        }

    }

    public static double getDoubleValue(String message) {
        System.out.println(message);

        Scanner scanner = new Scanner(System.in);

        while (true) {
            if (scanner.hasNextDouble()) {
                return scanner.nextDouble();
            } else if (scanner.hasNext()) {
                System.out.println("You have entered an invalid number, input real number");
                scanner.next();
            }
        }

    }

    public static String getStringValue(String message) {
        Scanner scanner = new Scanner(System.in);

        System.out.println(message);

        while (true) {
            if (scanner.hasNext("\\D+")) {
                return scanner.next("\\D+");
            } else {
                System.out.println("You have entered an invalid string, input string only letters");
                scanner.next();
            }
        }
    }

    public static int getPositiveIntValue(String message) {

        int t;
        System.out.print(message + ": ");
        while (true) {
            t = getIntValue("input positive integer number");

            if (t > 0) {
                return t;
            }
        }

    }

    public static int getPositiveAndZeroIntValueTillMaxNumber(String message, int maxNumber) {

        int number;
        System.out.print(message + ": \n");
        while (true) {
            number = getIntValue("input positive integer number");

            if (number > 0 && number < maxNumber) {
                return number;
            }
        }

    }


    public static String getStringLine(String message) {
        Scanner scanner = new Scanner(System.in);

        System.out.println(message);

        while (true) {
            if (scanner.hasNextLine()) {
                return scanner.nextLine();
            } else {
                System.out.println("You have entered an invalid string, input string only letters");
                scanner.next();
            }
        }
    }

    public static int[] getArrayInt(String s) {
        int n;
        int[] a;

        System.out.println(s);

        n = getPositiveIntValue("Input size the array");

        a = new int[n];

        for (int i = 0; i < n; i++) {
            a[i] = getIntValue("Input the number " + i);
        }
        return a;
    }

    public static int[] getNaturalArray(String s) {
        int n;
        int[] a;

        System.out.println(s);

        n = getPositiveIntValue("Input size the array");

        a = new int[n];

        for (int i = 0; i < n; i++) {
            a[i] = getPositiveIntValue("Input the number " + i);
        }
        return a;
    }

    public static double[] getArrayDouble(String s) {
        int n;
        double[] a;

        System.out.println(s);

        n = getPositiveIntValue("Input the number of elements in the array");

        a = new double[n];

        for (int i = 0; i < n; i++) {
            a[i] = getDoubleValue("Input double number " + i);
        }
        return a;
    }

    public static String getStringValueWithoutNumbers(String message) {
        Scanner scanner = new Scanner(System.in);


        while (true) {
            System.out.print(message + "  ");

            if (scanner.hasNext("\\D+")) {
                return scanner.next("\\D+");
            } else {
                System.err.println("You have entered an invalid string, input string only letters");
                scanner.nextLine();
            }
        }
    }


    public static int[][] getDoubleArrayOfInt(String s) {
        int m, n;
        int[][] a;

        System.out.println(s);

        m = getPositiveIntValue("Input size m of array");
        n = getPositiveIntValue("Input size n of array");

        a = new int[n][m];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < a[0].length; j++) {
                a[i][j] = getIntValue("Input the number " + i + "*" + j);
            }
        }
        return a;
    }

    public static int[][] getRandomTwoDimensionIntArray(int countRow, int countColumn, int limitForRandom) {

        Random random = new Random();
        int[][] mass = new int[countRow][countColumn];

        for (int i = 0; i < mass.length; i++) {
            for (int j = 0; j < mass[i].length; j++) {

                mass[i][j] = random.nextInt(limitForRandom);

            }
        }

        return mass;
    }

    public static long getRandomLong() {

        Random random = new Random();

        return random.nextLong();

    }

    public static int getRandomInt(int limit) {

        Random random = new Random();

        return random.nextInt(limit);

    }


    public static int[] getRandomIntArray(int countElements, int limitForRandom) {

        Random random = new Random();
        int[] mass = new int[countElements];

        for (int i = 0; i < mass.length; i++) {

            mass[i] = random.nextInt(limitForRandom);

        }

        return mass;
    }


    public static int[] getRandomIncreasingArrayOfInt(int countElements, int limitForRandom) {

        Random random = new Random();
        int[] mass = new int[countElements];

        do {
            mass[0] = random.nextInt(limitForRandom);
        } while (mass[0] > limitForRandom / 4);

        for (int i = 1; i < mass.length; i++) {

            do {

                mass[i] = random.nextInt(limitForRandom);

            } while (mass[i] < mass[i - 1]);

        }

        return mass;
    }

    public static Set<DayOfWeek> getDaysOfWeek(String message) {

        System.out.println(message);

        Set<DayOfWeek> paramDaysOfWeek = new HashSet<>();

        int userSelection;

        boolean isNotEmpty = false;

        while (true) {
            userSelection = getIntValue("Input number of day of week 0 < number <= 7:");

            if (userSelection >= 0 && userSelection < 7) {

                paramDaysOfWeek.add(DayOfWeek.of(userSelection));
                isNotEmpty = true;

            } else if (isNotEmpty) {
                break;
            }

        }
        return paramDaysOfWeek;
    }

    public static LocalTime getLocalTime(String message) {
        System.out.println(message);

        int hour, minute;

        do {

            hour = getPositiveIntValue("Input hour ");
        } while (hour < 0 || hour > 23);


        System.out.println(message + " minute");

        do {

            minute = getPositiveIntValue("Input minute ");
        } while (minute < 0 || minute > 59);
        return LocalTime.of(hour, minute);
    }

    public static int[] digitsOfNumber(int n) {
        int count = countDigits(n);
        int[] digits;
        int i = count - 1;

        digits = new int[count];
        for (int j = 0; j < count; j++) {
            {
                digits[i] = n % 10;
                n /= 10;
                i--;
            }

        }
        return digits;
    }

    public static int countDigits(int n) {
        int count = 0;

        while (n > 0) {
            n /= 10;
            count++;
        }
        return count;

    }

    public static int[][] copyInNewTwoDimensionArrayWithNeedLength(int[][] inputArray, int lengthRow,
                                                                   int lengthColumn) {

        int[][] newArray = new int[lengthRow][lengthColumn];

        for (int i = 0; i < lengthRow; i++) {
            for (int j = 0; j < newArray[0].length; j++) {
                newArray[i][j] = inputArray[i][j];
            }
        }
        return newArray;


    }

    public static int[] copyInNewArrayWithNeedLength(int[] inputArray, int length) {

        int[] newArray = new int[length];

        for (int i = 0; i < length; i++) {
            newArray[i] = inputArray[i];
        }

        return newArray;


    }

    public static void printTwoDimensionalArrayOfInt(int[][] arrayIn) {

        for (int[] ints : arrayIn) {

            for (int anInt : ints) {

                System.out.format("%6d", anInt);
            }
            System.out.println();
        }

    }

    public static void printTwoDimensionalArrayOfDouble(double[][] arrayIn) {

        for (double[] doubles : arrayIn) {

            for (double anDouble : doubles) {

                System.out.format("%14.2f", anDouble);
            }
            System.out.println();
        }

    }
}
