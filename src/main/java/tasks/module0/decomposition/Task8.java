package tasks.module0.decomposition;

import tasks.module0.Service;

import java.util.Arrays;

public class Task8 implements ArrayTasks {

    private static int getBeforeMaxNumberInArray(int[] array) {

        Arrays.sort(array);

        return array[array.length - 2];

    }

    @Override
    public void execute() {

        int numberElements = 10;
        int[] array = Service.getRandomIntArray(numberElements, 150);

        System.out.println("Source array:");
        System.out.println(Arrays.toString(array));

        System.out.println("Before maximum element=" + getBeforeMaxNumberInArray(array));

    }

}
