package tasks.module0.decomposition;

import tasks.module0.Service;

import java.util.Arrays;

public class Task11 implements ArrayTasks {

    private static int sumNumbersOfArrayFromStartToEnd(int[] a, int start, int end) {
        int sum = 0;

        if (isEndValueMoreThenLengthArray(a, end)) {
            end = a.length - 1;
        }

        if (isStartMoreThenEnd(start, end)) {
            int intermediate = start;
            start = end;
            end = intermediate;
        }

        for (int i = start; i < end; i++) {
            sum += a[i];
        }

        return sum;
    }

    private static boolean isStartMoreThenEnd(int start, int end) {
        return start > end;
    }

    private static boolean isEndValueMoreThenLengthArray(int[] a, int end) {
        return end > a.length - 1;
    }

    @Override
    public void execute() {

        int numberOfElements = 9;
        int[] array = Service.getRandomIntArray(numberOfElements, 150);

        System.out.println("Source array:");
        System.out.println(Arrays.toString(array));

        for (int i = 0; i < array.length - 1; i = i + 2) {

            int k = 3;

            if (isStartMoreThenEnd(i + k - 1, array.length)) {
                k = array.length - i - 1;
            }
            System.out.println("sum from D[" + (i + 1) + "] to D[" + (i + k) + "]= " + sumNumbersOfArrayFromStartToEnd(array, i, i + k));


        }

    }

}
