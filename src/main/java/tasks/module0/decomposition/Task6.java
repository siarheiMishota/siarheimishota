package tasks.module0.decomposition;

public class Task6 implements ArrayTasks {

    public static double area(int sideA, int sideB, int sideC) {

        double halfPerimeter = halfPerimeter(sideA, sideB, sideC);

        return Math.sqrt(Math.abs(halfPerimeter * (halfPerimeter - sideA) - (halfPerimeter - sideB) * (halfPerimeter - sideC)));

    }

    public static double halfPerimeter(int sideA, int sideB, int sideC) {

        return (sideA + sideB + sideC) / 2.0;

    }

    @Override
    public void execute() {

        int sideA = 8, sideB = 10, sideC = 3;
        double area = area(sideA, sideB, sideC);

        System.out.format("side a=%d,side b=%d,side c=%d, area=%.2f", sideA, sideB, sideC, area);

    }

}
