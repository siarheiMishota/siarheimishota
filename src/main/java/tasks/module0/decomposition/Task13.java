package tasks.module0.decomposition;

import tasks.module0.Service;

import java.util.Arrays;

public class Task13 implements ArrayTasks {

    private static long[] digits(long numberIn) {

        numberIn = Math.abs(numberIn);
        int count = numberOfDigitsInNumber(numberIn);

        long[] output = new long[count];

        for (int i = count - 1; i >= 0; i--) {

            output[i] = numberIn % 10;
            numberIn /= 10;
        }

        return output;
    }

    private static int numberOfDigitsInNumber(long b) {
        int count = 0;

        while (b > 0) {
            b = b / 10;
            count++;
        }
        return count;
    }

    @Override
    public void execute() {

        long number = Service.getRandomLong();

        System.out.println("Source number=" + number);

        System.out.println(Arrays.toString(digits(number)));
    }

}
