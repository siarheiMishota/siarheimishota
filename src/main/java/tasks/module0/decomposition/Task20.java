package tasks.module0.decomposition;

import tasks.module0.Service;

import static tasks.module0.Service.digitsOfNumber;

public class Task20 implements ArrayTasks {

    @Override
    public void execute() {

        int number = Service.getRandomInt(2000);
        int count = countOfSubtraction(number);

        System.out.println("Source number=" + number);
        System.out.println("count= " + count);


    }


    private static int countOfSubtraction(int n) {
        int count = 0;

        while (n != 0) {
            int sum = sumNumbersOfArray(digitsOfNumber(n));
            n -= sum;
            count++;
        }

        return count;

    }

    private static int sumNumbersOfArray(int[] a) {
        int sum = 0;

        for (int value : a) {
            sum += value;
        }
        return sum;
    }


}
