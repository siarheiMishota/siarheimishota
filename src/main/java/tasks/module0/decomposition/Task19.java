package tasks.module0.decomposition;

import java.util.Arrays;

import static java.lang.Math.pow;
import static tasks.module0.Service.*;

public class Task19 implements ArrayTasks {

    private static int[] getAllOddNumbers(int n) {
        int[] digits;
        int counter = 0, length = (int) pow(10, n), start = (int) pow(10, n - 1), end = (int) pow(10, n) - 1;

        digits = new int[length];

        for (int i = start; i < end; i++) {

            if (isOddNumber(i)) {
                digits[counter] = i;
                counter++;
            }
        }

        return copyInNewArrayWithNeedLength(digits, counter);
    }

    private static boolean isOddNumber(int n) {
        int countDigits = countDigits(n);
        int[] digitsOfNumber = digitsOfNumber(n);

        for (int i = 0; i < countDigits; i++) {
            if (digitsOfNumber[i] % 2 == 0) {
                return false;
            }
        }
        return true;
    }

    private static int sumNumbersOfArray(int[] a) {
        int sum = 0;

        for (int value : a) {
            sum += value;
        }
        return sum;
    }

    private static int countEvenNumber(int n) {
        int count = 0;
        int countDigits = countDigits(n);
        int[] digits = digitsOfNumber(n);

        for (int i = 0; i < countDigits; i++) {
            if (digits[i] % 2 == 0) {
                count++;
            }
        }
        return count;
    }

    @Override
    public void execute() {

        int number = getRandomInt(4);
        int[] arrayOfOddNumbers = getAllOddNumbers(number);

        int sum = sumNumbersOfArray(arrayOfOddNumbers);
        int countEvenNumber = countEvenNumber(sum);

        System.out.println("Source Array- " + Arrays.toString(arrayOfOddNumbers));
        System.out.println("sum of array= " + sum);
        System.out.println("Number of even number at sum= " + countEvenNumber);


    }


}
