package tasks.module0.decomposition;

public class Task2 implements ArrayTasks {

    private static long nod(long a, long b) {
        return b == 0 ? a : nod(b, a % b);
    }

    private static long nok(long a, long b) {
        return a / nod(a, b) * b;
    }

    @Override
    public void execute() {

        int numberA = 9, numberB = 42;

        System.out.format("a=%d, b=%d, NOD=%d", numberA, numberB, nod(numberA, numberB));
        System.out.format("a=%d, b=%d, NOK=%d", numberA, numberB, nok(numberA, numberB));


    }

}
