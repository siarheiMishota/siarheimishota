package tasks.module0.decomposition;

import tasks.module0.Service;

import java.util.Arrays;

public class Task4 implements ArrayTasks {

    private static long nokFromArray(int[] array) {
        int result = array[0];
        for (int i = 1; i < array.length; i++) {
            result = nok(result, array[i]);
        }
        return result;
    }

    private static int nok(int a, int b) {
        return a / nod(a, b) * b;
    }

    private static int nod(int a, int b) {
        return b == 0 ? a : nod(b, a % b);
    }

    @Override
    public void execute() {

        int numbersOfElements = 3;
        int[] numbers = Service.getRandomIntArray(numbersOfElements, 150);

        System.out.println("Source numbers:");
        System.out.println(Arrays.toString(numbers));
        System.out.println();

        System.out.format(" NOK=%d", nokFromArray(numbers));

    }


}
