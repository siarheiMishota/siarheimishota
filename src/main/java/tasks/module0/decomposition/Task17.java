package tasks.module0.decomposition;

import java.util.Arrays;

import static tasks.module0.Service.*;

public class Task17 implements ArrayTasks {

    private static int[] numbersArmstrong(int end) {
        int[] arrayNumberOfArmstrong = new int[(end - 1) / 2];

        int counter = 0;

        for (int i = 1; i <= end; i++) {

            int[] digitsOfNumber = digitsOfNumber(i);
            int sumDigitsOfNumber = sumDigitsInPow(digitsOfNumber, digitsOfNumber.length);

            if (isArmstrong(i, sumDigitsOfNumber)) {
                arrayNumberOfArmstrong[counter] = i;
                counter++;
            }
        }

        return copyInNewArrayWithNeedLength(arrayNumberOfArmstrong, counter);

    }

    private static int sumDigitsInPow(int[] a, int n) {
        int sum = 0;
        for (int j = 0; j < a.length; j++) {
            sum += Math.pow(a[j], n);
        }
        return sum;
    }

    private static boolean isArmstrong(int number, int sumDigitsOfNumber) {
        return sumDigitsOfNumber == number;
    }

    @Override
    public void execute() {

        int number = getRandomInt(250_000);

        int[] numbersOfArmstrong = numbersArmstrong(number);

        System.out.println("Numbers of Armstrong from 1 to " + number + "- " + Arrays.toString(numbersOfArmstrong));


    }


}
