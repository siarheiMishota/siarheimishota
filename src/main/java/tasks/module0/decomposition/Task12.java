package tasks.module0.decomposition;

public class Task12 implements ArrayTasks {

    private static double areaOfQuadrilateral(int x, int y, int z, int t) {

        return 1.0 / 2 * x * (y + t);

    }

    @Override
    public void execute() {

        int x = 10, y = 5, z = 7, t = 12;

        System.out.println("Area=" + areaOfQuadrilateral(x, y, z, t));


    }

}
