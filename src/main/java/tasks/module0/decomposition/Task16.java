package tasks.module0.decomposition;

import tasks.module0.Service;

import static tasks.module0.Service.copyInNewTwoDimensionArrayWithNeedLength;

public class Task16 implements ArrayTasks {

    private static int[][] twinsSimples(int number) {
        int k = 0, count = 0, m = 2;
        int[][] array = new int[number][m];
        int[][] out;

        for (int i = number; i < 2 * number - 2; i++) {
            if (isSimpleNumberAndNumberThtoughOne(i)) {
                array[k][0] = i;
                array[k][1] = i + 2;
                k++;
                count++;
            }
        }

        out = copyInNewTwoDimensionArrayWithNeedLength(array, count, m);

        return out;
    }

    private static boolean isSimpleNumberAndNumberThtoughOne(int i) {
        return isSimple(i) && isSimple(i + 2);
    }

    private static boolean isSimple(int n) {
        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                return false;
            }
        }

        return true;
    }

    @Override
    public void execute() {

        int number = 4000;

        int[][] twinsOfSimple = twinsSimples(number);

        System.out.println("prime numbers which is twins:");
        Service.printTwoDimensionalArrayOfInt(twinsOfSimple);


    }


}
