package tasks.module0.decomposition;

import tasks.module0.Service;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class Task7 implements ArrayTasks {

    private static double distanceBetweenPoints(int x0, int y0, int x1, int y1) {
        return sqrt(pow((x1 - x0), 2) + pow((y1 - y0), 2));
    }

    @Override
    public void execute() {
        int numberOfPoints = 10;
        int[][] coordinateOfPoints = Service.getRandomTwoDimensionIntArray(numberOfPoints, 2, 150);
        double max = 0;
        int maxPoint1 = 0, maxPoint2 = 0;

        System.out.println("Source points");
        Service.printTwoDimensionalArrayOfInt(coordinateOfPoints);


        for (int i = 0; i < coordinateOfPoints.length; i++) {
            for (int j = i + 1; j < coordinateOfPoints.length; j++) {
                double v = distanceBetweenPoints(coordinateOfPoints[i][0], coordinateOfPoints[i][1], coordinateOfPoints[j][0], coordinateOfPoints[j][1]);
                if (v > max) {
                    max = v;
                    maxPoint1 = i;
                    maxPoint2 = j;
                }
            }
        }

        System.out.println("max distance between points- " + maxPoint1 + " - " + maxPoint2 + " = " + max);

    }

}
