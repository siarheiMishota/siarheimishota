package tasks.module0.decomposition;

import tasks.module0.Service;

import java.util.Arrays;

public class Task3 implements ArrayTasks {

    private static int nod(int a, int b) {
        return b == 0 ? a : nod(b, a % b);
    }

    private static long nodFromArray(int[] array) {
        int result = array[0];
        for (int i = 1; i < array.length; i++) {
            result = nod(result, array[i]);
        }
        return result;
    }

    @Override
    public void execute() {

        int numbersOfElements = 4;
        int[] numberA = Service.getRandomIntArray(numbersOfElements, 150);

        System.out.println("Source numbers:");
        System.out.println(Arrays.toString(numberA));
        System.out.println();

        System.out.format(" NOD=%d", nodFromArray(numberA));

    }

}
