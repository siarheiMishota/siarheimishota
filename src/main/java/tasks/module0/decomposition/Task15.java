package tasks.module0.decomposition;

import java.util.Arrays;

import static tasks.module0.Service.*;

public class Task15 implements ArrayTasks {

    private static int[] createK(int k, int n) {

        int[] outWithZero = new int[n];
        int[] out;
        int[] middle;
        int count = 0;

        if (k < 1 && n < 1) {
            return null;
        }

        for (int i = 1; i < n; i++) {

            middle = digitsOfNumber(i);

            int sum = sumOfElementsInArray(middle);

            if (sum == k) {
                outWithZero[count] = i;
                count++;
            }

        }

        out = copyInNewArrayWithNeedLength(outWithZero, count);


        return out;
    }

    private static int sumOfElementsInArray(int[] middle) {

        int sum = 0;

        for (int value : middle) {
            sum += value;
        }
        return sum;
    }

    @Override
    public void execute() {

        int k, n;

        k = getPositiveIntValue("Input number" + " k");
        n = getPositiveIntValue("Input number" + " n");

        System.out.println("Arrays:");
        System.out.println(Arrays.toString(createK(k, n)));
    }


}
