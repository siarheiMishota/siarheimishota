package tasks.module0.decomposition;

import tasks.module0.Service;

import java.util.Arrays;

public class Task9 implements ArrayTasks {

    private static long gcd(long a, long b) {
        return b == 0 ? a : gcd(b, a % b);
    }

    @Override
    public void execute() {

        int[] array = Service.getRandomIntArray(3, 50);
        int simple = 1;
        boolean isComposite = false;


        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (gcd(array[i], array[j]) != simple) {
                    isComposite = true;
                }
            }
        }

        System.out.println("Source array:");
        System.out.println(Arrays.toString(array));

        if (isComposite) {
            System.out.println("Numbers isn't mutually simple");
        } else {
            System.out.println("numbers is mutually simple");
        }
    }


}
