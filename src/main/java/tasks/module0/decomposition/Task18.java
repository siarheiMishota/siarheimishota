package tasks.module0.decomposition;

import java.util.Arrays;

import static java.lang.Math.pow;
import static tasks.module0.Service.*;

public class Task18 implements ArrayTasks {

    private static int[] getAllIncreasingNumbers(int degree) {
        int counter = 0, startNumber = (int) pow(10, degree - 1), endNumber = (int) pow(10, degree) - 1;
        int lengthArray = (int) pow(10, degree);

        int[] increasingNumbers = new int[lengthArray];

        for (int i = startNumber; i < endNumber; i++) {

            if (isIncreasingNumber(i)) {
                increasingNumbers[counter] = i;
                counter++;
            }
        }
        return copyInNewArrayWithNeedLength(increasingNumbers, counter);

    }

    private static boolean isIncreasingNumber(int number) {
        int countDigits = countDigits(number);
        int[] digitsOfNumber = digitsOfNumber(number);

        for (int i = 1; i < countDigits; i++) {
            if (digitsOfNumber[i] <= digitsOfNumber[i - 1]) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void execute() {

        int degree = getRandomInt(9);

        System.out.format("Source degree=%d\n", degree);

        int[] arrayOfIncreasingDigitsInNumber = getAllIncreasingNumbers(degree);

        System.out.println(Arrays.toString(arrayOfIncreasingDigitsInNumber));


    }


}
