package tasks.module0.decomposition;

public class Task10 implements ArrayTasks {

    private static int factorial(int a) {
        int res;
        if (a == 1) {
            return 1;
        }
        res = factorial(a - 1) * a;
        return res;

    }

    private static long productOddFractalFromStartToEnd(int start, int end) {
        long product = 1;
        for (int i = start; i <= end; i++) {
            if (i % 2 != 0) {
                product *= factorial(i);
            }
        }

        return product;
    }

    @Override
    public void execute() {

        System.out.println("sum factorials from 1 to 9:");
        System.out.println(productOddFractalFromStartToEnd(1, 9));
    }

}
