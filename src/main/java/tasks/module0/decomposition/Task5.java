package tasks.module0.decomposition;

import tasks.module0.Service;

import java.util.Arrays;

public class Task5 implements ArrayTasks {

    private static int sumMinAndMaxInArray(int[] array) {

        return max(array) + min(array);

    }

    private static int max(int[] arrayIn) {

        if (arrayIn == null) {
            throw new IllegalArgumentException("Incorrect array");
        }

        int max = arrayIn[0];

        for (int valueInArray : arrayIn) {

            if (valueInArray > max) {
                max = valueInArray;
            }

        }

        return max;

    }

    private static int min(int[] arrayIn) {

        if (arrayIn == null) {
            throw new IllegalArgumentException("Incorrect array");
        }

        int min = arrayIn[0];

        for (int valueInArray : arrayIn) {

            if (valueInArray < min) {
                min = valueInArray;
            }

        }

        return min;

    }

    @Override
    public void execute() {
        int numbersOfElements = 3;
        int[] array = Service.getRandomIntArray(numbersOfElements, 150);

        System.out.println("Source numbers:");
        System.out.println(Arrays.toString(array));
        System.out.println();

        System.out.format(" sum min and max=%d", sumMinAndMaxInArray(array));
    }


}
