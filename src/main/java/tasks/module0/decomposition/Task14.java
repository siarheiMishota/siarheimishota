package tasks.module0.decomposition;

import tasks.module0.Service;

public class Task14 implements ArrayTasks {

    private static long compareElements(long firstNumber, long secondNumber) {

        int numberDigitsInFirst = numberDigits(firstNumber), numberDigitsInSecond = numberDigits(secondNumber);

        return Integer.compare(numberDigitsInFirst, numberDigitsInSecond);

    }

    private static int numberDigits(long numberIn) {
        int count = 0;

        while (numberIn > 0) {
            numberIn = numberIn / 10;
            count++;
        }
        return count;
    }

    @Override
    public void execute() {

        int firstNumber = Service.getRandomInt(8000), secondNumber = Service.getRandomInt(10001);

        System.out.println("First number=" + firstNumber);
        System.out.println("Second number=" + secondNumber);

        if (compareElements(firstNumber, secondNumber) > 1) {
            System.out.println("Number of digits in first number more then in second number");
        } else if (compareElements(firstNumber, secondNumber) == 0) {
            System.out.println("Number of digits in first number equal second number");
        } else {
            System.out.println("Number of digits in first number less then in second number");
        }

        System.out.println(compareElements(firstNumber, secondNumber));
    }

}
